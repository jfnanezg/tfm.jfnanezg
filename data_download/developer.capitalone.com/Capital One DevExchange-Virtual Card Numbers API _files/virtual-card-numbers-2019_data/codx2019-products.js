'use strict'

const _OPENER = document.querySelector('.DX-header_nav-mobile_hamburger');
const _CLOSER = document.querySelectorAll('.DX-header_nav-mobile_menu-close');
const _BACKS  = document.querySelectorAll('.DX-header_nav-mobile_menu-header a');
const _LINKS  = document.querySelectorAll('.DX-header_nav-mobile_menu-list ul li a');

console.log(_LINKS.length);

let activeNode = "";

window.addEventListener('DOMContentLoaded', function(event) {

    _OPENER.addEventListener('click', function(e) {
        e.preventDefault();
        document.querySelector('[data-mobile-nav=main]').classList.add('is-active');
        activeNode = document.querySelector('[data-mobile-nav=main]');
    });

    for(let c = 0; c < _CLOSER.length; c++) {
        _CLOSER[c].addEventListener('click', function(e) {
            e.preventDefault();
            activeNode.classList.remove('is-active');
            activeNode = "";
        });
    }

    for(let back = 0; back < _BACKS.length; back++) {
        _BACKS[back].addEventListener('click', function(e) {
            if(this.hasAttribute('data-mobile-link')) {
                e.preventDefault();
                activeNode.classList.remove('is-active');
                document.querySelector('[data-mobile-nav=' + this.getAttribute('data-mobile-link') + ']').classList.add('is-active');
                activeNode = document.querySelector('[data-mobile-nav=' + this.getAttribute('data-mobile-link') + ']');
            }
            else {
                return;
            }
        });
    }

    for(let link = 0; link < _LINKS.length; link++) {
        _LINKS[link].addEventListener('click', function(e) {
            if(this.hasAttribute('data-mobile-link')) {
                e.preventDefault();
                activeNode.classList.remove('is-active');
                document.querySelector('[data-mobile-nav=' + this.getAttribute('data-mobile-link') + ']').classList.add('is-active');
                activeNode = document.querySelector('[data-mobile-nav=' + this.getAttribute('data-mobile-link') + ']');
            }
            else {
                return;
            }
        });
    }

});
