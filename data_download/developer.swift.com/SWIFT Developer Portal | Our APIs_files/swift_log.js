(function ($) {
    Drupal.behaviors.swift_log = {
        attach: function(context, settings) {

            $(".sdk-download").on('click', function(event){
                var nid  = Drupal.settings.swift_log.nid;
                if(Drupal.settings.swift_log.uid != ''){
                    var uid  = Drupal.settings.swift_log.uid;
                }else{
                    alert("Please log in to download the SDK.");
                    event.preventDefault();
                    window.location.replace('/user/login?destination=node/'+nid);
                }
                var fileName = $(this). attr("href");
                var dataString = "filename="+fileName;
                $.ajax({
                    type: "POST",
                    url: Drupal.settings.basePath + "sdk/download/log",
                    dataType: 'json',
                    data: dataString,
                    cache: false,
                });
            });
        }
    };

}(jQuery));