(function ($)
 {


$(document).ready(function() 
 	{


 		$('#user-register-form button.btn-success').remove();
 		$('#registerLabel').text("Register a new user (This is mock registration flow.)");
 		$('#loginLabel').text("Log in (This is default demo login flow.)");
 		//alert($('ul.tabs--primary.nav.nav-tabs > li > a[href="/user/register"]').text());
 		$('ul.tabs--primary.nav.nav-tabs > li > a[href="/user/register"').text("Create new account (Mock registration)");
 		$('ul.tabs--primary.nav.nav-tabs > li > a[href="/user').text("Log in (Demo default login)");

 		$('#user-register-form input.username').val('openbank@apigee.net');
 		$('#user-register-form input.password-field').val('123456789');
 		$('#user-register-form input.password-confirm').val('123456789');
 		$('#user-register-form input[name=mail]').val('openbank@apigee.net');

 		// Login form default
 		$('#user-login #edit-name').val('openbank@apigee.net');
 		$('#user-login #edit-pass').val('(SDXCa3%kVck');

	var $registerButton = $('<button type="button" id="submitRegistration" value="Create new account" class="btn form-submit">Create new Account</button>');
	$registerButton.appendTo("#user-register-form");
	$('#submitRegistration').on("click", showSuccessRegistration);


	// app mock flow message message
	showMockFlowMessage();


	function showSuccessRegistration()
	{
		closeRegistration();
		$divSuccessRegister = $('<div class="alert alert-block alert-success messages status"><a class="close" data-dismiss="alert" href="#">&times;</a><h4 class="element-invisible">Status message</h4>User Registered Successfully(mock flow)</div>');
		jQuery("body").scrollTop(0);
		$('div.page-content').prepend($divSuccessRegister);
	}

     
    function closeRegistration() {
    jQuery('[role="dialog"].modal').modal('hide');
    jQuery('[role="dialog"].modal input').removeClass('error'); // Remove error class from the input boxes.
    jQuery('[role="dialog"].modal .error_container').hide().html(''); // Empty the error container and hide it.

    if(window.location.pathname == "/user/register")
    {
    	window.location = '/';
    }
    //return false;
  };


  	function showMockFlowMessage()
	{
		$divMockMessage = $('<div class="alert alert-block alert-success messages status"><a class="close" data-dismiss="alert" href="#">&times;</a><h4 class="element-invisible">Status message</h4>This is a mock flow for understanding purpose only. No App will actually be created on Apigee Edge</div>');
		
		$('#devconnect-developer-apps-edit-form').prepend($divMockMessage);
	}



 	});


 })(jQuery);
