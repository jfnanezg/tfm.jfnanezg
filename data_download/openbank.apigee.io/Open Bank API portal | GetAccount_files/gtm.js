
// Copyright 2012 Google Inc. All rights reserved.
(function(w,g){w[g]=w[g]||{};w[g].e=function(s){return eval(s);};})(window,'google_tag_manager');(function(){

var data = {
"resource": {
  "version":"346",
  
  "macros":[{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){var a=new Date;console.log(a);return a.getTime()})();"]
    },{
      "function":"__aev",
      "vtp_varType":"ELEMENT"
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){var a=",["escape",["macro",1],8,16],";return a=a.getAttribute(\"data-gtm-event\")})();"]
    },{
      "function":"__v",
      "vtp_name":"user.internal",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"user.email",
      "vtp_dataLayerVersion":2
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){var b=\"undefined\"!=typeof ",["escape",["macro",3],8,16],"?",["escape",["macro",3],8,16],":void 0,a=\"undefined\"!=typeof ",["escape",["macro",4],8,16],"?",["escape",["macro",4],8,16],".split(\"@\")[1].toLowerCase():\"\";return\"google.com\"===a||\"apigee.com\"===a?\"Internal\":b})();"]
    },{
      "function":"__u",
      "vtp_component":"HOST",
      "vtp_enableMultiQueryKeys":false
    },{
      "function":"__u",
      "vtp_component":"PATH",
      "vtp_enableMultiQueryKeys":false
    },{
      "function":"__e"
    },{
      "function":"__u",
      "vtp_component":"URL",
      "vtp_enableMultiQueryKeys":false
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"webapp.name"
    },{
      "function":"__f",
      "vtp_component":"URL"
    },{
      "function":"__remm",
      "vtp_setDefaultValue":false,
      "vtp_input":["macro",11],
      "vtp_fullMatch":true,
      "vtp_replaceAfterMatch":true,
      "vtp_ignoreCase":true,
      "vtp_map":["list",["map","key","^(https?:\/\/go.apigee.com).*","value","$1"],["map","key","^(https?:\/\/[^\\?]*).*","value","$1"]]
    },{
      "function":"__v",
      "vtp_name":"content-name",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"meant not to match anything"
    },{
      "function":"__smm",
      "vtp_setDefaultValue":true,
      "vtp_input":["macro",13],
      "vtp_defaultValue":["macro",13],
      "vtp_map":["list",["map","key",["macro",14],"value",["macro",7]]]
    },{
      "function":"__remm",
      "vtp_setDefaultValue":true,
      "vtp_input":["macro",15],
      "vtp_fullMatch":true,
      "vtp_replaceAfterMatch":true,
      "vtp_defaultValue":["macro",15],
      "vtp_ignoreCase":true,
      "vtp_map":["list",["map","key","^(.*?)([^\\\/]*)@([^\\\/]*)(.*)$","value","$1[redacted-email]$4"]]
    },{
      "function":"__u",
      "vtp_component":"QUERY",
      "vtp_queryKey":"dclid",
      "vtp_enableMultiQueryKeys":false
    },{
      "function":"__smm",
      "vtp_setDefaultValue":true,
      "vtp_input":["macro",17],
      "vtp_defaultValue":["template","dclid=",["macro",17],"\u0026"],
      "vtp_map":["list",["map","key",["macro",14],"value",""]]
    },{
      "function":"__u",
      "vtp_component":"QUERY",
      "vtp_queryKey":"gclid",
      "vtp_enableMultiQueryKeys":false
    },{
      "function":"__smm",
      "vtp_setDefaultValue":true,
      "vtp_input":["macro",19],
      "vtp_defaultValue":["template","gclid=",["macro",19],"\u0026"],
      "vtp_map":["list",["map","key",["macro",14],"value",""]]
    },{
      "function":"__c",
      "vtp_value":["template","https:\/\/",["macro",6],["macro",16],"?",["macro",18],["macro",20]]
    },{
      "function":"__u",
      "vtp_component":"QUERY",
      "vtp_queryKey":"utm_medium",
      "vtp_enableMultiQueryKeys":false
    },{
      "function":"__u",
      "vtp_component":"QUERY",
      "vtp_queryKey":"utm_source",
      "vtp_enableMultiQueryKeys":false
    },{
      "function":"__u",
      "vtp_component":"QUERY",
      "vtp_queryKey":"utm_campaign",
      "vtp_enableMultiQueryKeys":false
    },{
      "function":"__u",
      "vtp_component":"QUERY",
      "vtp_queryKey":"utm_content",
      "vtp_enableMultiQueryKeys":false
    },{
      "function":"__u",
      "vtp_component":"QUERY",
      "vtp_queryKey":"utm_term",
      "vtp_enableMultiQueryKeys":false
    },{
      "function":"__j",
      "vtp_name":"document.title"
    },{
      "function":"__remm",
      "vtp_setDefaultValue":true,
      "vtp_input":["macro",27],
      "vtp_fullMatch":true,
      "vtp_replaceAfterMatch":true,
      "vtp_defaultValue":["macro",27],
      "vtp_ignoreCase":true,
      "vtp_map":["list",["map","key","^(.*?)([^ ]*)@([^ ]*)(.*)$","value","$1[redacted-email]$4"]]
    },{
      "function":"__smm",
      "vtp_setDefaultValue":false,
      "vtp_input":["macro",10],
      "vtp_map":["list",["map","key","static","value","Edge"],["map","key","shoehorn","value","Edge"],["map","key","proxies","value","Edge"],["map","key","products","value","Edge"],["map","key","pinpoint","value","Edge"],["map","key","nucleus","value","Edge"],["map","key","portal2","value","Edge"],["map","key","management","value","Enterprise (classic)"],["map","key","history","value","Edge"],["map","key","eventsite","value","Marketing"],["map","key","docstore","value","Edge"],["map","key","developers","value","Edge"],["map","key","baasportal","value","Edge"],["map","key","apps","value","Edge"],["map","key","marketing","value","Marketing"],["map","key","devportal","value","Enterprise (classic)"],["map","key","marketing","value","Marketing"],["map","key","edgex","value","EdgeX"],["map","key","sense","value","Sense"]]
    },{
      "function":"__remm",
      "vtp_setDefaultValue":true,
      "vtp_input":["macro",6],
      "vtp_fullMatch":true,
      "vtp_replaceAfterMatch":true,
      "vtp_ignoreCase":true,
      "vtp_defaultValue":["macro",29],
      "vtp_map":["list",["map","key",".*(e2e|devportal|movie|docs|mktg-stg)\\.apigee\\..*","value",""]]
    },{
      "function":"__v",
      "vtp_name":"organization.name",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"organization.type",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"webapp.version"
    },{
      "function":"__c",
      "vtp_value":"UA-4084158-23"
    },{
      "function":"__gas",
      "vtp_cookieDomain":"auto",
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_fieldsToSet":["list",["map","fieldName","referrer","value",["macro",12]],["map","fieldName","anonymizeIp","value","true"],["map","fieldName","location","value",["macro",21]],["map","fieldName","campaignMedium","value",["macro",22]],["map","fieldName","campaignSource","value",["macro",23]],["map","fieldName","campaignName","value",["macro",24]],["map","fieldName","campaignContent","value",["macro",25]],["map","fieldName","campaignKeyword","value",["macro",26]],["map","fieldName","page","value",["macro",16]],["map","fieldName","siteSpeedSampleRate","value","10"],["map","fieldName","allowLinker","value","true"],["map","fieldName","title","value",["macro",28]]],
      "vtp_useHashAutoLink":false,
      "vtp_contentGroup":["list",["map","index","1","group",["macro",14]],["map","index","2","group",["macro",30]],["map","index","3","group",["macro",10]],["map","index","4","group",["macro",14]],["map","index","5","group",["macro",14]]],
      "vtp_decorateFormsAutoLink":false,
      "vtp_enableLinkId":false,
      "vtp_dimension":["list",["map","index","1","dimension",["macro",14]],["map","index","2","dimension",["macro",5]],["map","index","3","dimension",["macro",31]],["map","index","4","dimension",["macro",32]],["map","index","5","dimension",["template",["macro",10],"_",["macro",33]]]],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",34],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false
    },{
      "function":"__v",
      "vtp_name":"audience_segment",
      "vtp_defaultValue":"(no audience_segment detected)",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"fortune_1000",
      "vtp_defaultValue":"(non-company visitor)",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"watch_list_account_status",
      "vtp_defaultValue":"(non account watch visitor)",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"company_name",
      "vtp_defaultValue":"(non-company visitor)",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"industry",
      "vtp_defaultValue":"(non-company visitor)",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"revenue_range",
      "vtp_defaultValue":"(non-company visitor)",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"employee_range",
      "vtp_defaultValue":"(non-company visitor)",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"demandbase_sid",
      "vtp_defaultValue":"(non-company visitor)",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"audience",
      "vtp_defaultValue":"(no audience detected)",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"watch_list_sales_territory",
      "vtp_defaultValue":"(non account watch visitor)",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"sub_industry",
      "vtp_defaultValue":"(non-company visitor)",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"state",
      "vtp_defaultValue":"(non-company visitor)",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"country_name",
      "vtp_defaultValue":"(non-company visitor)",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"forbes_2000",
      "vtp_defaultValue":"(non-company visitor)",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":true,
      "vtp_defaultValue":"(non account watch visitor)",
      "vtp_name":"watch_list_db_campaign_code"
    },{
      "function":"__aev",
      "vtp_varType":"URL"
    },{
      "function":"__v",
      "vtp_name":"gtm.triggers",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":true,
      "vtp_defaultValue":""
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"interaction-type"
    },{
      "function":"__v",
      "vtp_name":"target",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"action",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"timingLabel"
    },{
      "function":"__v",
      "vtp_name":"value",
      "vtp_dataLayerVersion":2
    },{
      "function":"__aev",
      "vtp_varType":"CLASSES"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":true,
      "vtp_defaultValue":"Non-Campaign Visitor",
      "vtp_name":"db_experience"
    },{
      "function":"__aev",
      "vtp_setDefaultValue":true,
      "vtp_varType":"ATTRIBUTE",
      "vtp_defaultValue":"(not set)",
      "vtp_attribute":"data-category"
    },{
      "function":"__aev",
      "vtp_setDefaultValue":true,
      "vtp_varType":"ATTRIBUTE",
      "vtp_defaultValue":"(not set)",
      "vtp_attribute":"data-action"
    },{
      "function":"__aev",
      "vtp_setDefaultValue":true,
      "vtp_varType":"ATTRIBUTE",
      "vtp_defaultValue":"(not set)",
      "vtp_attribute":"data-label"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"timingVar"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"timingValue"
    },{
      "function":"__u",
      "vtp_component":"FRAGMENT",
      "vtp_enableMultiQueryKeys":false
    },{
      "function":"__remm",
      "vtp_setDefaultValue":false,
      "vtp_input":["macro",65],
      "vtp_fullMatch":true,
      "vtp_replaceAfterMatch":true,
      "vtp_ignoreCase":true,
      "vtp_map":["list",["map","key","^.*?\\?(.*)","value",["template","https:\/\/",["macro",6],["macro",7],"?$1"]],["map","key","^(.*)","value",["template","https:\/\/",["macro",6],["macro",7],"?$1"]]]
    },{
      "function":"__u",
      "vtp_component":"QUERY",
      "vtp_queryKey":"utm_medium",
      "vtp_customUrlSource":["macro",66],
      "vtp_enableMultiQueryKeys":false
    },{
      "function":"__smm",
      "vtp_setDefaultValue":true,
      "vtp_input":["macro",22],
      "vtp_defaultValue":["macro",22],
      "vtp_map":["list",["map","key",["macro",14],"value",["macro",67]],["map","key","","value",["macro",67]]]
    },{
      "function":"__u",
      "vtp_component":"QUERY",
      "vtp_queryKey":"utm_source",
      "vtp_customUrlSource":["macro",66],
      "vtp_enableMultiQueryKeys":false
    },{
      "function":"__smm",
      "vtp_setDefaultValue":true,
      "vtp_input":["macro",23],
      "vtp_defaultValue":["macro",23],
      "vtp_map":["list",["map","key",["macro",14],"value",["macro",69]],["map","key","","value",["macro",69]]]
    },{
      "function":"__u",
      "vtp_component":"QUERY",
      "vtp_queryKey":"utm_campaign",
      "vtp_customUrlSource":["macro",66],
      "vtp_enableMultiQueryKeys":false
    },{
      "function":"__smm",
      "vtp_setDefaultValue":true,
      "vtp_input":["macro",24],
      "vtp_defaultValue":["macro",24],
      "vtp_map":["list",["map","key",["macro",14],"value",["macro",71]],["map","key","","value",["macro",71]]]
    },{
      "function":"__u",
      "vtp_component":"QUERY",
      "vtp_queryKey":"utm_content",
      "vtp_customUrlSource":["macro",66],
      "vtp_enableMultiQueryKeys":false
    },{
      "function":"__smm",
      "vtp_setDefaultValue":true,
      "vtp_input":["macro",25],
      "vtp_defaultValue":["macro",25],
      "vtp_map":["list",["map","key",["macro",14],"value",["macro",73]],["map","key","","value",["macro",73]]]
    },{
      "function":"__u",
      "vtp_component":"QUERY",
      "vtp_queryKey":"utm_term",
      "vtp_customUrlSource":["macro",66],
      "vtp_enableMultiQueryKeys":false
    },{
      "function":"__smm",
      "vtp_setDefaultValue":true,
      "vtp_input":["macro",26],
      "vtp_defaultValue":["macro",26],
      "vtp_map":["list",["map","key",["macro",14],"value",["macro",75]],["map","key","","value",["macro",75]]]
    },{
      "function":"__smm",
      "vtp_setDefaultValue":true,
      "vtp_input":["macro",31],
      "vtp_defaultValue":["template",["macro",31],", ",["macro",32]],
      "vtp_map":["list",["map","key",["macro",14],"value",["macro",14]]]
    },{
      "function":"__c",
      "vtp_value":"UA-4084158-4"
    },{
      "function":"__gas",
      "vtp_cookieDomain":"auto",
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_fieldsToSet":["list",["map","fieldName","referrer","value",["macro",12]],["map","fieldName","anonymizeIp","value","true"],["map","fieldName","location","value",["macro",21]],["map","fieldName","campaignMedium","value",["macro",68]],["map","fieldName","campaignSource","value",["macro",70]],["map","fieldName","campaignName","value",["macro",72]],["map","fieldName","campaignContent","value",["macro",74]],["map","fieldName","campaignKeyword","value",["macro",76]],["map","fieldName","page","value",["macro",16]],["map","fieldName","siteSpeedSampleRate","value","10"],["map","fieldName","allowLinker","value","true"],["map","fieldName","title","value",["macro",28]]],
      "vtp_useHashAutoLink":false,
      "vtp_contentGroup":["list",["map","index","1","group",["macro",14]],["map","index","2","group",["macro",30]],["map","index","3","group",["macro",10]],["map","index","4","group",["macro",14]],["map","index","5","group",["macro",14]]],
      "vtp_decorateFormsAutoLink":false,
      "vtp_enableLinkId":false,
      "vtp_dimension":["list",["map","index","1","dimension",["macro",14]],["map","index","2","dimension",["macro",5]],["map","index","3","dimension",["macro",31]],["map","index","4","dimension",["macro",32]],["map","index","5","dimension",["template",["macro",10],"_",["macro",33]]],["map","index","21","dimension",["macro",77]],["map","index","22","dimension",["macro",10]],["map","index","23","dimension",["macro",33]]],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",78],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"target-properties"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"timingCategory"
    },{
      "function":"__aev",
      "vtp_setDefaultValue":true,
      "vtp_varType":"ATTRIBUTE",
      "vtp_defaultValue":"false",
      "vtp_attribute":"data-noninteraction"
    },{
      "function":"__aev",
      "vtp_setDefaultValue":true,
      "vtp_varType":"ATTRIBUTE",
      "vtp_defaultValue":"",
      "vtp_attribute":"data-value"
    },{
      "function":"__v",
      "vtp_name":"eventCategory",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"eventAction",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"eventLabel",
      "vtp_defaultValue":"",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":true,
      "vtp_defaultValue":"(non account watch visitor)",
      "vtp_name":"watch_list_abm_category"
    },{
      "function":"__r"
    },{
      "function":"__u",
      "vtp_enableMultiQueryKeys":false
    },{
      "function":"__remm",
      "vtp_setDefaultValue":true,
      "vtp_input":["macro",15],
      "vtp_fullMatch":true,
      "vtp_replaceAfterMatch":true,
      "vtp_ignoreCase":true,
      "vtp_defaultValue":"",
      "vtp_map":["list",["map","key","^(.*)-re-ty(\\..*|$)","value","apige00a"],["map","key","^(.*)-rw-ty(\\..*|$)","value","apige00-"],["map","key","^(.*)-confirmation-ty(\\..*|$)","value","apige009"],["map","key","^(.*)-bsm-ty(\\..*|$)","value","apige008"],["map","key","^(.*)-register(\\..*|$)","value","apige007"],["map","key",".*(webinar|webcast|virtual).*\/registrations\/new\/thanks.*","value","apige00-"],["map","key",".*\/registrations\/new\/thanks.*","value","apige00a"]]
    },{
      "function":"__remm",
      "vtp_setDefaultValue":true,
      "vtp_input":["macro",15],
      "vtp_fullMatch":true,
      "vtp_replaceAfterMatch":true,
      "vtp_ignoreCase":true,
      "vtp_defaultValue":"",
      "vtp_map":["list",["map","key","^(.*)-re-ty(\\..*|$)","value","$1"],["map","key","^(.*)-rw-ty(\\..*|$)","value","$1"],["map","key","^(.*)-confirmation-ty(\\..*|$)","value","$1"],["map","key","^(.*)-bsm-ty(\\..*|$)","value","$1"],["map","key","^(.*)-register(\\..*|$)","value","$1"],["map","key","^(.*)\/registrations\/new\/thanks\/.*","value","$1"]]
    },{
      "function":"__aev",
      "vtp_setDefaultValue":false,
      "vtp_varType":"ATTRIBUTE",
      "vtp_attribute":"data-gtmcategory"
    },{
      "function":"__aev",
      "vtp_setDefaultValue":false,
      "vtp_varType":"ATTRIBUTE",
      "vtp_attribute":"data-gtmaction"
    },{
      "function":"__aev",
      "vtp_setDefaultValue":false,
      "vtp_varType":"ATTRIBUTE",
      "vtp_attribute":"data-gtmlabel"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"enableAutoEvents"
    },{
      "function":"__t"
    },{
      "function":"__smm",
      "vtp_setDefaultValue":true,
      "vtp_input":["macro",10],
      "vtp_defaultValue":"undefined",
      "vtp_map":["list",["map","key","apps","value","vypwx4bul51wpyxzlyzdsa"],["map","key","developers","value","vypwx4bul51wpyxzlyzdsa"],["map","key","history","value","vypwx4bul51wpyxzlyzdsa"],["map","key","portal2","value","vypwx4bul51wpyxzlyzdsa"],["map","key","products","value","vypwx4bul51wpyxzlyzdsa"],["map","key","proxies","value","vypwx4bul51wpyxzlyzdsa"],["map","key","shoehorn","value","vypwx4bul51wpyxzlyzdsa"],["map","key","static","value","vypwx4bul51wpyxzlyzdsa"],["map","key","studio","value","vypwx4bul51wpyxzlyzdsa"],["map","key","docstore","value","vypwx4bul51wpyxzlyzdsa"],["map","key","landing","value","vypwx4bul51wpyxzlyzdsa"],["map","key","SharedFlowsSPA","value","vypwx4bul51wpyxzlyzdsa"],["map","key","developer-programs","value","vypwx4bul51wpyxzlyzdsa"],["map","key","security","value","vypwx4bul51wpyxzlyzdsa"],["map","key","AsyncReportsSPA","value","vypwx4bul51wpyxzlyzdsa"],["map","key","environments","value","vypwx4bul51wpyxzlyzdsa"],["map","key","extensions","value","vypwx4bul51wpyxzlyzdsa"],["map","key","privacy","value","vypwx4bul51wpyxzlyzdsa"]]
    },{
      "function":"__v",
      "vtp_dataLayerVersion":1,
      "vtp_setDefaultValue":false,
      "vtp_name":"webapp.name"
    },{
      "function":"__e"
    },{
      "function":"__aev",
      "vtp_varType":"ID"
    },{
      "function":"__aev",
      "vtp_varType":"TARGET"
    },{
      "function":"__v",
      "vtp_name":"user.uuid",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"eventValue",
      "vtp_dataLayerVersion":1
    },{
      "function":"__e"
    },{
      "function":"__d",
      "vtp_attributeName":"dropHandle",
      "vtp_selectorType":"CSS",
      "vtp_elementSelector":["macro",58]
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"content-name"
    },{
      "function":"__u",
      "vtp_component":"URL",
      "vtp_enableMultiQueryKeys":false
    },{
      "function":"__f",
      "vtp_component":"URL"
    },{
      "function":"__v",
      "vtp_name":"gtm.element",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.elementTarget",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.elementUrl",
      "vtp_dataLayerVersion":1
    },{
      "function":"__aev",
      "vtp_varType":"TEXT"
    }],
  "tags":[{
      "function":"__paused",
      "vtp_originalTagType":"asp",
      "tag_id":17
    },{
      "function":"__paused",
      "vtp_originalTagType":"html",
      "tag_id":18
    },{
      "function":"__paused",
      "vtp_originalTagType":"html",
      "tag_id":19
    },{
      "function":"__paused",
      "vtp_originalTagType":"ga",
      "tag_id":25
    },{
      "function":"__paused",
      "vtp_originalTagType":"html",
      "tag_id":26
    },{
      "function":"__paused",
      "vtp_originalTagType":"html",
      "tag_id":27
    },{
      "function":"__paused",
      "vtp_originalTagType":"ga",
      "tag_id":36
    },{
      "function":"__paused",
      "vtp_originalTagType":"asp",
      "tag_id":37
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_overrideGaSettings":true,
      "vtp_setTrackerName":false,
      "vtp_doubleClick":true,
      "vtp_useDebugVersion":false,
      "vtp_fieldsToSet":["list",["map","fieldName","allowLinker","value","false"]],
      "vtp_useHashAutoLink":false,
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_decorateFormsAutoLink":false,
      "vtp_enableLinkId":false,
      "vtp_gaSettings":["macro",35],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":"UA-4084158-23",
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":41
    },{
      "function":"__ua",
      "vtp_nonInteraction":true,
      "vtp_useDebugVersion":false,
      "vtp_eventCategory":"Demandbase",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",35],
      "vtp_eventAction":"API Resolution",
      "vtp_eventLabel":"IP Address API",
      "vtp_overrideGaSettings":true,
      "vtp_setTrackerName":true,
      "vtp_trackerName":"apigee08282017",
      "vtp_enableLinkId":false,
      "vtp_dimension":["list",["map","index","7","dimension",["macro",36]],["map","index","16","dimension",["macro",37]],["map","index","14","dimension",["macro",38]],["map","index","8","dimension",["macro",39]],["map","index","9","dimension",["macro",40]],["map","index","11","dimension",["macro",41]],["map","index","12","dimension",["macro",42]],["map","index","13","dimension",["macro",43]],["map","index","6","dimension",["macro",44]],["map","index","15","dimension",["macro",45]],["map","index","10","dimension",["macro",46]],["map","index","18","dimension",["macro",47]],["map","index","19","dimension",["macro",48]],["map","index","17","dimension",["macro",49]],["map","index","25","dimension",["macro",50]]],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":"UA-4084158-23",
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":43
    },{
      "function":"__paused",
      "vtp_originalTagType":"ga",
      "tag_id":45
    },{
      "function":"__paused",
      "vtp_originalTagType":"html",
      "tag_id":46
    },{
      "function":"__paused",
      "vtp_originalTagType":"ga",
      "tag_id":47
    },{
      "function":"__paused",
      "vtp_originalTagType":"ga",
      "tag_id":48
    },{
      "function":"__paused",
      "vtp_originalTagType":"html",
      "tag_id":49
    },{
      "function":"__paused",
      "vtp_originalTagType":"ga",
      "tag_id":52
    },{
      "function":"__paused",
      "vtp_originalTagType":"ga",
      "tag_id":73
    },{
      "function":"__paused",
      "vtp_originalTagType":"html",
      "tag_id":79
    },{
      "function":"__paused",
      "vtp_originalTagType":"ga",
      "tag_id":80
    },{
      "function":"__paused",
      "vtp_originalTagType":"ga",
      "tag_id":81
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_useDebugVersion":false,
      "vtp_useHashAutoLink":false,
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_contentGroup":["list",["map","index","1","group",["macro",10]],["map","index","2","group",["macro",30]]],
      "vtp_decorateFormsAutoLink":false,
      "vtp_gaSettings":["macro",35],
      "vtp_overrideGaSettings":true,
      "vtp_setTrackerName":false,
      "vtp_doubleClick":true,
      "vtp_fieldsToSet":["list",["map","fieldName","allowLinker","value","false"]],
      "vtp_enableLinkId":false,
      "vtp_dimension":["list",["map","index","2","dimension",["macro",5]],["map","index","3","dimension",["macro",31]],["map","index","4","dimension",["macro",32]]],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":"UA-4084158-23",
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":84
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":["macro",53],
      "vtp_useDebugVersion":false,
      "vtp_eventCategory":["macro",54],
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",35],
      "vtp_eventAction":["macro",55],
      "vtp_eventLabel":["macro",56],
      "vtp_overrideGaSettings":true,
      "vtp_setTrackerName":false,
      "vtp_doubleClick":false,
      "vtp_eventValue":["macro",57],
      "vtp_enableLinkId":false,
      "vtp_dimension":["list",["map","index","2","dimension",["macro",5]],["map","index","3","dimension",["macro",31]],["map","index","4","dimension",["macro",32]]],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":"UA-4084158-23",
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":85
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_useDebugVersion":false,
      "vtp_useHashAutoLink":false,
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_contentGroup":["list",["map","index","1","group",["macro",10]],["map","index","2","group",["macro",30]]],
      "vtp_decorateFormsAutoLink":false,
      "vtp_gaSettings":["macro",35],
      "vtp_overrideGaSettings":true,
      "vtp_setTrackerName":false,
      "vtp_doubleClick":false,
      "vtp_fieldsToSet":["list",["map","fieldName","page","value",["macro",13]]],
      "vtp_enableLinkId":false,
      "vtp_dimension":["list",["map","index","2","dimension",["macro",5]],["map","index","3","dimension",["macro",31]],["map","index","4","dimension",["macro",32]]],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":"UA-4084158-23",
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":86
    },{
      "function":"__paused",
      "vtp_originalTagType":"html",
      "tag_id":87
    },{
      "function":"__paused",
      "vtp_originalTagType":"html",
      "tag_id":90
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_useDebugVersion":false,
      "vtp_eventCategory":"Home-Clicks",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",35],
      "vtp_eventAction":["macro",51],
      "vtp_eventLabel":["macro",9],
      "vtp_overrideGaSettings":true,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_enableLinkId":false,
      "vtp_enableEcommerce":false,
      "vtp_trackingId":"UA-4084158-23",
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":91
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":true,
      "vtp_useDebugVersion":false,
      "vtp_eventCategory":"Demandbase",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",35],
      "vtp_eventAction":"Personalization",
      "vtp_eventLabel":"Experience Loaded",
      "vtp_overrideGaSettings":true,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_enableLinkId":false,
      "vtp_dimension":["list",["map","index","20","dimension",["macro",59]]],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":"UA-4084158-23",
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":92
    },{
      "function":"__paused",
      "vtp_originalTagType":"ga",
      "tag_id":97
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_useDebugVersion":false,
      "vtp_useHashAutoLink":false,
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_contentGroup":["list",["map","index","2","group",["macro",10]],["map","index","1","group",["macro",30]]],
      "vtp_decorateFormsAutoLink":false,
      "vtp_gaSettings":["macro",35],
      "vtp_overrideGaSettings":true,
      "vtp_setTrackerName":false,
      "vtp_doubleClick":true,
      "vtp_fieldsToSet":["list",["map","fieldName","allowLinker","value","false"]],
      "vtp_enableLinkId":false,
      "vtp_dimension":["list",["map","index","2","dimension",["macro",5]]],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":"UA-4084158-23",
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":98
    },{
      "function":"__paused",
      "vtp_originalTagType":"ga",
      "tag_id":99
    },{
      "function":"__paused",
      "vtp_originalTagType":"html",
      "tag_id":104
    },{
      "function":"__paused",
      "vtp_originalTagType":"html",
      "tag_id":105
    },{
      "function":"__paused",
      "vtp_originalTagType":"html",
      "tag_id":112
    },{
      "function":"__paused",
      "vtp_originalTagType":"html",
      "tag_id":120
    },{
      "function":"__paused",
      "vtp_originalTagType":"ga",
      "tag_id":121
    },{
      "function":"__paused",
      "vtp_originalTagType":"ga",
      "tag_id":125
    },{
      "function":"__ua",
      "vtp_overrideGaSettings":true,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_trackType":"TRACK_TIMING",
      "vtp_timingLabel":["macro",56],
      "vtp_enableLinkId":false,
      "vtp_timingCategory":["template","HTTP: ",["macro",63]],
      "vtp_gaSettings":["macro",35],
      "vtp_timingVar":["macro",63],
      "vtp_timingValue":["macro",64],
      "vtp_trackingId":"UA-4084158-23",
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsTiming":true,
      "tag_id":127
    },{
      "function":"__ua",
      "vtp_useDebugVersion":false,
      "vtp_trackType":"TRACK_TIMING",
      "vtp_timingLabel":["macro",56],
      "vtp_contentGroup":["list",["map","index","2","group",["macro",30]],["map","index","3","group",["macro",10]]],
      "vtp_timingCategory":["template","HTTP ",["macro",63]],
      "vtp_gaSettings":["macro",35],
      "vtp_timingVar":["macro",63],
      "vtp_overrideGaSettings":true,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_fieldsToSet":["list",["map","fieldName","siteSpeedSampleRate","value","100"]],
      "vtp_enableLinkId":false,
      "vtp_dimension":["list",["map","index","2","dimension",["macro",5]],["map","index","3","dimension",["macro",31]],["map","index","4","dimension",["macro",32]]],
      "vtp_timingValue":["macro",64],
      "vtp_trackingId":"UA-4084158-4",
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsTiming":true,
      "tag_id":128
    },{
      "function":"__paused",
      "vtp_originalTagType":"html",
      "tag_id":141
    },{
      "function":"__ua",
      "vtp_nonInteraction":["macro",53],
      "vtp_overrideGaSettings":true,
      "vtp_eventValue":["macro",57],
      "vtp_eventCategory":["macro",54],
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",79],
      "vtp_eventAction":["macro",55],
      "vtp_eventLabel":["macro",80],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":146
    },{
      "function":"__ua",
      "vtp_overrideGaSettings":true,
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_gaSettings":["macro",79],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":147
    },{
      "function":"__ua",
      "vtp_overrideGaSettings":true,
      "vtp_trackType":"TRACK_TIMING",
      "vtp_timingLabel":["macro",56],
      "vtp_timingCategory":["macro",81],
      "vtp_gaSettings":["macro",79],
      "vtp_timingVar":["template","HTTP",["macro",63]],
      "vtp_timingValue":["macro",64],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsTiming":true,
      "tag_id":148
    },{
      "function":"__ua",
      "vtp_nonInteraction":["macro",82],
      "vtp_overrideGaSettings":true,
      "vtp_eventValue":["macro",83],
      "vtp_eventCategory":["macro",60],
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",79],
      "vtp_eventAction":["macro",61],
      "vtp_eventLabel":["macro",62],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":149
    },{
      "function":"__ua",
      "vtp_nonInteraction":true,
      "vtp_overrideGaSettings":true,
      "vtp_eventValue":["macro",7],
      "vtp_eventCategory":["macro",84],
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",79],
      "vtp_eventAction":["macro",85],
      "vtp_eventLabel":["macro",86],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":150
    },{
      "function":"__ua",
      "vtp_overrideGaSettings":true,
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_gaSettings":["macro",79],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":151
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":true,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"Outbound Link",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",79],
      "vtp_eventAction":["macro",51],
      "vtp_eventLabel":["template",["macro",6],["macro",7]],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":152
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_overrideGaSettings":true,
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_gaSettings":["macro",79],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":153
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_overrideGaSettings":true,
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_gaSettings":["macro",79],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":154
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_overrideGaSettings":true,
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_gaSettings":["macro",79],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":155
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_overrideGaSettings":true,
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_gaSettings":["macro",79],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":156
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_overrideGaSettings":true,
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_gaSettings":["macro",79],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":157
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"File Download",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",79],
      "vtp_eventAction":["macro",51],
      "vtp_eventLabel":["template",["macro",6],["macro",7]],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":158
    },{
      "function":"__ua",
      "vtp_nonInteraction":true,
      "vtp_overrideGaSettings":true,
      "vtp_useDebugVersion":false,
      "vtp_eventCategory":"Demandbase",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_gaSettings":["macro",79],
      "vtp_eventAction":"API Resolution",
      "vtp_eventLabel":"IP Address API",
      "vtp_dimension":["list",["map","index","6","dimension",["macro",44]],["map","index","7","dimension",["macro",36]],["map","index","8","dimension",["macro",39]],["map","index","9","dimension",["macro",40]],["map","index","10","dimension",["macro",46]],["map","index","11","dimension",["macro",41]],["map","index","12","dimension",["macro",42]],["map","index","13","dimension",["macro",43]],["map","index","14","dimension",["macro",38]],["map","index","15","dimension",["macro",45]],["map","index","16","dimension",["macro",37]],["map","index","17","dimension",["macro",49]],["map","index","18","dimension",["macro",47]],["map","index","19","dimension",["macro",48]],["map","index","24","dimension",["macro",50]],["map","index","25","dimension",["macro",87]]],
      "vtp_enableEcommerce":false,
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":159
    },{
      "function":"__flc",
      "once_per_event":true,
      "vtp_enableConversionLinker":true,
      "vtp_groupTag":"apige0",
      "vtp_useImageTag":false,
      "vtp_activityTag":"apige0",
      "vtp_ordinalType":"STANDARD",
      "vtp_conversionCookiePrefix":"_gcl",
      "vtp_advertiserId":"2542116",
      "vtp_ordinalStandard":["macro",88],
      "vtp_url":["macro",89],
      "vtp_enableGoogleAttributionOptions":false,
      "vtp_showConversionLinkingControls":true,
      "tag_id":160
    },{
      "function":"__flc",
      "vtp_enableConversionLinker":true,
      "vtp_groupTag":"cloud",
      "vtp_useImageTag":true,
      "vtp_activityTag":"googl013",
      "vtp_ordinalType":"STANDARD",
      "vtp_conversionCookiePrefix":"_gcl",
      "vtp_advertiserId":"2507573",
      "vtp_ordinalStandard":["macro",88],
      "vtp_url":["macro",89],
      "vtp_useImageTagIsTrue":true,
      "vtp_enableGoogleAttributionOptions":false,
      "vtp_showConversionLinkingControls":true,
      "tag_id":161
    },{
      "function":"__flc",
      "vtp_enableConversionLinker":true,
      "vtp_groupTag":"cloud",
      "vtp_useImageTag":true,
      "vtp_activityTag":"googl014",
      "vtp_ordinalType":"STANDARD",
      "vtp_conversionCookiePrefix":"_gcl",
      "vtp_advertiserId":"2507573",
      "vtp_ordinalStandard":["macro",88],
      "vtp_url":["macro",89],
      "vtp_useImageTagIsTrue":true,
      "vtp_enableGoogleAttributionOptions":false,
      "vtp_showConversionLinkingControls":true,
      "tag_id":162
    },{
      "function":"__flc",
      "vtp_enableConversionLinker":true,
      "vtp_groupTag":"apigee",
      "vtp_useImageTag":true,
      "vtp_activityTag":"apige00",
      "vtp_ordinalType":"UNIQUE",
      "vtp_conversionCookiePrefix":"_gcl",
      "vtp_advertiserId":"2507573",
      "vtp_ordinalUnique":"1",
      "vtp_number":["macro",88],
      "vtp_url":["macro",89],
      "vtp_useImageTagIsTrue":true,
      "vtp_enableGoogleAttributionOptions":false,
      "vtp_showConversionLinkingControls":true,
      "tag_id":163
    },{
      "function":"__flc",
      "vtp_enableConversionLinker":true,
      "vtp_groupTag":"cloud",
      "vtp_useImageTag":true,
      "vtp_activityTag":"apige0",
      "vtp_ordinalType":"STANDARD",
      "vtp_conversionCookiePrefix":"_gcl",
      "vtp_advertiserId":"2507573",
      "vtp_ordinalStandard":["macro",88],
      "vtp_url":["macro",89],
      "vtp_useImageTagIsTrue":true,
      "vtp_enableGoogleAttributionOptions":false,
      "vtp_showConversionLinkingControls":true,
      "tag_id":164
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_overrideGaSettings":true,
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_trackingId":"UA-112679519-1",
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":165
    },{
      "function":"__flc",
      "vtp_enableConversionLinker":true,
      "vtp_groupTag":"apigee",
      "vtp_useImageTag":true,
      "vtp_activityTag":"apige000",
      "vtp_ordinalType":"STANDARD",
      "vtp_conversionCookiePrefix":"_gcl",
      "vtp_advertiserId":"2507573",
      "vtp_ordinalStandard":["macro",88],
      "vtp_url":["macro",89],
      "vtp_useImageTagIsTrue":true,
      "vtp_enableGoogleAttributionOptions":false,
      "vtp_showConversionLinkingControls":true,
      "tag_id":166
    },{
      "function":"__gclidw",
      "once_per_event":true,
      "vtp_enableCrossDomain":false,
      "vtp_enableCookieOverrides":false,
      "vtp_enableCrossDomainFeature":true,
      "vtp_enableCookieUpdateFeature":false,
      "tag_id":167
    },{
      "function":"__awct",
      "once_per_event":true,
      "vtp_enableConversionLinker":true,
      "vtp_conversionCookiePrefix":"_gcl",
      "vtp_conversionId":"819728548",
      "vtp_conversionLabel":"3HzmCJuSp3wQpKHwhgM",
      "vtp_url":["macro",89],
      "vtp_enableProductReportingCheckbox":false,
      "tag_id":168
    },{
      "function":"__flc",
      "vtp_enableConversionLinker":true,
      "vtp_groupTag":"apigee",
      "vtp_useImageTag":true,
      "vtp_activityTag":"apige001",
      "vtp_ordinalType":"STANDARD",
      "vtp_conversionCookiePrefix":"_gcl",
      "vtp_advertiserId":"2507573",
      "vtp_ordinalStandard":["macro",88],
      "vtp_url":["macro",89],
      "vtp_useImageTagIsTrue":true,
      "vtp_enableGoogleAttributionOptions":false,
      "vtp_showConversionLinkingControls":true,
      "tag_id":169
    },{
      "function":"__sp",
      "once_per_event":true,
      "vtp_conversionId":"813545861",
      "vtp_customParamsFormat":"NONE",
      "vtp_enableOgtRmktParams":true,
      "vtp_url":["macro",89],
      "tag_id":170
    },{
      "function":"__flc",
      "once_per_event":true,
      "vtp_customVariable":["list",["map","key","u9","value",["macro",91]]],
      "vtp_enableConversionLinker":true,
      "vtp_groupTag":"apigee",
      "vtp_useImageTag":false,
      "vtp_activityTag":["macro",90],
      "vtp_ordinalType":"STANDARD",
      "vtp_conversionCookiePrefix":"_gcl",
      "vtp_advertiserId":"2507573",
      "vtp_ordinalStandard":["macro",88],
      "vtp_url":["macro",89],
      "vtp_enableGoogleAttributionOptions":false,
      "vtp_showConversionLinkingControls":true,
      "tag_id":171
    },{
      "function":"__ua",
      "vtp_nonInteraction":true,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":["macro",92],
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",79],
      "vtp_eventAction":["macro",93],
      "vtp_eventLabel":["macro",94],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":172
    },{
      "function":"__ua",
      "vtp_nonInteraction":true,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":["macro",84],
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",79],
      "vtp_eventAction":["macro",85],
      "vtp_eventLabel":["macro",86],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":173
    },{
      "function":"__zone",
      "once_per_event":true,
      "vtp_childContainers":["list",["map","publicId","GTM-5CVQBG","nickname","CGC Container"]],
      "vtp_boundaries":["list",["zb","_cn",["macro",6],"login.e2e.apigee.net",false,false]],
      "vtp_enableTypeRestrictions":false,
      "tag_id":174
    },{
      "function":"__cl",
      "tag_id":175
    },{
      "function":"__lcl",
      "vtp_waitForTags":true,
      "vtp_checkValidation":false,
      "vtp_waitForTagsTimeout":"2000",
      "vtp_uniqueTriggerId":"268073_60",
      "tag_id":176
    },{
      "function":"__cl",
      "tag_id":177
    },{
      "function":"__cl",
      "tag_id":178
    },{
      "function":"__cl",
      "tag_id":179
    },{
      "function":"__cl",
      "tag_id":180
    },{
      "function":"__cl",
      "tag_id":181
    },{
      "function":"__cl",
      "tag_id":182
    },{
      "function":"__cl",
      "tag_id":183
    },{
      "function":"__cl",
      "tag_id":184
    },{
      "function":"__cl",
      "tag_id":185
    },{
      "function":"__cl",
      "tag_id":186
    },{
      "function":"__evl",
      "vtp_elementId":"hly-message",
      "vtp_useOnScreenDuration":false,
      "vtp_useDomChangeListener":true,
      "vtp_firingFrequency":"ONCE",
      "vtp_selectorType":"ID",
      "vtp_onScreenRatio":"5",
      "vtp_uniqueTriggerId":"268073_335",
      "tag_id":187
    },{
      "function":"__lcl",
      "vtp_waitForTags":true,
      "vtp_checkValidation":false,
      "vtp_waitForTagsTimeout":"500",
      "vtp_uniqueTriggerId":"268073_375",
      "tag_id":188
    },{
      "function":"__html",
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Evar gtm=document.getElementById(\"gtm\");gtm.innerHTML='\\x3cdiv class\\x3d\"sso-container\" style\\x3d\"margin-top: 2.5em;border: 1px solid #CFCFCF;padding: 20px 0;border-radius: 3px;text-align:center\"\\x3eHave questions? Try \\x3ca href\\x3d\"https:\/\/community.apigee.com\" target\\x3d\"_blank\" title\\x3d\"Community Forums\"\\x3ecommunity.apigee.com\\x3c\/a\\x3e.\\x3c\/div\\x3e';\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":6
    },{
      "function":"__html",
      "vtp_html":"\u003Cscript\u003Econsole.log(\"header click event happened\");\u003C\/script\u003E",
      "vtp_supportDocumentWrite":true,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "vtp_usePostscribe":true,
      "tag_id":9
    },{
      "function":"__html",
      "vtp_html":"\u003Cscript\u003E$.getJSON(location.host+\"\/status\",function(a){$('\\x3cdiv style\\x3d\"margin: auto\"\\x3e\\x3ch3\\x3eNon-production planet status: \\x3c\/h3\\x3e\\x3cul style\\x3d\"text-decoration:none;\"\\x3e\\x3cli\\x3eAPI Platform: '+a.userStoreStatus.apiPlatform+\"\\x3c\/li\\x3e\\x3cli\\x3eUsergrid: \"+a.userStoreStatus.usergrid+\"\\x3c\/li\\x3e\\x3cli\\x3eVersion: \"+a.version+\"\\x3c\/li\\x3e\\x3c\/ul\\x3e\\x3c\/div\\x3e\").appendTo(\"#gtm\")});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":true,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "vtp_usePostscribe":true,
      "tag_id":10
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Evar BugHerdConfig={reporter:{email:",["escape",["macro",4],8,16],",required:\"true\"},metadata:{apigee_user:",["escape",["macro",4],8,16],",webapp:",["escape",["macro",10],8,16],"},feedback:{tab_text:\"Feedback\",option_title_text:\"Is your feedback about\\u2026\",option_pin_text:\"a specific part of this page\",option_site_text:\"the whole page or site\",pin_instruction_text:\"Hover over an element to highlight it, then click to give feedback about it.\",feedback_entry_placeholder:\"Write your comments here. (Need immediate support? Use the support portal instead.)\",\nfeedback_email_placeholder:\"your email address\",feedback_submit_text:\"Send Feedback\",confirm_success_text:\"Thanks, your feedback helps us improve.\",confirm_loading_text:\"Sending feedback\\u2026\",confirm_close_text:\"Close\",confirm_error_text:\"Sending feedback was unsuccessful.\",confirm_retry_text:\"Try Again\",confirm_extension_text:\"Did you know you can send a screenshot with your feedback?\",confirm_extension_link_text:\"Find out how.\",hide:!0}},feedbackcss=document.createElement(\"style\");\nfeedbackcss.type=\"text\/css\";feedbackcss.innerHTML=\"#navfeedback { display: block!important }\";document.body.appendChild(feedbackcss);(function(a,c){var b=a.createElement(c),d=a.getElementsByTagName(c)[0];b.type=\"text\/javascript\";b.src=\"\/\/www.bugherd.com\/sidebarv2.js?apikey\\x3dvypwx4bul51wpyxzlyzdsa\";d.parentNode.insertBefore(b,d)})(document,\"script\");\ndocument.body.addEventListener(\"sidebarAttached\",function(a){if(feedback_menu_item=document.getElementById(\"navfeedback\"))feedback_menu_item.style.display=\"block\"});\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":14
    },{
      "function":"__html",
      "vtp_html":"\u003Cscript\u003E$(\"label\").append(\"*\");$(\":input\").blur(function(){0\u003C$(this).val().length?_gaq.push([\"_trackEvent\",\"SSO_Signup_form\",\"Completed field\",$(this).attr(\"name\")]):_gaq.push([\"_trackEvent\",\"SSO_Signup_form\",\"Skipped field\",$(this).attr(\"name\")])});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":true,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "vtp_usePostscribe":true,
      "tag_id":38
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E(function(a,b,c,d,e){a=b.createElement(c);b=b.getElementsByTagName(c)[0];a.async=1;a.id=e;a.src=(\"https:\"==document.location.protocol?\"https:\/\/\":\"http:\/\/\")+d;b.parentNode.insertBefore(a,b)})(window,document,\"script\",\"scripts.demandbase.com\/ynoqwj6n.min.js\",\"demandbase_js_lib\");\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":42
    },{
      "function":"__html",
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E\/*\n @preserve\n jquery.scrolldepth.js | v0.4.1\n Copyright (c) 2014 Rob Flaherty (@robflaherty)\n Licensed under the MIT and GPL licenses.\n*\/\n(function(c,h,m,u){var n={elements:[],minHeight:0,percentage:!0,testing:!1},k=c(h),f=[];c.scrollDepth=function(d){function l(a,b,e){d.testing?c(\"#console\").html(a+\": \"+b):\"undefined\"!==typeof dataLayer?(dataLayer.push({event:\"ScrollDistance\",eventCategory:\"Scroll Depth\",eventAction:a,eventLabel:b,eventValue:1,eventNonInteraction:!0}),2\u003Carguments.length\u0026\u0026dataLayer.push({event:\"ScrollTiming\",eventCategory:\"Scroll Depth\",eventAction:a,eventLabel:b,eventTiming:e})):(\"undefined\"!==typeof ga\u0026\u0026(ga(\"send\",\n\"event\",\"Scroll Depth\",a,b,1,{nonInteraction:1}),2\u003Carguments.length\u0026\u0026ga(\"send\",\"timing\",\"Scroll Depth\",a,e,b)),\"undefined\"!==typeof _gaq\u0026\u0026(_gaq.push([\"_trackEvent\",\"Scroll Depth\",a,b,1,!0]),2\u003Carguments.length\u0026\u0026_gaq.push([\"_trackTiming\",\"Scroll Depth\",a,e,b,100])))}function p(a,b,e){c.each(a,function(a,g){-1===c.inArray(a,f)\u0026\u0026b\u003E=g\u0026\u0026(l(\"Percentage\",a,e),f.push(a))})}function q(a,b,e){c.each(a,function(a,g){-1===c.inArray(g,f)\u0026\u0026c(g).length\u0026\u0026b\u003E=c(g).offset().top\u0026\u0026(l(\"Elements\",g,e),f.push(g))})}function r(a,\nb){var e,c,g,d=null,f=0,k=function(){f=new Date;d=null;g=a.apply(e,c)};return function(){var h=new Date;f||(f=h);var l=b-(h-f);e=this;c=arguments;0\u003E=l?(clearTimeout(d),d=null,f=h,g=a.apply(e,c)):d||(d=setTimeout(k,l));return g}}var t=+new Date;d=c.extend({},n,d);c(m).height()\u003Cd.minHeight||(l(\"Percentage\",\"Baseline\"),k.on(\"scroll.scrollDepth\",r(function(){var a=c(m).height(),b=h.innerHeight?h.innerHeight:k.height();b=k.scrollTop()+b;a={\"25%\":parseInt(.25*a,10),\"50%\":parseInt(.5*a,10),\"75%\":parseInt(.75*\na,10),\"100%\":a-1};var e=+new Date-t;f.length\u003E=4+d.elements.length?k.off(\"scroll.scrollDepth\"):(d.elements\u0026\u0026q(d.elements,b,e),d.percentage\u0026\u0026p(a,b,e))},500)))}})(jQuery,window,document);jQuery.scrollDepth();\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":59
    },{
      "function":"__html",
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E(function(b){var a=b.createElement(\"script\");a.src=\"https:\/\/widget.bugherd.com\/v1.js\";a.textContent='{\"endpoint\":\"https:\/\/app.getstack.io\/bugherd\",\"projectId\":4166}';b.body.appendChild(a)})(document);\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":65
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E$($(\"#marketing_content\")).css(\"background-image\",\"url(https:\/\/apigee.com\/about\/sites\/mktg-new\/files\/dev_0828.jpg)\").css(\"background-size\",\"100% 100%\");\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":75
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E$(\".page-header h1\")[0].innerHTML=\"Create your Apigee account.\";$(\".username_indication\")[0].innerHTML=\"An organization will be created with your username. You can create more orgs later.\";\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":78
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cstyle\u003E\n#marketing_content {\n    display:none;\n  }\n\u003C\/style\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":82
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003EsharperEdge=jQuery(\"#menu-2361-1 a\");sharperEdge.attr(\"href\",sharperEdge.attr(\"href\")+\"?intent\\x3dmanagement\");\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":88
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Elocation.href.match(\"intent\\x3dmanagement\")\u0026\u0026(localStorage.signupIntent=\"management\");\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":89
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cstyle\u003E\n    .progress-bar {\n        min-height: 100px;\n        border-left: 3px dotted #8C8C8C;\n        margin-left: 35px;\n    }\n\n    .progress-bar.progress-bar1 {\n        min-height: 120px;\n    }\n\n    .progress-bar.progress-bar2 {\n        min-height: 155px;\n    }\n\n    .progress-bar.progress-bar4 {\n        min-height: 110px;\n    }\n\n    .welcome-intent-api {\n        float: left;\n        margin-top: 110px;\n        margin-left: 40px;\n    }\n\n    i.fa {\n        float: left;\n        display: block;\n        font-size: 18px;\n        padding-right: 10px;\n    \tcolor: #FA3F00;\n    }\n\n    i.fa.fa-list {\n        padding-right: 1px;\n    }\n  \n\ti.fa.fa-lock {\n    \tpadding-right: 20px;\n\t}\n\n    .intent-wrapper {\n        padding-top: 5px;\n        padding-bottom: 25px;\n    }\n\n    .intent-title {\n        float: left;\n        font-size: 15px;\n    }\n\n\u003C\/style\u003E  \n\u003Cscript data-gtmsrc=\"https:\/\/use.fontawesome.com\/aec1e358ba.js\" type=\"text\/gtmscript\"\u003E\u003C\/script\u003E\n\u003Cscript type=\"text\/gtmscript\"\u003Efunction getQueryStringValue(a){return unescape(window.location.search.replace(new RegExp(\"^(?:.*[\\x26\\\\?]\"+escape(a).replace(\/[\\.\\+\\*]\/g,\"\\\\$\\x26\")+\"(?:\\\\\\x3d([^\\x26]*))?)?.*$\",\"i\"),\"$1\"))}\nif(\"management\"==getQueryStringValue(\"intent\")){var intentInfo=$('\\x3cdiv class\\x3d\"welcome-intent-api\"\\x3e \\x3cdiv class\\x3d\"intent-wrapper develop-apis\"\\x3e \\x3ci class\\x3d\"fa fa-cogs\" aria-hidden\\x3d\"true\"\\x3e\\x3c\/i\\x3e \\x3cdiv class\\x3d\"intent-title\"\\x3eDevelop APIs\\x3c\/div\\x3e \\x3c\/div\\x3e \\x3cdiv class\\x3d\"progress-bar progress-bar1\"\\x3e\\x3c\/div\\x3e \\x3cdiv class\\x3d\"intent-wrapper manage-apis\"\\x3e \\x3ci class\\x3d\"fa fa-list\" aria-hidden\\x3d\"true\"\\x3e\\x3c\/i\\x3e \\x3ci class\\x3d\"fa fa-cog\" aria-hidden\\x3d\"true\"\\x3e\\x3c\/i\\x3e \\x3cdiv class\\x3d\"intent-title\"\\x3eManage APIs\\x3c\/div\\x3e \\x3c\/div\\x3e \\x3cdiv class\\x3d\"progress-bar progress-bar2\"\\x3e\\x3c\/div\\x3e \\x3cdiv class\\x3d\"intent-wrapper secure-apis\"\\x3e \\x3ci class\\x3d\"fa fa-lock\" aria-hidden\\x3d\"true\"\\x3e\\x3c\/i\\x3e \\x3cdiv class\\x3d\"intent-title\"\\x3eSecure APIs\\x3c\/div\\x3e \\x3c\/div\\x3e \\x3cdiv class\\x3d\"progress-bar progress-bar3\"\\x3e\\x3c\/div\\x3e \\x3cdiv class\\x3d\"intent-wrapper dev-onboard\"\\x3e \\x3ci class\\x3d\"fa fa-user-plus\" aria-hidden\\x3d\"true\"\\x3e\\x3c\/i\\x3e \\x3cdiv class\\x3d\"intent-title\"\\x3eOnboard Developers\\x3c\/div\\x3e \\x3c\/div\\x3e \\x3cdiv class\\x3d\"progress-bar progress-bar4\"\\x3e\\x3c\/div\\x3e \\x3cdiv class\\x3d\"intent-wrapper track-analytics\"\\x3e \\x3ci class\\x3d\"fa fa-line-chart\" aria-hidden\\x3d\"true\"\\x3e\\x3c\/i\\x3e \\x3cdiv class\\x3d\"intent-title\"\\x3eTrack Analytics\\x3c\/div\\x3e \\x3c\/div\\x3e \\x3c\/div\\x3e');$(\"body \\x3e div#container \\x3e div.content\").prepend(intentInfo);\n$(\":input\").blur(function(){0\u003C$(\"input#userName\").val().length\u0026\u0026$(\".progress-bar.progress-bar1\").css(\"border-color\",\"#FA3F00\");0\u003C$(\"input#company\").val().length\u0026\u0026$(\".progress-bar.progress-bar2\").css(\"border-color\",\"#FA3F00\");0\u003C$(\"input#password\").val().length\u0026\u0026$(\".progress-bar.progress-bar3\").css(\"border-color\",\"#FA3F00\");0\u003C$(\"input#confirmedPassword\").val().length\u0026\u0026$(\".progress-bar.progress-bar4\").css(\"border-color\",\"#FA3F00\")})};\u003C\/script\u003E  ",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":101
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Ewindow.onload=function(){\"management\"==localStorage.signupIntent\u0026\u0026($(\"a#apiManagementLink\").css({\"box-shadow\":\"15px 15px 15px rgba(32, 107, 134, 0.41)\"}),$(\"#apiManagementLink .topProduct.apiPlat\").css({height:\"20px\"}),$(\"#apiManagementLink .openProduct.apiPlat\").css({height:\"60px\"}),$(\"a#apiManagementLink\").hover(function(){$(this).css({\"box-shadow\":\"20px 20px 20px rgba(32, 107, 134, 0.41)\"})},function(){$(this).css({\"box-shadow\":\"15px 15px 15px rgba(32, 107, 134, 0.41)\"})}))};\u003C\/script\u003E  ",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":102
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003EsharperEdge=jQuery(\".node-developer-page .field-name-field-curated-item div.even a\");sharperEdge.attr(\"href\",sharperEdge.attr(\"href\")+\"?intent\\x3dmanagement\");sharperEdgeMenu=jQuery(\"#menu-2361-1 a\");sharperEdgeMenu.attr(\"href\",sharperEdgeMenu.attr(\"href\")+\"?intent\\x3dmanagement\");\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":103
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cstyle\u003E\n.redactor_editor, .redactor_editor:focus {\n  min-height: 200px;  \n  max-height: 300px;\n  overflow: scroll;\n}\np.required-note {\n  padding-top: 10px;\n  color: red;\n}\n\u003C\/style\u003E\n\u003Cscript type=\"text\/gtmscript\"\u003E$(\"#title\").attr(\"maxlength\",\"140\");$(\"#title\").siblings(\"label\").text(\"Subject:\");$(\"#title\").attr(\"placeholder\",\"Type a quick summary of your question here\");$(\"#title\").after(\"\\x3cp class\\x3d'title-note'\\x3eMaximum 140 characters allowed in title.\");$(\".redactor_box\").parent().prepend('\\x3cdiv class\\x3d\"qdescription\"\\x3e\\x3clabel\\x3eYour Question: \\x3c\/label\\x3e\\x3c\/div\\x3e');$(\".controls .redactor_box\").after(\"\\x3cp class\\x3d'required-note'\\x3ePlease enter minimum 60 characters in description to post a question.\");\n$(\"#submit-question\").prop(\"disabled\",!0);$(\".redactor_redactor.redactor_editor\").bind(\"DOMSubtreeModified\",function(){60\u003C$(\".redactor\").val().length?$(\"#submit-question\").prop(\"disabled\",!1):$(\"#submit-question\").prop(\"disabled\",!0)});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":109
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript src=\"https:\/\/code.jquery.com\/ui\/1.12.0\/jquery-ui.min.js\"\u003E\u003C\/script\u003E\n\u003Clink rel=\"stylesheet\" href=\"https:\/\/code.jquery.com\/ui\/1.12.1\/themes\/base\/jquery-ui.css\"\u003E\n\u003Cstyle\u003E\n  button.saveButtonClass.ui-button.ui-corner-all.ui-widget {\n    background: #fc4c02;\n    color: #ffffff;\n  }\n\u003C\/style\u003E  \n\u003Cscript\u003Evar quesAuthorName=$(\"div.question.post img.gravatar\").attr(\"title\"),answerAuthorName=$(\".widget.answer-form-widget img.gravatar\").attr(\"title\");quesAuthorName==answerAuthorName\u0026\u0026($(\"#answer-form\").hide(),$(\"#answer-form\").after('\\x3cdiv id\\x3d\"dialog-button\" class\\x3d\"popup\"\\x3e\\x3cinput class\\x3d\"btn btn-primary\" type\\x3d\"button\" value\\x3d\"Answer Your Question\"\\x3e\\x3c\/div\\x3e'),$(\"#answer-form\").after('\\x3cdiv id\\x3d\"dialog-confirm\" title\\x3d\"Are you sure you want to answer your question?\"\\x3e\\x3cp\\x3e\\x3cspan class\\x3d\"ui-icon ui-icon-alert\"\\x3e\\x3c\/span\\x3e\\x3cul\\x3e\\x3cli\\x3e\\x3cstrong\\x3eComment\\x3c\/strong\\x3e if you are trying to respond to an answer.\\x3c\/li\\x3e \\x3cli\\x3e\\x3cstrong\\x3eEdit your question\\x3c\/strong\\x3e if you need to add more details. \\x3c\/li\\x3e\\x3c\/ul\\x3e\\x3c\/p\\x3e\\x3c\/div\\x3e'));\n$(\"#dialog-confirm\").dialog({resizable:!1,height:\"auto\",width:\"400px\",autoOpen:!1,modal:!0,buttons:[{text:\"Cancel\",\"class\":\"cancelButtonClass\",click:function(){$(this).dialog(\"close\")}},{text:\"Yes, I want to post an answer\",\"class\":\"saveButtonClass\",click:function(){$(this).dialog(\"close\");$(\"#dialog-button\").hide();$(\"#answer-form\").show()}}]});$(\"#dialog-button\").click(function(){$(\"#dialog-confirm\").dialog(\"open\")});\u003C\/script\u003E  ",
      "vtp_supportDocumentWrite":true,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "vtp_usePostscribe":true,
      "tag_id":110
    },{
      "function":"__html",
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Evar BugHerdConfig={reporter:{email:",["escape",["macro",4],8,16],",required:\"true\"},metadata:{apigee_user:",["escape",["macro",4],8,16],",webapp:",["escape",["macro",10],8,16],"}};(function(b,c){var a=b.createElement(c),d=b.getElementsByTagName(c)[0];a.type=\"text\/javascript\";a.src=\"\/\/www.bugherd.com\/sidebarv2.js?apikey\\x3dlzttx2nak3wx4vvn7eombq\";d.parentNode.insertBefore(a,d)})(document,\"script\");\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":113
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Evar BugHerdConfig={reporter:{email:",["escape",["macro",4],8,16],",required:\"true\"},metadata:{apigee_user:",["escape",["macro",4],8,16],",webapp:",["escape",["macro",10],8,16],"},feedback:{tab_text:\"Feedback\",option_title_text:\"Is your feedback about\\u2026\",option_pin_text:\"a specific part of this page\",option_site_text:\"the whole page or site\",pin_instruction_text:\"Hover over an element to highlight it, then click to give feedback about it.\",feedback_entry_placeholder:\"Write your comments here. (Need immediate support? Use the support portal instead.)\",\nfeedback_email_placeholder:\"your email address\",feedback_submit_text:\"Send Feedback\",confirm_success_text:\"Thanks, your feedback helps us improve.\",confirm_loading_text:\"Sending feedback\\u2026\",confirm_close_text:\"Close\",confirm_error_text:\"Sending feedback was unsuccessful.\",confirm_retry_text:\"Try Again\",confirm_extension_text:\"Did you know you can send a screenshot with your feedback?\",confirm_extension_link_text:\"Find out how.\"}};\n(function(b,c){var a=b.createElement(c),d=b.getElementsByTagName(c)[0];a.type=\"text\/javascript\";a.src=\"\/\/www.bugherd.com\/sidebarv2.js?apikey\\x3dxlbxnpoucfo3s2n3hjbulg\";d.parentNode.insertBefore(a,d)})(document,\"script\");\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":124
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Evar BugHerdConfig={reporter:{email:",["escape",["macro",4],8,16],",required:\"true\"},metadata:{apigee_user:",["escape",["macro",4],8,16],",webapp:",["escape",["macro",10],8,16],"},feedback:{tab_text:\"Feedback\",option_title_text:\"Is your feedback about\\u2026\",option_pin_text:\"a specific part of this page\",option_site_text:\"the whole page or site\",pin_instruction_text:\"Hover over an element to highlight it, then click to give feedback about it.\",feedback_entry_placeholder:\"Write your comments here. (Need immediate support? Use the support portal instead.)\",\nfeedback_email_placeholder:\"your email address\",feedback_submit_text:\"Send Feedback\",confirm_success_text:\"Thanks, your feedback helps us improve.\",confirm_loading_text:\"Sending feedback\\u2026\",confirm_close_text:\"Close\",confirm_error_text:\"Sending feedback was unsuccessful.\",confirm_retry_text:\"Try Again\",confirm_extension_text:\"Did you know you can send a screenshot with your feedback?\",confirm_extension_link_text:\"Find out how.\",hide:!0}},feedbackcss=document.createElement(\"style\");\nfeedbackcss.type=\"text\/css\";feedbackcss.innerHTML=\"#navfeedback { display: block!important }\";document.body.appendChild(feedbackcss);(function(a,c){var b=a.createElement(c),d=a.getElementsByTagName(c)[0];b.type=\"text\/javascript\";b.src=\"\/\/www.bugherd.com\/sidebarv2.js?apikey\\x3dvypwx4bul51wpyxzlyzdsa\";d.parentNode.insertBefore(b,d)})(document,\"script\");\ndocument.body.addEventListener(\"sidebarAttached\",function(a){if(feedback_menu_item=document.getElementById(\"navfeedback\"))feedback_menu_item.style.display=\"block\"});\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":129
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E(function(){function b(){!1===c\u0026\u0026(c=!0,Munchkin.init(\"351-WXY-166\",{asyncOnly:!0,disableClickDelay:!0}))}var c=!1,a=document.createElement(\"script\");a.type=\"text\/javascript\";a.async=!0;a.src=\"\/\/munchkin.marketo.net\/143\/munchkin.js\";a.onreadystatechange=function(){\"complete\"!=this.readyState\u0026\u0026\"loaded\"!=this.readyState||b()};a.onload=b;document.getElementsByTagName(\"head\")[0].appendChild(a)})();\u003C\/script\u003E\n",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":136
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\" data-gtmsrc=\"https:\/\/support.google.com\/inapp\/api.js\"\u003E\u003C\/script\u003E\n\u003Cscript type=\"text\/gtmscript\"\u003Euserfeedback.CONFIG={productId:\"1636213\",authuser:void 0,bucket:\"",["escape",["macro",10],7],"\",productVersion:\"",["escape",["macro",33],7],"\",enableAnonymousFeedback:!0};",["escape",["macro",5],8,16],"\u0026\u0026(userfeedback.CONFIG.enableAnonymousFeedback=!1);userfeedback.DATA={userEmail:\"",["escape",["macro",4],7],"\",organization:\"",["escape",["macro",31],7],"\",orgType:\"",["escape",["macro",32],7],"\"};function feedbackReport(){userfeedback.api.startFeedback(userfeedback.CONFIG,userfeedback.DATA)};\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":142
    }],
  "predicates":[{
      "function":"_eq",
      "arg0":["macro",6],
      "arg1":"apigee.com"
    },{
      "function":"_sw",
      "arg0":["macro",7],
      "arg1":"\/about"
    },{
      "function":"_eq",
      "arg0":["macro",8],
      "arg1":"gtm.js"
    },{
      "function":"_cn",
      "arg0":["macro",9],
      "arg1":"community.apigee.com"
    },{
      "function":"_eq",
      "arg0":["macro",8],
      "arg1":"gtm.dom"
    },{
      "function":"_re",
      "arg0":["macro",9],
      "arg1":"apigee.com\/docs|docs.apigee.com"
    },{
      "function":"_cn",
      "arg0":["macro",9],
      "arg1":"developers.apigee.com"
    },{
      "function":"_cn",
      "arg0":["macro",9],
      "arg1":"pages.apigee.com"
    },{
      "function":"_cn",
      "arg0":["macro",6],
      "arg1":"marketo"
    },{
      "function":"_cn",
      "arg0":["macro",6],
      "arg1":"iloveapis.com"
    },{
      "function":"_eq",
      "arg0":["macro",6],
      "arg1":"academy.apigee.com"
    },{
      "function":"_re",
      "arg0":["macro",9],
      "arg1":"^accounts.apigee.com\/accounts\/sign_in|^login.apigee.com\/login",
      "ignore_case":true
    },{
      "function":"_cn",
      "arg0":["macro",9],
      "arg1":"globalapichallenge.com"
    },{
      "function":"_eq",
      "arg0":["macro",6],
      "arg1":"status.apigee.com"
    },{
      "function":"_cn",
      "arg0":["macro",6],
      "arg1":"investors.apigee.com"
    },{
      "function":"_eq",
      "arg0":["macro",6],
      "arg1":"apistudio.io"
    },{
      "function":"_eq",
      "arg0":["macro",6],
      "arg1":"360.apigee.com"
    },{
      "function":"_eq",
      "arg0":["macro",6],
      "arg1":"opensource.apigee.com"
    },{
      "function":"_eq",
      "arg0":["macro",6],
      "arg1":"healthapix.apigee.com"
    },{
      "function":"_cn",
      "arg0":["macro",6],
      "arg1":"london.iloveapis.com"
    },{
      "function":"_cn",
      "arg0":["macro",6],
      "arg1":"citytours.apigee.com"
    },{
      "function":"_cn",
      "arg0":["macro",6],
      "arg1":"openbank.apigee.com"
    },{
      "function":"_eq",
      "arg0":["macro",6],
      "arg1":"adapt.apigee.com"
    },{
      "function":"_eq",
      "arg0":["macro",6],
      "arg1":"movie.apigee.com"
    },{
      "function":"_eq",
      "arg0":["macro",10],
      "arg1":"static"
    },{
      "function":"_eq",
      "arg0":["macro",10],
      "arg1":"magellan"
    },{
      "function":"_cn",
      "arg0":["macro",6],
      "arg1":"mktg-dev.apigee.com"
    },{
      "function":"_cn",
      "arg0":["macro",6],
      "arg1":"mktg-stg.apigee.com"
    },{
      "function":"_eq",
      "arg0":["macro",8],
      "arg1":"docs"
    },{
      "function":"_re",
      "arg0":["macro",7],
      "arg1":"e_tail|retail|walgreens|target|carscom|7-eleven",
      "ignore_case":true
    },{
      "function":"_re",
      "arg0":["macro",9],
      "arg1":"apigee.com\/about|pages.apigee.com|blog.apigee.com"
    },{
      "function":"_re",
      "arg0":["macro",10],
      "arg1":"^eventsite$|^marketing$"
    },{
      "function":"_cn",
      "arg0":["macro",9],
      "arg1":"login.e2e.apigee.net"
    },{
      "function":"_re",
      "arg0":["macro",8],
      "arg1":".*"
    },{
      "function":"_eq",
      "arg0":["macro",8],
      "arg1":"Demandbase_Loaded"
    },{
      "function":"_cn",
      "arg0":["macro",6],
      "arg1":"apigee.com"
    },{
      "function":"_sw",
      "arg0":["macro",7],
      "arg1":"\/docs"
    },{
      "function":"_eq",
      "arg0":["macro",8],
      "arg1":"docsAjaxLoad"
    },{
      "function":"_ew",
      "arg0":["macro",6],
      "arg1":"apigee.com"
    },{
      "function":"_cn",
      "arg0":["macro",9],
      "arg1":"apigee.com\/about\/press-releases-raw"
    },{
      "function":"_eq",
      "arg0":["macro",8],
      "arg1":"interaction"
    },{
      "function":"_eq",
      "arg0":["macro",8],
      "arg1":"content-view"
    },{
      "function":"_cn",
      "arg0":["macro",9],
      "arg1":"apigee.com\/apimanagement"
    },{
      "function":"_cn",
      "arg0":["macro",51],
      "arg1":"pages.apigee.com"
    },{
      "function":"_eq",
      "arg0":["macro",8],
      "arg1":"gtm.click"
    },{
      "function":"_cn",
      "arg0":["macro",51],
      "arg1":"apigee.com\/"
    },{
      "function":"_cn",
      "arg0":["macro",51],
      "arg1":"cloudfront.net\/"
    },{
      "function":"_re",
      "arg0":["macro",51],
      "arg1":".+",
      "ignore_case":true
    },{
      "function":"_eq",
      "arg0":["macro",8],
      "arg1":"gtm.linkClick"
    },{
      "function":"_re",
      "arg0":["macro",52],
      "arg1":"(^$|((^|,)268073_60($|,)))"
    },{
      "function":"_cn",
      "arg0":["macro",51],
      "arg1":"community.apigee.com"
    },{
      "function":"_cn",
      "arg0":["macro",6],
      "arg1":"e2e.apigee.net"
    },{
      "function":"_eq",
      "arg0":["macro",6],
      "arg1":"enterprise.apigee.com"
    },{
      "function":"_eq",
      "arg0":["macro",6],
      "arg1":"edge.apigee.com"
    },{
      "function":"_cn",
      "arg0":["macro",9],
      "arg1":"mktg-new.local"
    },{
      "function":"_cn",
      "arg0":["macro",51],
      "arg1":"cloudfront.net"
    },{
      "function":"_eq",
      "arg0":["macro",6],
      "arg1":"acceleratorawards.com"
    },{
      "function":"_eq",
      "arg0":["macro",6],
      "arg1":"apiallthethings.com"
    },{
      "function":"_cn",
      "arg0":["macro",9],
      "arg1":"cioupload.com"
    },{
      "function":"_cn",
      "arg0":["macro",9],
      "arg1":"apigee.com\/about"
    },{
      "function":"_eq",
      "arg0":["macro",6],
      "arg1":"health.apigee.com"
    },{
      "function":"_eq",
      "arg0":["macro",10],
      "arg1":"management"
    },{
      "function":"_eq",
      "arg0":["macro",10],
      "arg1":"pinpoint"
    },{
      "function":"_eq",
      "arg0":["macro",10],
      "arg1":"proxies"
    },{
      "function":"_eq",
      "arg0":["macro",10],
      "arg1":"developers"
    },{
      "function":"_eq",
      "arg0":["macro",10],
      "arg1":"products"
    },{
      "function":"_eq",
      "arg0":["macro",10],
      "arg1":"apps"
    },{
      "function":"_eq",
      "arg0":["macro",10],
      "arg1":"history"
    },{
      "function":"_sw",
      "arg0":["macro",7],
      "arg1":"\/admin\/"
    },{
      "function":"_cn",
      "arg0":["macro",6],
      "arg1":"community.apigee.com"
    },{
      "function":"_cn",
      "arg0":["macro",6],
      "arg1":"enterprise.apigee.com"
    },{
      "function":"_eq",
      "arg0":["macro",10],
      "arg1":"portal2"
    },{
      "function":"_eq",
      "arg0":["macro",10],
      "arg1":"docstore"
    },{
      "function":"_re",
      "arg0":["macro",9],
      "arg1":"mktg-dev.apigee.com\/docs\/|docs-dev.apigee.com"
    },{
      "function":"_re",
      "arg0":["macro",9],
      "arg1":"mktg-stg.apigee.com\/docs\/|docs-stg.apigee.com",
      "ignore_case":true
    },{
      "function":"_re",
      "arg0":["macro",9],
      "arg1":"apigee.com\/docs\/|docs.apigee.com"
    },{
      "function":"_cn",
      "arg0":["macro",9],
      "arg1":"docs.local"
    },{
      "function":"_re",
      "arg0":["macro",9],
      "arg1":".*"
    },{
      "function":"_cn",
      "arg0":["macro",58],
      "arg1":"dbase-personal"
    },{
      "function":"_eq",
      "arg0":["macro",8],
      "arg1":"DB_EXP_LOADED"
    },{
      "function":"_cn",
      "arg0":["macro",51],
      "arg1":"apigee.com\/about\/sites\/mktg-new\/files\/"
    },{
      "function":"_re",
      "arg0":["macro",51],
      "arg1":"jpeg|png|jpg"
    },{
      "function":"_eq",
      "arg0":["macro",10],
      "arg1":"baasportal"
    },{
      "function":"_eq",
      "arg0":["macro",10],
      "arg1":"studio"
    },{
      "function":"_eq",
      "arg0":["macro",10],
      "arg1":"edgex"
    },{
      "function":"_eq",
      "arg0":["macro",10],
      "arg1":"nucleus"
    },{
      "function":"_cn",
      "arg0":["macro",7],
      "arg1":"accounts\/sign_up"
    },{
      "function":"_eq",
      "arg0":["macro",60],
      "arg1":"(not set)"
    },{
      "function":"_eq",
      "arg0":["macro",61],
      "arg1":"(not set)"
    },{
      "function":"_eq",
      "arg0":["macro",62],
      "arg1":"(not set)"
    },{
      "function":"_eq",
      "arg0":["macro",8],
      "arg1":"timing"
    },{
      "function":"_cn",
      "arg0":["macro",7],
      "arg1":"\/cp1"
    },{
      "function":"_cn",
      "arg0":["macro",7],
      "arg1":"\/cp"
    },{
      "function":"_cn",
      "arg0":["macro",7],
      "arg1":"\/about"
    },{
      "function":"_eq",
      "arg0":["macro",6],
      "arg1":"events.withgoogle.com"
    },{
      "function":"_cn",
      "arg0":["macro",9],
      "arg1":"pages.apigee.com\/online-meetup-apigee-jan."
    },{
      "function":"_eq",
      "arg0":["macro",6],
      "arg1":"transform.apigee.com"
    },{
      "function":"_eq",
      "arg0":["macro",7],
      "arg1":"\/assessment"
    },{
      "function":"_eq",
      "arg0":["macro",6],
      "arg1":"pages.apigee.com"
    },{
      "function":"_re",
      "arg0":["macro",7],
      "arg1":"\/ebook-digital-transformation-journey-chart-your-path-forward-with-apigee-compass-conf.html",
      "ignore_case":true
    },{
      "function":"_cn",
      "arg0":["macro",7],
      "arg1":"report-2016-Gartner-magic-quadrant-conf.html"
    },{
      "function":"_cn",
      "arg0":["macro",58],
      "arg1":"hly-success-msg"
    },{
      "function":"_eq",
      "arg0":["macro",8],
      "arg1":"gtm.elementVisibility"
    },{
      "function":"_re",
      "arg0":["macro",52],
      "arg1":"(^$|((^|,)268073_335($|,)))"
    },{
      "function":"_re",
      "arg0":["macro",7],
      "arg1":"^\/results\/.*"
    },{
      "function":"_cn",
      "arg0":["macro",6],
      "arg1":"googleplex"
    },{
      "function":"_cn",
      "arg0":["macro",7],
      "arg1":"2018-I-Love-APIs-London-Registration-TY.html"
    },{
      "function":"_re",
      "arg0":["macro",7],
      "arg1":"2018-02-EMEA-FSI-CIO-Apigeeforfinance_2018-02-EMEA-FSI-CIO-Apigeeforfinance\\.html",
      "ignore_case":true
    },{
      "function":"_re",
      "arg0":["macro",90],
      "arg1":".+"
    },{
      "function":"_re",
      "arg0":["macro",6],
      "arg1":"mktg-stg|pages\\.apigee\\.com|events\\.withgoogle\\.com"
    },{
      "function":"_re",
      "arg0":["macro",8],
      "arg1":"gtm\\.js|content-view"
    },{
      "function":"_re",
      "arg0":["macro",92],
      "arg1":".+",
      "ignore_case":true
    },{
      "function":"_re",
      "arg0":["macro",93],
      "arg1":".+",
      "ignore_case":true
    },{
      "function":"_re",
      "arg0":["macro",52],
      "arg1":"(^$|((^|,)268073_375($|,)))"
    },{
      "function":"_eq",
      "arg0":["macro",8],
      "arg1":"trackEvent"
    },{
      "function":"_eq",
      "arg0":["macro",95],
      "arg1":"true"
    },{
      "function":"_sw",
      "arg0":["macro",7],
      "arg1":"\/accounts\/sign_in"
    },{
      "function":"_cn",
      "arg0":["macro",6],
      "arg1":"accounts"
    },{
      "function":"_eq",
      "arg0":["macro",6],
      "arg1":"appservices.apigee.com"
    },{
      "function":"_cn",
      "arg0":["macro",7],
      "arg1":"\/login"
    },{
      "function":"_cn",
      "arg0":["macro",58],
      "arg1":"navbar"
    },{
      "function":"_eq",
      "arg0":["macro",97],
      "arg1":"undefined"
    },{
      "function":"_sw",
      "arg0":["macro",7],
      "arg1":"\/accounts\/sign_up"
    },{
      "function":"_sw",
      "arg0":["macro",9],
      "arg1":"https:\/\/apigee.com\/edge"
    },{
      "function":"_cn",
      "arg0":["macro",9],
      "arg1":"apigee.com\/about\/"
    },{
      "function":"_re",
      "arg0":["macro",7],
      "arg1":"\/about\/products\/api-management|\/about\/pricing\/apigee-edge-pricing-features|\/about\/specifications\/apigee-edge",
      "ignore_case":true
    },{
      "function":"_cn",
      "arg0":["macro",7],
      "arg1":"accounts\/dashboard"
    },{
      "function":"_cn",
      "arg0":["macro",7],
      "arg1":"about\/developers"
    },{
      "function":"_sw",
      "arg0":["macro",9],
      "arg1":"https:\/\/community.apigee.com\/questions\/ask.html"
    },{
      "function":"_sw",
      "arg0":["macro",9],
      "arg1":"https:\/\/community.apigee.com\/questions\/"
    },{
      "function":"_re",
      "arg0":["macro",10],
      "arg1":"docstore|portal2"
    },{
      "function":"_eq",
      "arg0":["macro",8],
      "arg1":"Push Context"
    },{
      "function":"_eq",
      "arg0":["macro",98],
      "arg1":"management"
    }],
  "rules":[
    [["if",0,1,2],["add",0,1,3,8,11,33,46],["block",33]],
    [["if",3,4],["add",0,1,3,8,14,46]],
    [["if",2,5],["add",0,1,3,8,46]],
    [["if",2,6],["add",0,3,8,46]],
    [["if",4,7],["add",0,2,3,8,14,46]],
    [["if",2,8],["add",0,2]],
    [["if",2,9],["add",1,16,49]],
    [["if",2,10],["add",1,3,8,46]],
    [["if",2,11],["add",3,8,46,92]],
    [["if",2,12],["add",3,46]],
    [["if",2,13],["add",3,8,46]],
    [["if",2,14],["add",3,8,46]],
    [["if",2,15],["add",3,16,46,49]],
    [["if",2,16],["add",3,46]],
    [["if",2,17],["add",3,8,46]],
    [["if",2,18],["add",3,8,46]],
    [["if",2,19],["add",3,8,46]],
    [["if",2,20],["add",3,8,46]],
    [["if",2,21],["add",3,8,46]],
    [["if",2,22],["add",3,8,46]],
    [["if",2,23],["add",3,8,46]],
    [["if",2,24],["add",3,8,46]],
    [["if",2,25],["add",3,8,46]],
    [["if",2,26],["add",4,23]],
    [["if",2,27],["add",4,5,8,11]],
    [["if",28],["add",6,43]],
    [["if",2,29,30],["add",7]],
    [["if",2,31],["add",8]],
    [["if",34],["add",9,52]],
    [["if",35,36,37],["add",10,44]],
    [["if",40],["add",12,21,39]],
    [["if",41],["add",13,22,40]],
    [["if",4,42],["add",14]],
    [["if",43,44],["add",15,45]],
    [["if",47,48,49],["unless",45,46],["add",15,45]],
    [["if",44,50],["add",15,45]],
    [["if",2,56],["add",16,49]],
    [["if",2,57],["add",16,49]],
    [["if",2,58],["add",16,49]],
    [["if",4,59],["add",17,30]],
    [["if",2,60],["add",18,50]],
    [["if",2,61],["add",18,20,50]],
    [["if",2,62],["add",18,20,50]],
    [["if",2,63],["add",18,20,50]],
    [["if",2,64],["add",18,20,50]],
    [["if",2,65],["add",18,20,50]],
    [["if",2,66],["add",18,20,50]],
    [["if",2,67],["add",18,20,50]],
    [["if",2,68],["unless",69,70],["add",19,48]],
    [["if",2,71],["add",20]],
    [["if",2,72],["add",20]],
    [["if",2,73],["add",24]],
    [["if",2,74],["add",24]],
    [["if",2,75],["add",24]],
    [["if",2,76],["add",24]],
    [["if",44,78],["add",25]],
    [["if",79],["add",26]],
    [["if",44,80],["unless",81],["add",27,51]],
    [["if",2,82],["add",28,29,47]],
    [["if",2,83],["add",28,29,47]],
    [["if",2,84],["add",28,29,47,101]],
    [["if",2,85],["add",29,32,47,100]],
    [["if",4,86],["add",31]],
    [["if",4,61],["add",33],["block",33]],
    [["if",44],["unless",87,88,89],["add",34,42]],
    [["if",90],["add",35,36,37,41],["block",36,37]],
    [["if",4,31],["unless",91,92],["add",38]],
    [["if",0,4,93],["unless",91,92],["add",38]],
    [["if",2,94],["add",46]],
    [["if",2,95],["add",53]],
    [["if",2,96,97],["add",54]],
    [["if",2,98,99],["add",55]],
    [["if",2,100],["add",56]],
    [["if",101,102,103],["add",56]],
    [["if",2,96,104],["add",57,61]],
    [["if",2,105],["add",58]],
    [["if",2,106],["add",59]],
    [["if",2],["add",60,63,67,68,70,71,72,73,74,75,76,77,78,79,80]],
    [["if",2,107],["add",62]],
    [["if",108,109,110],["add",64]],
    [["if",48,111,112,113],["add",65]],
    [["if",114],["add",66]],
    [["if",2,77],["add",69,103],["block",24]],
    [["if",2,115],["add",81]],
    [["if",2,116,117],["add",82,84,90]],
    [["if",2,118,119],["add",82]],
    [["if",44,120],["add",83]],
    [["if",2],["unless",121],["add",85,104],["block",102]],
    [["if",4,122],["add",86,91,94,95]],
    [["if",4],["add",87,88]],
    [["if",2,54],["add",89]],
    [["if",0,4,125],["add",93]],
    [["if",2,126],["add",96],["block",96]],
    [["if",4,127],["add",97],["block",97]],
    [["if",4,128],["add",98]],
    [["if",4,129],["add",99]],
    [["if",130,131],["add",102,104]],
    [["if",2,132],["add",104]],
    [["if",32,33],["block",8,9,20,21,22,25,26,28,36,37,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104]],
    [["if",2,38],["block",11,84]],
    [["if",2,39],["block",11]],
    [["if",33,51],["block",15,45]],
    [["if",33,52],["block",15,45]],
    [["if",33,53],["block",15,45]],
    [["if",33,54],["block",15,45]],
    [["if",44,55],["block",15,45]],
    [["if",33,105],["block",63,87,103]],
    [["if",2],["unless",38],["block",82,86]],
    [["if",33,77],["block",83]],
    [["if",4,25],["block",87]],
    [["if",33,123],["block",87]],
    [["if",33],["unless",124],["block",88]]]
},
"runtime":[
[],[]
]



};
var aa,ca=this||self,da=/^[\w+/_-]+[=]{0,2}$/,ea=null;var fa=function(){},ha=function(a){return"function"==typeof a},ia=function(a){return"string"==typeof a},ja=function(a){return"number"==typeof a&&!isNaN(a)},ka=function(a){return"[object Array]"==Object.prototype.toString.call(Object(a))},la=function(a,b){if(Array.prototype.indexOf){var c=a.indexOf(b);return"number"==typeof c?c:-1}for(var d=0;d<a.length;d++)if(a[d]===b)return d;return-1},ma=function(a,b){if(a&&ka(a))for(var c=0;c<a.length;c++)if(a[c]&&b(a[c]))return a[c]},oa=function(a,b){if(!ja(a)||
!ja(b)||a>b)a=0,b=2147483647;return Math.floor(Math.random()*(b-a+1)+a)},qa=function(a,b){for(var c=new pa,d=0;d<a.length;d++)c.set(a[d],!0);for(var e=0;e<b.length;e++)if(c.get(b[e]))return!0;return!1},ra=function(a,b){for(var c in a)Object.prototype.hasOwnProperty.call(a,c)&&b(c,a[c])},sa=function(a){return Math.round(Number(a))||0},ta=function(a){return"false"==String(a).toLowerCase()?!1:!!a},ua=function(a){var b=[];if(ka(a))for(var c=0;c<a.length;c++)b.push(String(a[c]));return b},va=function(a){return a?
a.replace(/^\s+|\s+$/g,""):""},wa=function(){return(new Date).getTime()},pa=function(){this.prefix="gtm.";this.values={}};pa.prototype.set=function(a,b){this.values[this.prefix+a]=b};pa.prototype.get=function(a){return this.values[this.prefix+a]};pa.prototype.contains=function(a){return void 0!==this.get(a)};
var xa=function(a,b,c){return a&&a.hasOwnProperty(b)?a[b]:c},ya=function(a){var b=!1;return function(){if(!b)try{a()}catch(c){}b=!0}},za=function(a,b){for(var c in b)b.hasOwnProperty(c)&&(a[c]=b[c])},Aa=function(a){for(var b in a)if(a.hasOwnProperty(b))return!0;return!1},Da=function(a,b){for(var c=[],d=0;d<a.length;d++)c.push(a[d]),c.push.apply(c,b[a[d]]||[]);return c};/*
 jQuery v1.9.1 (c) 2005, 2012 jQuery Foundation, Inc. jquery.org/license. */
var Ea=/\[object (Boolean|Number|String|Function|Array|Date|RegExp)\]/,Fa=function(a){if(null==a)return String(a);var b=Ea.exec(Object.prototype.toString.call(Object(a)));return b?b[1].toLowerCase():"object"},Ga=function(a,b){return Object.prototype.hasOwnProperty.call(Object(a),b)},Ha=function(a){if(!a||"object"!=Fa(a)||a.nodeType||a==a.window)return!1;try{if(a.constructor&&!Ga(a,"constructor")&&!Ga(a.constructor.prototype,"isPrototypeOf"))return!1}catch(c){return!1}for(var b in a);return void 0===
b||Ga(a,b)},f=function(a,b){var c=b||("array"==Fa(a)?[]:{}),d;for(d in a)if(Ga(a,d)){var e=a[d];"array"==Fa(e)?("array"!=Fa(c[d])&&(c[d]=[]),c[d]=f(e,c[d])):Ha(e)?(Ha(c[d])||(c[d]={}),c[d]=f(e,c[d])):c[d]=e}return c};var u=window,C=document,Ia=navigator,Ja=C.currentScript&&C.currentScript.src,Ka=function(a,b){var c=u[a];u[a]=void 0===c?b:c;return u[a]},La=function(a,b){b&&(a.addEventListener?a.onload=b:a.onreadystatechange=function(){a.readyState in{loaded:1,complete:1}&&(a.onreadystatechange=null,b())})},Ma=function(a,b,c){var d=C.createElement("script");d.type="text/javascript";d.async=!0;d.src=a;La(d,b);c&&(d.onerror=c);var e;if(null===ea)b:{var g=ca.document,h=g.querySelector&&g.querySelector("script[nonce]");
if(h){var k=h.nonce||h.getAttribute("nonce");if(k&&da.test(k)){ea=k;break b}}ea=""}e=ea;e&&d.setAttribute("nonce",e);var l=C.getElementsByTagName("script")[0]||C.body||C.head;l.parentNode.insertBefore(d,l);return d},Na=function(){if(Ja){var a=Ja.toLowerCase();if(0===a.indexOf("https://"))return 2;if(0===a.indexOf("http://"))return 3}return 1},Oa=function(a,b){var c=C.createElement("iframe");c.height="0";c.width="0";c.style.display="none";c.style.visibility="hidden";var d=C.body&&C.body.lastChild||
C.body||C.head;d.parentNode.insertBefore(c,d);La(c,b);void 0!==a&&(c.src=a);return c},Pa=function(a,b,c){var d=new Image(1,1);d.onload=function(){d.onload=null;b&&b()};d.onerror=function(){d.onerror=null;c&&c()};d.src=a;return d},Qa=function(a,b,c,d){a.addEventListener?a.addEventListener(b,c,!!d):a.attachEvent&&a.attachEvent("on"+b,c)},Ra=function(a,b,c){a.removeEventListener?a.removeEventListener(b,c,!1):a.detachEvent&&a.detachEvent("on"+b,c)},D=function(a){u.setTimeout(a,0)},Sa=function(a,b){return a&&
b&&a.attributes&&a.attributes[b]?a.attributes[b].value:null},Wa=function(a){var b=a.innerText||a.textContent||"";b&&" "!=b&&(b=b.replace(/^[\s\xa0]+|[\s\xa0]+$/g,""));b&&(b=b.replace(/(\xa0+|\s{2,}|\n|\r\t)/g," "));return b},Xa=function(a){var b=C.createElement("div");b.innerHTML="A<div>"+a+"</div>";b=b.lastChild;for(var c=[];b.firstChild;)c.push(b.removeChild(b.firstChild));return c},Ya=function(a,b,c){c=c||100;for(var d={},e=0;e<b.length;e++)d[b[e]]=!0;for(var g=a,h=0;g&&h<=c;h++){if(d[String(g.tagName).toLowerCase()])return g;
g=g.parentElement}return null},Za=function(a,b){var c=a[b];c&&"string"===typeof c.animVal&&(c=c.animVal);return c};var $a=/^(?:(?:https?|mailto|ftp):|[^:/?#]*(?:[/?#]|$))/i;var ab={},bb=function(a,b){ab[a]=ab[a]||[];ab[a][b]=!0},cb=function(a){for(var b=[],c=ab[a]||[],d=0;d<c.length;d++)c[d]&&(b[Math.floor(d/6)]^=1<<d%6);for(var e=0;e<b.length;e++)b[e]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_".charAt(b[e]||0);return b.join("")};var db=/:[0-9]+$/,eb=function(a,b,c){for(var d=a.split("&"),e=0;e<d.length;e++){var g=d[e].split("=");if(decodeURIComponent(g[0]).replace(/\+/g," ")===b){var h=g.slice(1).join("=");return c?h:decodeURIComponent(h).replace(/\+/g," ")}}},ib=function(a,b,c,d,e){b&&(b=String(b).toLowerCase());if("protocol"===b||"port"===b)a.protocol=fb(a.protocol)||fb(u.location.protocol);"port"===b?a.port=String(Number(a.hostname?a.port:u.location.port)||("http"==a.protocol?80:"https"==a.protocol?443:"")):"host"===b&&
(a.hostname=(a.hostname||u.location.hostname).replace(db,"").toLowerCase());var g=b,h,k=fb(a.protocol);g&&(g=String(g).toLowerCase());switch(g){case "url_no_fragment":h=hb(a);break;case "protocol":h=k;break;case "host":h=a.hostname.replace(db,"").toLowerCase();if(c){var l=/^www\d*\./.exec(h);l&&l[0]&&(h=h.substr(l[0].length))}break;case "port":h=String(Number(a.port)||("http"==k?80:"https"==k?443:""));break;case "path":a.pathname||a.hostname||bb("TAGGING",1);h="/"==a.pathname.substr(0,1)?a.pathname:
"/"+a.pathname;var m=h.split("/");0<=la(d||[],m[m.length-1])&&(m[m.length-1]="");h=m.join("/");break;case "query":h=a.search.replace("?","");e&&(h=eb(h,e,void 0));break;case "extension":var n=a.pathname.split(".");h=1<n.length?n[n.length-1]:"";h=h.split("/")[0];break;case "fragment":h=a.hash.replace("#","");break;default:h=a&&a.href}return h},fb=function(a){return a?a.replace(":","").toLowerCase():""},hb=function(a){var b="";if(a&&a.href){var c=a.href.indexOf("#");b=0>c?a.href:a.href.substr(0,c)}return b},
jb=function(a){var b=C.createElement("a");a&&(b.href=a);var c=b.pathname;"/"!==c[0]&&(a||bb("TAGGING",1),c="/"+c);var d=b.hostname.replace(db,"");return{href:b.href,protocol:b.protocol,host:b.host,hostname:d,pathname:c,search:b.search,hash:b.hash,port:b.port}};var kb=function(a,b,c){for(var d=[],e=String(b||document.cookie).split(";"),g=0;g<e.length;g++){var h=e[g].split("="),k=h[0].replace(/^\s*|\s*$/g,"");if(k&&k==a){var l=h.slice(1).join("=").replace(/^\s*|\s*$/g,"");l&&c&&(l=decodeURIComponent(l));d.push(l)}}return d},nb=function(a,b,c,d){var e=lb(a,d);if(1===e.length)return e[0].id;if(0!==e.length){e=mb(e,function(g){return g.Ib},b);if(1===e.length)return e[0].id;e=mb(e,function(g){return g.eb},c);return e[0]?e[0].id:void 0}};
function ob(a,b,c){var d=document.cookie;document.cookie=a;var e=document.cookie;return d!=e||void 0!=c&&0<=kb(b,e).indexOf(c)}
var sb=function(a,b,c,d,e,g){d=d||"auto";var h={path:c||"/"};e&&(h.expires=e);"none"!==d&&(h.domain=d);var k;a:{var l=b,m;if(void 0==l)m=a+"=deleted; expires="+(new Date(0)).toUTCString();else{g&&(l=encodeURIComponent(l));var n=l;n&&1200<n.length&&(n=n.substring(0,1200));l=n;m=a+"="+l}var p=void 0,t=void 0,q;for(q in h)if(h.hasOwnProperty(q)){var r=h[q];if(null!=r)switch(q){case "secure":r&&(m+="; secure");break;case "domain":p=r;break;default:"path"==q&&(t=r),"expires"==q&&r instanceof Date&&(r=
r.toUTCString()),m+="; "+q+"="+r}}if("auto"===p){for(var v=pb(),w=0;w<v.length;++w){var x="none"!=v[w]?v[w]:void 0;if(!rb(x,t)&&ob(m+(x?"; domain="+x:""),a,l)){k=!0;break a}}k=!1}else p&&"none"!=p&&(m+="; domain="+p),k=!rb(p,t)&&ob(m,a,l)}return k};function mb(a,b,c){for(var d=[],e=[],g,h=0;h<a.length;h++){var k=a[h],l=b(k);l===c?d.push(k):void 0===g||l<g?(e=[k],g=l):l===g&&e.push(k)}return 0<d.length?d:e}
function lb(a,b){for(var c=[],d=kb(a),e=0;e<d.length;e++){var g=d[e].split("."),h=g.shift();if(!b||-1!==b.indexOf(h)){var k=g.shift();k&&(k=k.split("-"),c.push({id:g.join("."),Ib:1*k[0]||1,eb:1*k[1]||1}))}}return c}
var tb=/^(www\.)?google(\.com?)?(\.[a-z]{2})?$/,ub=/(^|\.)doubleclick\.net$/i,rb=function(a,b){return ub.test(document.location.hostname)||"/"===b&&tb.test(a)},pb=function(){var a=[],b=document.location.hostname.split(".");if(4===b.length){var c=b[b.length-1];if(parseInt(c,10).toString()===c)return["none"]}for(var d=b.length-2;0<=d;d--)a.push(b.slice(d).join("."));a.push("none");return a};
var vb=[],wb={"\x00":"&#0;",'"':"&quot;","&":"&amp;","'":"&#39;","<":"&lt;",">":"&gt;","\t":"&#9;","\n":"&#10;","\x0B":"&#11;","\f":"&#12;","\r":"&#13;"," ":"&#32;","-":"&#45;","/":"&#47;","=":"&#61;","`":"&#96;","\u0085":"&#133;","\u00a0":"&#160;","\u2028":"&#8232;","\u2029":"&#8233;"},xb=function(a){return wb[a]},yb=/[\x00\x22\x26\x27\x3c\x3e]/g;var Cb=/[\x00\x08-\x0d\x22\x26\x27\/\x3c-\x3e\\\x85\u2028\u2029]/g,Db={"\x00":"\\x00","\b":"\\x08","\t":"\\t","\n":"\\n","\x0B":"\\x0b",
"\f":"\\f","\r":"\\r",'"':"\\x22","&":"\\x26","'":"\\x27","/":"\\/","<":"\\x3c","=":"\\x3d",">":"\\x3e","\\":"\\\\","\u0085":"\\x85","\u2028":"\\u2028","\u2029":"\\u2029",$:"\\x24","(":"\\x28",")":"\\x29","*":"\\x2a","+":"\\x2b",",":"\\x2c","-":"\\x2d",".":"\\x2e",":":"\\x3a","?":"\\x3f","[":"\\x5b","]":"\\x5d","^":"\\x5e","{":"\\x7b","|":"\\x7c","}":"\\x7d"},Eb=function(a){return Db[a]};vb[7]=function(a){return String(a).replace(Cb,Eb)};
vb[8]=function(a){if(null==a)return" null ";switch(typeof a){case "boolean":case "number":return" "+a+" ";default:return"'"+String(String(a)).replace(Cb,Eb)+"'"}};var Mb=/[\x00- \x22\x27-\x29\x3c\x3e\\\x7b\x7d\x7f\x85\xa0\u2028\u2029\uff01\uff03\uff04\uff06-\uff0c\uff0f\uff1a\uff1b\uff1d\uff1f\uff20\uff3b\uff3d]/g,Nb={"\x00":"%00","\u0001":"%01","\u0002":"%02","\u0003":"%03","\u0004":"%04","\u0005":"%05","\u0006":"%06","\u0007":"%07","\b":"%08","\t":"%09","\n":"%0A","\x0B":"%0B","\f":"%0C","\r":"%0D","\u000e":"%0E","\u000f":"%0F","\u0010":"%10",
"\u0011":"%11","\u0012":"%12","\u0013":"%13","\u0014":"%14","\u0015":"%15","\u0016":"%16","\u0017":"%17","\u0018":"%18","\u0019":"%19","\u001a":"%1A","\u001b":"%1B","\u001c":"%1C","\u001d":"%1D","\u001e":"%1E","\u001f":"%1F"," ":"%20",'"':"%22","'":"%27","(":"%28",")":"%29","<":"%3C",">":"%3E","\\":"%5C","{":"%7B","}":"%7D","\u007f":"%7F","\u0085":"%C2%85","\u00a0":"%C2%A0","\u2028":"%E2%80%A8","\u2029":"%E2%80%A9","\uff01":"%EF%BC%81","\uff03":"%EF%BC%83","\uff04":"%EF%BC%84","\uff06":"%EF%BC%86",
"\uff07":"%EF%BC%87","\uff08":"%EF%BC%88","\uff09":"%EF%BC%89","\uff0a":"%EF%BC%8A","\uff0b":"%EF%BC%8B","\uff0c":"%EF%BC%8C","\uff0f":"%EF%BC%8F","\uff1a":"%EF%BC%9A","\uff1b":"%EF%BC%9B","\uff1d":"%EF%BC%9D","\uff1f":"%EF%BC%9F","\uff20":"%EF%BC%A0","\uff3b":"%EF%BC%BB","\uff3d":"%EF%BC%BD"},Ob=function(a){return Nb[a]};vb[16]=function(a){return a};var Qb=[],Rb=[],Sb=[],Tb=[],Ub=[],Wb={},Xb,Yb,Zb,$b=function(a,b){var c={};c["function"]="__"+a;for(var d in b)b.hasOwnProperty(d)&&(c["vtp_"+d]=b[d]);return c},ac=function(a,b){var c=a["function"];if(!c)throw Error("Error: No function name given for function call.");var d=!!Wb[c],e={},g;for(g in a)a.hasOwnProperty(g)&&0===g.indexOf("vtp_")&&(e[d?g:g.substr(4)]=a[g]);return d?Wb[c](e):(void 0)(c,e,b)},cc=function(a,b,c){c=c||[];var d={},e;for(e in a)a.hasOwnProperty(e)&&(d[e]=bc(a[e],b,c));return d},
dc=function(a){var b=a["function"];if(!b)throw"Error: No function name given for function call.";var c=Wb[b];return c?c.priorityOverride||0:0},bc=function(a,b,c){if(ka(a)){var d;switch(a[0]){case "function_id":return a[1];case "list":d=[];for(var e=1;e<a.length;e++)d.push(bc(a[e],b,c));return d;case "macro":var g=a[1];if(c[g])return;var h=Qb[g];if(!h||b.Cc(h))return;c[g]=!0;try{var k=cc(h,b,c);k.vtp_gtmEventId=b.id;d=ac(k,b);Zb&&(d=Zb.Gf(d,k))}catch(w){b.ae&&b.ae(w,Number(g)),d=!1}c[g]=!1;return d;
case "map":d={};for(var l=1;l<a.length;l+=2)d[bc(a[l],b,c)]=bc(a[l+1],b,c);return d;case "template":d=[];for(var m=!1,n=1;n<a.length;n++){var p=bc(a[n],b,c);Yb&&(m=m||p===Yb.wb);d.push(p)}return Yb&&m?Yb.Jf(d):d.join("");case "escape":d=bc(a[1],b,c);if(Yb&&ka(a[1])&&"macro"===a[1][0]&&Yb.kg(a))return Yb.wg(d);d=String(d);for(var t=2;t<a.length;t++)vb[a[t]]&&(d=vb[a[t]](d));return d;case "tag":var q=a[1];if(!Tb[q])throw Error("Unable to resolve tag reference "+q+".");return d={Nd:a[2],index:q};case "zb":var r=
{arg0:a[2],arg1:a[3],ignore_case:a[5]};r["function"]=a[1];var v=ec(r,b,c);a[4]&&(v=!v);return v;default:throw Error("Attempting to expand unknown Value type: "+a[0]+".");}}return a},ec=function(a,b,c){try{return Xb(cc(a,b,c))}catch(d){JSON.stringify(a)}return null};var fc=function(){var a=function(b){return{toString:function(){return b}}};return{ed:a("convert_case_to"),fd:a("convert_false_to"),gd:a("convert_null_to"),hd:a("convert_true_to"),jd:a("convert_undefined_to"),eh:a("debug_mode_metadata"),ka:a("function"),Ue:a("instance_name"),Ve:a("live_only"),We:a("malware_disabled"),Xe:a("metadata"),gh:a("original_vendor_template_id"),Ye:a("once_per_event"),Cd:a("once_per_load"),Dd:a("setup_tags"),Ed:a("tag_id"),Fd:a("teardown_tags")}}();var gc=null,jc=function(a){function b(p){for(var t=0;t<p.length;t++)d[p[t]]=!0}var c=[],d=[];gc=hc(a);for(var e=0;e<Rb.length;e++){var g=Rb[e],h=ic(g);if(h){for(var k=g.add||[],l=0;l<k.length;l++)c[k[l]]=!0;b(g.block||[])}else null===h&&b(g.block||[])}for(var m=[],n=0;n<Tb.length;n++)c[n]&&!d[n]&&(m[n]=!0);return m},ic=function(a){for(var b=a["if"]||[],c=0;c<b.length;c++){var d=gc(b[c]);if(!d)return null===d?null:!1}for(var e=a.unless||[],g=0;g<e.length;g++){var h=gc(e[g]);if(null===h)return null;
if(h)return!1}return!0},hc=function(a){var b=[];return function(c){void 0===b[c]&&(b[c]=ec(Sb[c],a));return b[c]}};/*
 Copyright (c) 2014 Derek Brans, MIT license https://github.com/krux/postscribe/blob/master/LICENSE. Portions derived from simplehtmlparser, which is licensed under the Apache License, Version 2.0 */

var kc,lc=function(){};(function(){function a(k,l){k=k||"";l=l||{};for(var m in b)b.hasOwnProperty(m)&&(l.sf&&(l["fix_"+m]=!0),l.Od=l.Od||l["fix_"+m]);var n={comment:/^\x3c!--/,endTag:/^<\//,atomicTag:/^<\s*(script|style|noscript|iframe|textarea)[\s\/>]/i,startTag:/^</,chars:/^[^<]/},p={comment:function(){var q=k.indexOf("--\x3e");if(0<=q)return{content:k.substr(4,q),length:q+3}},endTag:function(){var q=k.match(d);if(q)return{tagName:q[1],length:q[0].length}},atomicTag:function(){var q=p.startTag();
if(q){var r=k.slice(q.length);if(r.match(new RegExp("</\\s*"+q.tagName+"\\s*>","i"))){var v=r.match(new RegExp("([\\s\\S]*?)</\\s*"+q.tagName+"\\s*>","i"));if(v)return{tagName:q.tagName,F:q.F,content:v[1],length:v[0].length+q.length}}}},startTag:function(){var q=k.match(c);if(q){var r={};q[2].replace(e,function(v,w,x,y,z){var B=x||y||z||g.test(w)&&w||null,A=document.createElement("div");A.innerHTML=B;r[w]=A.textContent||A.innerText||B});return{tagName:q[1],F:r,jb:!!q[3],length:q[0].length}}},chars:function(){var q=
k.indexOf("<");return{length:0<=q?q:k.length}}},t=function(){for(var q in n)if(n[q].test(k)){var r=p[q]();return r?(r.type=r.type||q,r.text=k.substr(0,r.length),k=k.slice(r.length),r):null}};l.Od&&function(){var q=/^(AREA|BASE|BASEFONT|BR|COL|FRAME|HR|IMG|INPUT|ISINDEX|LINK|META|PARAM|EMBED)$/i,r=/^(COLGROUP|DD|DT|LI|OPTIONS|P|TD|TFOOT|TH|THEAD|TR)$/i,v=[];v.$d=function(){return this[this.length-1]};v.Gc=function(A){var E=this.$d();return E&&E.tagName&&E.tagName.toUpperCase()===A.toUpperCase()};v.Ff=
function(A){for(var E=0,F;F=this[E];E++)if(F.tagName===A)return!0;return!1};var w=function(A){A&&"startTag"===A.type&&(A.jb=q.test(A.tagName)||A.jb);return A},x=t,y=function(){k="</"+v.pop().tagName+">"+k},z={startTag:function(A){var E=A.tagName;"TR"===E.toUpperCase()&&v.Gc("TABLE")?(k="<TBODY>"+k,B()):l.oh&&r.test(E)&&v.Ff(E)?v.Gc(E)?y():(k="</"+A.tagName+">"+k,B()):A.jb||v.push(A)},endTag:function(A){v.$d()?l.Tf&&!v.Gc(A.tagName)?y():v.pop():l.Tf&&(x(),B())}},B=function(){var A=k,E=w(x());k=A;if(E&&
z[E.type])z[E.type](E)};t=function(){B();return w(x())}}();return{append:function(q){k+=q},Fg:t,uh:function(q){for(var r;(r=t())&&(!q[r.type]||!1!==q[r.type](r)););},clear:function(){var q=k;k="";return q},vh:function(){return k},stack:[]}}var b=function(){var k={},l=this.document.createElement("div");l.innerHTML="<P><I></P></I>";k.Bh="<P><I></P></I>"!==l.innerHTML;l.innerHTML="<P><i><P></P></i></P>";k.xh=2===l.childNodes.length;return k}(),c=/^<([\-A-Za-z0-9_]+)((?:\s+[\w\-]+(?:\s*=?\s*(?:(?:"[^"]*")|(?:'[^']*')|[^>\s]+))?)*)\s*(\/?)>/,
d=/^<\/([\-A-Za-z0-9_]+)[^>]*>/,e=/([\-A-Za-z0-9_]+)(?:\s*=\s*(?:(?:"((?:\\.|[^"])*)")|(?:'((?:\\.|[^'])*)')|([^>\s]+)))?/g,g=/^(checked|compact|declare|defer|disabled|ismap|multiple|nohref|noresize|noshade|nowrap|readonly|selected)$/i;a.supports=b;a.Ch=function(k){var l={comment:function(m){return"<--"+m.content+"--\x3e"},endTag:function(m){return"</"+m.tagName+">"},atomicTag:function(m){return l.startTag(m)+m.content+l.endTag(m)},startTag:function(m){var n="<"+m.tagName,p;for(p in m.F){var t=m.F[p];
n+=" "+p+'="'+(t?t.replace(/(^|[^\\])"/g,'$1\\"'):"")+'"'}return n+(m.jb?"/>":">")},chars:function(m){return m.text}};return l[k.type](k)};a.nh=function(k){var l={},m;for(m in k){var n=k[m];l[m]=n&&n.replace(/(^|[^\\])"/g,'$1\\"')}return l};for(var h in b)a.xf=a.xf||!b[h]&&h;kc=a})();(function(){function a(){}function b(p){return void 0!==p&&null!==p}function c(p,t,q){var r,v=p&&p.length||0;for(r=0;r<v;r++)t.call(q,p[r],r)}function d(p,t,q){for(var r in p)p.hasOwnProperty(r)&&t.call(q,r,p[r])}function e(p,
t){d(t,function(q,r){p[q]=r});return p}function g(p,t){p=p||{};d(t,function(q,r){b(p[q])||(p[q]=r)});return p}function h(p){try{return m.call(p)}catch(q){var t=[];c(p,function(r){t.push(r)});return t}}var k={ef:a,ff:a,hf:a,jf:a,tf:a,uf:function(p){return p},done:a,error:function(p){throw p;},Jg:!1},l=this;if(!l.postscribe){var m=Array.prototype.slice,n=function(){function p(q,r,v){var w="data-ps-"+r;if(2===arguments.length){var x=q.getAttribute(w);return b(x)?String(x):x}b(v)&&""!==v?q.setAttribute(w,
v):q.removeAttribute(w)}function t(q,r){var v=q.ownerDocument;e(this,{root:q,options:r,kb:v.defaultView||v.parentWindow,Ca:v,Pb:kc("",{sf:!0}),nc:[q],Qc:"",Rc:v.createElement(q.nodeName),hb:[],qa:[]});p(this.Rc,"proxyof",0)}t.prototype.write=function(){[].push.apply(this.qa,arguments);for(var q;!this.Hb&&this.qa.length;)q=this.qa.shift(),"function"===typeof q?this.Bf(q):this.ad(q)};t.prototype.Bf=function(q){var r={type:"function",value:q.name||q.toString()};this.Mc(r);q.call(this.kb,this.Ca);this.de(r)};
t.prototype.ad=function(q){this.Pb.append(q);for(var r,v=[],w,x;(r=this.Pb.Fg())&&!(w=r&&"tagName"in r?!!~r.tagName.toLowerCase().indexOf("script"):!1)&&!(x=r&&"tagName"in r?!!~r.tagName.toLowerCase().indexOf("style"):!1);)v.push(r);this.bh(v);w&&this.Zf(r);x&&this.$f(r)};t.prototype.bh=function(q){var r=this.yf(q);r.Hd&&(r.Ac=this.Qc+r.Hd,this.Qc+=r.Bg,this.Rc.innerHTML=r.Ac,this.$g())};t.prototype.yf=function(q){var r=this.nc.length,v=[],w=[],x=[];c(q,function(y){v.push(y.text);if(y.F){if(!/^noscript$/i.test(y.tagName)){var z=
r++;w.push(y.text.replace(/(\/?>)/," data-ps-id="+z+" $1"));"ps-script"!==y.F.id&&"ps-style"!==y.F.id&&x.push("atomicTag"===y.type?"":"<"+y.tagName+" data-ps-proxyof="+z+(y.jb?" />":">"))}}else w.push(y.text),x.push("endTag"===y.type?y.text:"")});return{Dh:q,raw:v.join(""),Hd:w.join(""),Bg:x.join("")}};t.prototype.$g=function(){for(var q,r=[this.Rc];b(q=r.shift());){var v=1===q.nodeType;if(!v||!p(q,"proxyof")){v&&(this.nc[p(q,"id")]=q,p(q,"id",null));var w=q.parentNode&&p(q.parentNode,"proxyof");
w&&this.nc[w].appendChild(q)}r.unshift.apply(r,h(q.childNodes))}};t.prototype.Zf=function(q){var r=this.Pb.clear();r&&this.qa.unshift(r);q.src=q.F.src||q.F.hh;q.src&&this.hb.length?this.Hb=q:this.Mc(q);var v=this;this.ah(q,function(){v.de(q)})};t.prototype.$f=function(q){var r=this.Pb.clear();r&&this.qa.unshift(r);q.type=q.F.type||q.F.ih||"text/css";this.dh(q);r&&this.write()};t.prototype.dh=function(q){var r=this.Af(q);this.ig(r);q.content&&(r.styleSheet&&!r.sheet?r.styleSheet.cssText=q.content:
r.appendChild(this.Ca.createTextNode(q.content)))};t.prototype.Af=function(q){var r=this.Ca.createElement(q.tagName);r.setAttribute("type",q.type);d(q.F,function(v,w){r.setAttribute(v,w)});return r};t.prototype.ig=function(q){this.ad('<span id="ps-style"/>');var r=this.Ca.getElementById("ps-style");r.parentNode.replaceChild(q,r)};t.prototype.Mc=function(q){q.rg=this.qa;this.qa=[];this.hb.unshift(q)};t.prototype.de=function(q){q!==this.hb[0]?this.options.error({message:"Bad script nesting or script finished twice"}):
(this.hb.shift(),this.write.apply(this,q.rg),!this.hb.length&&this.Hb&&(this.Mc(this.Hb),this.Hb=null))};t.prototype.ah=function(q,r){var v=this.zf(q),w=this.Pg(v),x=this.options.ef;q.src&&(v.src=q.src,this.Ng(v,w?x:function(){r();x()}));try{this.hg(v),q.src&&!w||r()}catch(y){this.options.error(y),r()}};t.prototype.zf=function(q){var r=this.Ca.createElement(q.tagName);d(q.F,function(v,w){r.setAttribute(v,w)});q.content&&(r.text=q.content);return r};t.prototype.hg=function(q){this.ad('<span id="ps-script"/>');
var r=this.Ca.getElementById("ps-script");r.parentNode.replaceChild(q,r)};t.prototype.Ng=function(q,r){function v(){q=q.onload=q.onreadystatechange=q.onerror=null}var w=this.options.error;e(q,{onload:function(){v();r()},onreadystatechange:function(){/^(loaded|complete)$/.test(q.readyState)&&(v(),r())},onerror:function(){var x={message:"remote script failed "+q.src};v();w(x);r()}})};t.prototype.Pg=function(q){return!/^script$/i.test(q.nodeName)||!!(this.options.Jg&&q.src&&q.hasAttribute("async"))};
return t}();l.postscribe=function(){function p(){var w=r.shift(),x;w&&(x=w[w.length-1],x.ff(),w.stream=t.apply(null,w),x.hf())}function t(w,x,y){function z(F){F=y.uf(F);v.write(F);y.jf(F)}v=new n(w,y);v.id=q++;v.name=y.name||v.id;var B=w.ownerDocument,A={close:B.close,open:B.open,write:B.write,writeln:B.writeln};e(B,{close:a,open:a,write:function(){return z(h(arguments).join(""))},writeln:function(){return z(h(arguments).join("")+"\n")}});var E=v.kb.onerror||a;v.kb.onerror=function(F,I,S){y.error({rh:F+
" - "+I+":"+S});E.apply(v.kb,arguments)};v.write(x,function(){e(B,A);v.kb.onerror=E;y.done();v=null;p()});return v}var q=0,r=[],v=null;return e(function(w,x,y){"function"===typeof y&&(y={done:y});y=g(y,k);w=/^#/.test(w)?l.document.getElementById(w.substr(1)):w.ph?w[0]:w;var z=[w,x,y];w.vg={cancel:function(){z.stream?z.stream.abort():z[1]=a}};y.tf(z);r.push(z);v||p();return w.vg},{streams:{},th:r,jh:n})}();lc=l.postscribe}})();for(var mc="floor ceil round max min abs pow sqrt".split(" "),nc=0;nc<mc.length;nc++)Math.hasOwnProperty(mc[nc]);var G={$b:"event_callback",Na:"event_timeout",U:"gtag.config",N:"allow_ad_personalization_signals",P:"cookie_expires",Ma:"cookie_update",xa:"session_duration"};var Cc=/[A-Z]+/,Dc=/\s/,Ec=function(a){if(ia(a)&&(a=va(a),!Dc.test(a))){var b=a.indexOf("-");if(!(0>b)){var c=a.substring(0,b);if(Cc.test(c)){for(var d=a.substring(b+1).split("/"),e=0;e<d.length;e++)if(!d[e])return;return{id:a,prefix:c,containerId:c+"-"+d[0],M:d}}}}},Gc=function(a){for(var b={},c=0;c<a.length;++c){var d=Ec(a[c]);d&&(b[d.id]=d)}Fc(b);var e=[];ra(b,function(g,h){e.push(h)});return e};
function Fc(a){var b=[],c;for(c in a)if(a.hasOwnProperty(c)){var d=a[c];"AW"===d.prefix&&d.M[1]&&b.push(d.containerId)}for(var e=0;e<b.length;++e)delete a[b[e]]};var Hc={},Ic=null,Jc=Math.random();Hc.i="GTM-N52333";Hc.Ab="8e1";var Kc={__cl:!0,__ecl:!0,__ehl:!0,__evl:!0,__fal:!0,__fil:!0,__fsl:!0,__hl:!0,__jel:!0,__lcl:!0,__sdl:!0,__tl:!0,__ytl:!0,__paused:!0},Lc="www.googletagmanager.com/gtm.js";var Mc=Lc,Nc=null,Oc=null,Pc=null,Qc="//www.googletagmanager.com/a?id="+Hc.i+"&cv=346",Rc={},Sc={},Tc=function(){var a=Ic.sequence||0;Ic.sequence=a+1;return a};
var Uc=function(){return"&tc="+Tb.filter(function(a){return a}).length},cd=function(){Vc&&(u.clearTimeout(Vc),Vc=void 0);void 0===Wc||Xc[Wc]&&!Yc||(Zc[Wc]||$c.mg()||0>=ad--?(bb("GTM",1),Zc[Wc]=!0):($c.Hg(),Pa(bd()),Xc[Wc]=!0,Yc=""))},bd=function(){var a=Wc;if(void 0===a)return"";var b=cb("GTM"),c=cb("TAGGING");return[dd,Xc[a]?"":"&es=1",ed[a],b?"&u="+b:"",c?"&ut="+c:"",Uc(),Yc,"&z=0"].join("")},fd=function(){return[Qc,"&v=3&t=t","&pid="+oa(),"&rv="+Hc.Ab].join("")},gd="0.005000">
Math.random(),dd=fd(),hd=function(){dd=fd()},Xc={},Yc="",Wc=void 0,ed={},Zc={},Vc=void 0,$c=function(a,b){var c=0,d=0;return{mg:function(){if(c<a)return!1;wa()-d>=b&&(c=0);return c>=a},Hg:function(){wa()-d>=b&&(c=0);c++;d=wa()}}}(2,1E3),ad=1E3,id=function(a,b){if(gd&&!Zc[a]&&Wc!==a){cd();Wc=a;Yc="";var c;c=0===b.indexOf("gtm.")?encodeURIComponent(b):"*";ed[a]="&e="+c+"&eid="+a;Vc||(Vc=u.setTimeout(cd,500))}},jd=function(a,b,c){if(gd&&!Zc[a]&&b){a!==Wc&&(cd(),Wc=a);var d=String(b[fc.ka]||"").replace(/_/g,
"");0===d.indexOf("cvt")&&(d="cvt");var e=c+d;Yc=Yc?Yc+"."+e:"&tr="+e;Vc||(Vc=u.setTimeout(cd,500));2022<=bd().length&&cd()}};var kd={},ld=new pa,md={},nd={},rd={name:"dataLayer",set:function(a,b){f(od(a,b),md);pd()},get:function(a){return qd(a,2)},reset:function(){ld=new pa;md={};pd()}},qd=function(a,b){if(2!=b){var c=ld.get(a);if(gd){var d=sd(a);c!==d&&bb("GTM",5)}return c}return sd(a)},sd=function(a,b,c){var d=a.split("."),e=!1,g=void 0;return e?g:ud(d)},ud=function(a){for(var b=md,c=0;c<a.length;c++){if(null===b)return!1;if(void 0===b)break;b=b[a[c]]}return b};
var xd=function(a,b){nd.hasOwnProperty(a)||(ld.set(a,b),f(od(a,b),md),pd())},od=function(a,b){for(var c={},d=c,e=a.split("."),g=0;g<e.length-1;g++)d=d[e[g]]={};d[e[e.length-1]]=b;return c},pd=function(a){ra(nd,function(b,c){ld.set(b,c);f(od(b,void 0),md);f(od(b,c),md);a&&delete nd[b]})},yd=function(a,b,c){kd[a]=kd[a]||{};var d=1!==c?sd(b):ld.get(b);"array"===Fa(d)||"object"===Fa(d)?kd[a][b]=f(d):kd[a][b]=d},zd=function(a,b){if(kd[a])return kd[a][b]};var Ad=function(){var a=!1;return a};var H=function(a,b,c,d){return(2===Bd()||d||"http:"!=u.location.protocol?a:b)+c},Bd=function(){var a=Na(),b;if(1===a)a:{var c=Mc;c=c.toLowerCase();for(var d="https://"+c,e="http://"+c,g=1,h=C.getElementsByTagName("script"),k=0;k<h.length&&100>k;k++){var l=h[k].src;if(l){l=l.toLowerCase();if(0===l.indexOf(e)){b=3;break a}1===g&&0===l.indexOf(d)&&(g=2)}}b=g}else b=a;return b};var Pd=new RegExp(/^(.*\.)?(google|youtube|blogger|withgoogle)(\.com?)?(\.[a-z]{2})?\.?$/),Qd={cl:["ecl"],customPixels:["nonGooglePixels"],ecl:["cl"],ehl:["hl"],hl:["ehl"],html:["customScripts","customPixels","nonGooglePixels","nonGoogleScripts","nonGoogleIframes"],customScripts:["html","customPixels","nonGooglePixels","nonGoogleScripts","nonGoogleIframes"],nonGooglePixels:[],nonGoogleScripts:["nonGooglePixels"],nonGoogleIframes:["nonGooglePixels"]},Rd={cl:["ecl"],customPixels:["customScripts","html"],
ecl:["cl"],ehl:["hl"],hl:["ehl"],html:["customScripts"],customScripts:["html"],nonGooglePixels:["customPixels","customScripts","html","nonGoogleScripts","nonGoogleIframes"],nonGoogleScripts:["customScripts","html"],nonGoogleIframes:["customScripts","html","nonGoogleScripts"]},Sd="google customPixels customScripts html nonGooglePixels nonGoogleScripts nonGoogleIframes".split(" ");
var Ud=function(a){var b=qd("gtm.whitelist");b&&bb("GTM",9);var c=b&&Da(ua(b),Qd),d=qd("gtm.blacklist");d||(d=qd("tagTypeBlacklist"))&&bb("GTM",3);
d?bb("GTM",8):d=[];Td()&&(d=ua(d),d.push("nonGooglePixels","nonGoogleScripts"));0<=la(ua(d),"google")&&bb("GTM",2);var e=d&&Da(ua(d),Rd),g={};return function(h){var k=h&&h[fc.ka];if(!k||"string"!=typeof k)return!0;k=k.replace(/^_*/,"");if(void 0!==g[k])return g[k];var l=Sc[k]||[],m=a(k);if(b){var n;if(n=m)a:{if(0>la(c,k))if(l&&0<l.length)for(var p=0;p<l.length;p++){if(0>
la(c,l[p])){bb("GTM",11);n=!1;break a}}else{n=!1;break a}n=!0}m=n}var t=!1;if(d){var q=0<=la(e,k);if(q)t=q;else{var r=qa(e,l||[]);r&&bb("GTM",10);t=r}}var v=!m||t;v||!(0<=la(l,"sandboxedScripts"))||c&&-1!==la(c,"sandboxedScripts")||(v=qa(e,Sd));return g[k]=v}},Td=function(){return Pd.test(u.location&&u.location.hostname)};var Vd={Gf:function(a,b){b[fc.ed]&&"string"===typeof a&&(a=1==b[fc.ed]?a.toLowerCase():a.toUpperCase());b.hasOwnProperty(fc.gd)&&null===a&&(a=b[fc.gd]);b.hasOwnProperty(fc.jd)&&void 0===a&&(a=b[fc.jd]);b.hasOwnProperty(fc.hd)&&!0===a&&(a=b[fc.hd]);b.hasOwnProperty(fc.fd)&&!1===a&&(a=b[fc.fd]);return a}};var Wd={active:!0,isWhitelisted:function(){return!0}},Xd=function(a){var b=Ic.zones;!b&&a&&(b=Ic.zones=a());return b};var Yd=!1,Zd=0,$d=[];function ae(a){if(!Yd){var b=C.createEventObject,c="complete"==C.readyState,d="interactive"==C.readyState;if(!a||"readystatechange"!=a.type||c||!b&&d){Yd=!0;for(var e=0;e<$d.length;e++)D($d[e])}$d.push=function(){for(var g=0;g<arguments.length;g++)D(arguments[g]);return 0}}}function be(){if(!Yd&&140>Zd){Zd++;try{C.documentElement.doScroll("left"),ae()}catch(a){u.setTimeout(be,50)}}}var ce=function(a){Yd?a():$d.push(a)};var de={},ee={},fe=function(a,b,c,d){if(!ee[a]||Kc[b]||"__zone"===b)return-1;var e={};Ha(d)&&(e=f(d,e));e.id=c;e.status="timeout";return ee[a].tags.push(e)-1},ge=function(a,b,c,d){if(ee[a]){var e=ee[a].tags[b];e&&(e.status=c,e.executionTime=d)}};function he(a){for(var b=de[a]||[],c=0;c<b.length;c++)b[c]();de[a]={push:function(d){d(Hc.i,ee[a])}}}
var ke=function(a,b,c){ee[a]={tags:[]};ha(b)&&ie(a,b);c&&u.setTimeout(function(){return he(a)},Number(c));return je(a)},ie=function(a,b){de[a]=de[a]||[];de[a].push(ya(function(){return D(function(){b(Hc.i,ee[a])})}))};function je(a){var b=0,c=0,d=!1;return{add:function(){c++;return ya(function(){b++;d&&b>=c&&he(a)})},qf:function(){d=!0;b>=c&&he(a)}}};var le=function(){function a(d){return!ja(d)||0>d?0:d}if(!Ic._li&&u.performance&&u.performance.timing){var b=u.performance.timing.navigationStart,c=ja(rd.get("gtm.start"))?rd.get("gtm.start"):0;Ic._li={cst:a(c-b),cbt:a(Oc-b)}}};var pe=!1,qe=function(){return u.GoogleAnalyticsObject&&u[u.GoogleAnalyticsObject]},re=!1;
var se=function(a){u.GoogleAnalyticsObject||(u.GoogleAnalyticsObject=a||"ga");var b=u.GoogleAnalyticsObject;if(u[b])u.hasOwnProperty(b)||bb("GTM",12);else{var c=function(){c.q=c.q||[];c.q.push(arguments)};c.l=Number(new Date);u[b]=c}le();return u[b]},te=function(a,b,c,d){b=String(b).replace(/\s+/g,"").split(",");var e=qe();e(a+"require","linker");e(a+"linker:autoLink",b,c,d)};
var ve=function(){},ue=function(){return u.GoogleAnalyticsObject||"ga"};var Ce=function(a){};function Be(a,b){a.containerId=Hc.i;var c={type:"GENERIC",value:a};b.length&&(c.trace=b);return c};function De(a,b,c,d){var e=Tb[a],g=Ee(a,b,c,d);if(!g)return null;var h=bc(e[fc.Dd],c,[]);if(h&&h.length){var k=h[0];g=De(k.index,{I:g,S:1===k.Nd?b.terminate:g,terminate:b.terminate},c,d)}return g}
function Ee(a,b,c,d){function e(){if(g[fc.We])k();else{var w=cc(g,c,[]),x=fe(c.id,String(g[fc.ka]),Number(g[fc.Ed]),w[fc.Xe]),y=!1;w.vtp_gtmOnSuccess=function(){if(!y){y=!0;var A=wa()-B;jd(c.id,Tb[a],"5");ge(c.id,x,"success",A);h()}};w.vtp_gtmOnFailure=function(){if(!y){y=!0;var A=wa()-B;jd(c.id,Tb[a],"6");ge(c.id,x,"failure",A);k()}};w.vtp_gtmTagId=g.tag_id;
w.vtp_gtmEventId=c.id;jd(c.id,g,"1");var z=function(A){var E=wa()-B;Ce(A);jd(c.id,g,"7");ge(c.id,x,"exception",E);y||(y=!0,k())};var B=wa();try{ac(w,c)}catch(A){z(A)}}}var g=Tb[a],h=b.I,k=b.S,l=b.terminate;if(c.Cc(g))return null;var m=bc(g[fc.Fd],c,[]);if(m&&m.length){var n=m[0],p=De(n.index,{I:h,S:k,terminate:l},c,d);if(!p)return null;h=p;k=2===n.Nd?l:p}if(g[fc.Cd]||g[fc.Ye]){var t=g[fc.Cd]?Ub:c.Rg,q=h,r=k;if(!t[a]){e=ya(e);var v=Fe(a,t,e);h=v.I;k=v.S}return function(){t[a](q,r)}}return e}
function Fe(a,b,c){var d=[],e=[];b[a]=Ge(d,e,c);return{I:function(){b[a]=He;for(var g=0;g<d.length;g++)d[g]()},S:function(){b[a]=Ie;for(var g=0;g<e.length;g++)e[g]()}}}function Ge(a,b,c){return function(d,e){a.push(d);b.push(e);c()}}function He(a){a()}function Ie(a,b){b()};var Le=function(a,b){for(var c=[],d=0;d<Tb.length;d++)if(a.cb[d]){var e=Tb[d];var g=b.add();try{var h=De(d,{I:g,S:g,terminate:g},a,d);h?c.push({te:d,ie:dc(e),Rf:h}):(Je(d,a),g())}catch(l){g()}}b.qf();c.sort(Ke);for(var k=0;k<c.length;k++)c[k].Rf();return 0<c.length};function Ke(a,b){var c,d=b.ie,e=a.ie;c=d>e?1:d<e?-1:0;var g;if(0!==c)g=c;else{var h=a.te,k=b.te;g=h>k?1:h<k?-1:0}return g}
function Je(a,b){if(!gd)return;var c=function(d){var e=b.Cc(Tb[d])?"3":"4",g=bc(Tb[d][fc.Dd],b,[]);g&&g.length&&c(g[0].index);jd(b.id,Tb[d],e);var h=bc(Tb[d][fc.Fd],b,[]);h&&h.length&&c(h[0].index)};c(a);}
var Me=!1,Ne=function(a,b,c,d,e){if("gtm.js"==b){if(Me)return!1;Me=!0}id(a,b);var g=ke(a,d,e);yd(a,"event",1);yd(a,"ecommerce",1);yd(a,"gtm");var h={id:a,name:b,Cc:Ud(c),cb:[],Rg:[],ae:function(n){bb("GTM",6);Ce(n)}};h.cb=jc(h);var k=Le(h,g);"gtm.js"!==b&&"gtm.sync"!==b||ve();if(!k)return k;for(var l=0;l<h.cb.length;l++)if(h.cb[l]){var m=
Tb[l];if(m&&!Kc[String(m[fc.ka])])return!0}return!1};var Pe=function(a,b,c){var d=this;this.eventModel=a;this.targetConfig=b||{};this.globalConfig=c||{};this.getWithConfig=function(e){if(d.eventModel.hasOwnProperty(e))return d.eventModel[e];if(d.targetConfig.hasOwnProperty(e))return d.targetConfig[e];if(d.globalConfig.hasOwnProperty(e))return d.globalConfig[e]}};function Qe(){var a=Ic;return a.gcq=a.gcq||new Re}
var Te=function(a,b){Qe().register(a,b)},Ue=function(a,b,c,d){Qe().push("event",[b,a],c,d)},Ve=function(){this.status=1;this.uc={};this.je=null;this.Xd=!1},We=function(a,b,c,d,e){this.type=a;this.Wg=b;this.oa=c||"";this.Db=d;this.defer=e},Re=function(){this.ue={};this.Sd={};this.Xa=[]},Xe=function(a,b){return a.ue[b]=a.ue[b]||new Ve},Ye=function(a,b,c,d){var e=Xe(a,d.oa).je;if(e){var g=f(c),h=f(Xe(a,d.oa).uc),k=f(a.Sd),l=new Pe(g,h,k);try{e(b,d.Wg,l)}catch(m){}}};
Re.prototype.register=function(a,b){3!==Xe(this,a).status&&(Xe(this,a).je=b,Xe(this,a).status=3,this.flush())};Re.prototype.push=function(a,b,c,d){var e=Math.floor(wa()/1E3);if(c&&1===Xe(this,c).status&&(Xe(this,c).status=2,this.push("require",[],c),!Ad())){var g=encodeURIComponent(c);Ma(("http:"!=u.location.protocol?"https:":"http:")+("//www.googletagmanager.com/gtag/js?id="+g+"&l=dataLayer&cx=c"))}this.Xa.push(new We(a,e,c,b,d));d||this.flush()};
Re.prototype.flush=function(a){for(var b=this;this.Xa.length;){var c=this.Xa[0];if(c.defer)c.defer=!1,this.Xa.push(c);else switch(c.type){case "require":if(3!==Xe(this,c.oa).status&&!a)return;break;case "set":ra(c.Db[0],function(h,k){b.Sd[h]=k});break;case "config":var d=c.Db[0],e=!!d[G.ub];delete d[G.ub];var g=Xe(this,c.oa);e||(g.uc={});g.Xd&&e||Ye(this,G.U,d,c);g.Xd=!0;f(d,g.uc);break;case "event":Ye(this,c.Db[1],c.Db[0],c)}this.Xa.shift()}};var Ze=new function(){this.Nc={}};var $e=null,af={},bf={},cf,df=function(a,b){var c={event:a};b&&(c.eventModel=f(b),b[G.$b]&&(c.eventCallback=b[G.$b]),b[G.Na]&&(c.eventTimeout=b[G.Na]));return c};
var kf={config:function(a){},event:function(a){var b=a[1];if(ia(b)&&!(3<a.length)){var c;
if(2<a.length){if(!Ha(a[2]))return;c=a[2]}var d=df(b,c);return d}},js:function(a){if(2==a.length&&a[1].getTime)return{event:"gtm.js","gtm.start":a[1].getTime()}},policy:function(a){if(3===a.length){var b=a[1],c=a[2];Ze.Nc[b]?Ze.Nc[b].push(c):Ze.Nc[b]=[c]}},set:function(a){var b;2==a.length&&Ha(a[1])?b=f(a[1]):3==a.length&&ia(a[1])&&(b={},b[a[1]]=a[2]);if(b){
b._clear=!0;return b}}},lf={policy:!0};var nf=function(a){return mf?C.querySelectorAll(a):null},of=function(a,b){if(!mf)return null;if(Element.prototype.closest)try{return a.closest(b)}catch(e){return null}var c=Element.prototype.matches||Element.prototype.webkitMatchesSelector||Element.prototype.mozMatchesSelector||Element.prototype.msMatchesSelector||Element.prototype.oMatchesSelector,d=a;if(!C.documentElement.contains(d))return null;do{try{if(c.call(d,b))return d}catch(e){break}d=d.parentElement||d.parentNode}while(null!==d&&1===d.nodeType);
return null},pf=!1;if(C.querySelectorAll)try{var qf=C.querySelectorAll(":root");qf&&1==qf.length&&qf[0]==C.documentElement&&(pf=!0)}catch(a){}var mf=pf;var xf=function(a){if(wf(a))return a;this.Zg=a};xf.prototype.Yf=function(){return this.Zg};var wf=function(a){return!a||"object"!==Fa(a)||Ha(a)?!1:"getUntrustedUpdateValue"in a};xf.prototype.getUntrustedUpdateValue=xf.prototype.Yf;var yf=!1,zf=[];function Af(){if(!yf){yf=!0;for(var a=0;a<zf.length;a++)D(zf[a])}}var Bf=function(a){yf?D(a):zf.push(a)};var Cf=[],Df=!1,Ef=function(a){return u["dataLayer"].push(a)},Ff=function(a){var b=Ic["dataLayer"],c=b?b.subscribers:1,d=0;return function(){++d===c&&a()}},Hf=function(a){var b=a._clear;ra(a,function(g,h){"_clear"!==g&&(b&&xd(g,void 0),xd(g,h))});Nc||(Nc=a["gtm.start"]);var c=a.event;if(!c)return!1;var d=a["gtm.uniqueEventId"];d||(d=Tc(),a["gtm.uniqueEventId"]=d,xd("gtm.uniqueEventId",d));Pc=c;var e=Gf(a);
Pc=null;switch(c){case "gtm.init":bb("GTM",19),e&&bb("GTM",20)}return e};function Gf(a){var b=a.event,c=a["gtm.uniqueEventId"],d,e=Ic.zones;d=e?e.checkState(Hc.i,c):Wd;return d.active?Ne(c,b,d.isWhitelisted,a.eventCallback,a.eventTimeout)?!0:!1:!1}
var If=function(){for(var a=!1;!Df&&0<Cf.length;){Df=!0;delete md.eventModel;pd();var b=Cf.shift();if(null!=b){var c=wf(b);if(c){var d=b;b=wf(d)?d.getUntrustedUpdateValue():void 0;for(var e=["gtm.whitelist","gtm.blacklist","tagTypeBlacklist"],g=0;g<e.length;g++){var h=e[g],k=qd(h,1);if(ka(k)||Ha(k))k=f(k);nd[h]=k}}try{if(ha(b))try{b.call(rd)}catch(v){}else if(ka(b)){var l=b;if(ia(l[0])){var m=
l[0].split("."),n=m.pop(),p=l.slice(1),t=qd(m.join("."),2);if(void 0!==t&&null!==t)try{t[n].apply(t,p)}catch(v){}}}else{var q=b;if(q&&("[object Arguments]"==Object.prototype.toString.call(q)||Object.prototype.hasOwnProperty.call(q,"callee"))){a:{if(b.length&&ia(b[0])){var r=kf[b[0]];if(r&&(!c||!lf[b[0]])){b=r(b);break a}}b=void 0}if(!b){Df=!1;continue}}a=Hf(b)||a}}finally{c&&pd(!0)}}Df=!1}
return!a},Jf=function(){var a=If();try{var b=Hc.i,c=u["dataLayer"].hide;if(c&&void 0!==c[b]&&c.end){c[b]=!1;var d=!0,e;for(e in c)if(c.hasOwnProperty(e)&&!0===c[e]){d=!1;break}d&&(c.end(),c.end=null)}}catch(g){}return a},Kf=function(){var a=Ka("dataLayer",[]),b=Ka("google_tag_manager",{});b=b["dataLayer"]=b["dataLayer"]||{};ce(function(){b.gtmDom||(b.gtmDom=!0,a.push({event:"gtm.dom"}))});Bf(function(){b.gtmLoad||(b.gtmLoad=!0,a.push({event:"gtm.load"}))});b.subscribers=(b.subscribers||
0)+1;var c=a.push;a.push=function(){var d;if(0<Ic.SANDBOXED_JS_SEMAPHORE){d=[];for(var e=0;e<arguments.length;e++)d[e]=new xf(arguments[e])}else d=[].slice.call(arguments,0);var g=c.apply(a,d);Cf.push.apply(Cf,d);if(300<this.length)for(bb("GTM",4);300<this.length;)this.shift();var h="boolean"!==typeof g||g;return If()&&h};Cf.push.apply(Cf,a.slice(0));D(Jf)};var Lf;var gg={};gg.wb=new String("undefined");
var hg=function(a){this.resolve=function(b){for(var c=[],d=0;d<a.length;d++)c.push(a[d]===gg.wb?b:a[d]);return c.join("")}};hg.prototype.toString=function(){return this.resolve("undefined")};hg.prototype.valueOf=hg.prototype.toString;gg.$e=hg;gg.kc={};gg.Jf=function(a){return new hg(a)};var ig={};gg.Ig=function(a,b){var c=Tc();ig[c]=[a,b];return c};gg.Kd=function(a){var b=a?0:1;return function(c){var d=ig[c];if(d&&"function"===typeof d[b])d[b]();ig[c]=void 0}};gg.kg=function(a){for(var b=!1,c=!1,
d=2;d<a.length;d++)b=b||8===a[d],c=c||16===a[d];return b&&c};gg.wg=function(a){if(a===gg.wb)return a;var b=Tc();gg.kc[b]=a;return'google_tag_manager["'+Hc.i+'"].macro('+b+")"};gg.og=function(a,b,c){a instanceof gg.$e&&(a=a.resolve(gg.Ig(b,c)),b=fa);return{Ac:a,I:b}};var jg=function(a,b,c){function d(g,h){var k=g[h];return k}var e={event:b,"gtm.element":a,"gtm.elementClasses":d(a,"className"),"gtm.elementId":a["for"]||Sa(a,"id")||"","gtm.elementTarget":a.formTarget||d(a,"target")||""};c&&(e["gtm.triggers"]=c.join(","));e["gtm.elementUrl"]=(a.attributes&&a.attributes.formaction?a.formAction:"")||a.action||d(a,"href")||a.src||a.code||a.codebase||
"";return e},kg=function(a){Ic.hasOwnProperty("autoEventsSettings")||(Ic.autoEventsSettings={});var b=Ic.autoEventsSettings;b.hasOwnProperty(a)||(b[a]={});return b[a]},lg=function(a,b,c){kg(a)[b]=c},mg=function(a,b,c,d){var e=kg(a),g=xa(e,b,d);e[b]=c(g)},ng=function(a,b,c){var d=kg(a);return xa(d,b,c)};var og=function(){for(var a=Ia.userAgent+(C.cookie||"")+(C.referrer||""),b=a.length,c=u.history.length;0<c;)a+=c--^b++;var d=1,e,g,h;if(a)for(d=0,g=a.length-1;0<=g;g--)h=a.charCodeAt(g),d=(d<<6&268435455)+h+(h<<14),e=d&266338304,d=0!=e?d^e>>21:d;return[Math.round(2147483647*Math.random())^d&2147483647,Math.round(wa()/1E3)].join(".")},rg=function(a,b,c,d){var e=pg(b);return nb(a,e,qg(c),d)},sg=function(a,b,c,d){var e=""+pg(c),g=qg(d);1<g&&(e+="-"+g);return[b,e,a].join(".")},pg=function(a){if(!a)return 1;
a=0===a.indexOf(".")?a.substr(1):a;return a.split(".").length},qg=function(a){if(!a||"/"===a)return 1;"/"!==a[0]&&(a="/"+a);"/"!==a[a.length-1]&&(a+="/");return a.split("/").length-1};var tg=["1"],ug={},yg=function(a,b,c,d){var e=vg(a);ug[e]||wg(e,b,c)||(xg(e,og(),b,c,d),wg(e,b,c))};function xg(a,b,c,d,e){var g=sg(b,"1",d,c);sb(a,g,c,d,0==e?void 0:new Date(wa()+1E3*(void 0==e?7776E3:e)))}function wg(a,b,c){var d=rg(a,b,c,tg);d&&(ug[a]=d);return d}function vg(a){return(a||"_gcl")+"_au"};var zg=function(){for(var a=[],b=C.cookie.split(";"),c=/^\s*_gac_(UA-\d+-\d+)=\s*(.+?)\s*$/,d=0;d<b.length;d++){var e=b[d].match(c);e&&a.push({Yc:e[1],value:e[2]})}var g={};if(!a||!a.length)return g;for(var h=0;h<a.length;h++){var k=a[h].value.split(".");"1"==k[0]&&3==k.length&&k[1]&&(g[a[h].Yc]||(g[a[h].Yc]=[]),g[a[h].Yc].push({timestamp:k[1],Vf:k[2]}))}return g};function Ag(){for(var a=Bg,b={},c=0;c<a.length;++c)b[a[c]]=c;return b}function Cg(){var a="ABCDEFGHIJKLMNOPQRSTUVWXYZ";a+=a.toLowerCase()+"0123456789-_";return a+"."}
var Bg,Dg,Eg=function(a){Bg=Bg||Cg();Dg=Dg||Ag();for(var b=[],c=0;c<a.length;c+=3){var d=c+1<a.length,e=c+2<a.length,g=a.charCodeAt(c),h=d?a.charCodeAt(c+1):0,k=e?a.charCodeAt(c+2):0,l=g>>2,m=(g&3)<<4|h>>4,n=(h&15)<<2|k>>6,p=k&63;e||(p=64,d||(n=64));b.push(Bg[l],Bg[m],Bg[n],Bg[p])}return b.join("")},Fg=function(a){function b(l){for(;d<a.length;){var m=a.charAt(d++),n=Dg[m];if(null!=n)return n;if(!/^[\s\xa0]*$/.test(m))throw Error("Unknown base64 encoding at char: "+m);}return l}Bg=Bg||Cg();Dg=Dg||
Ag();for(var c="",d=0;;){var e=b(-1),g=b(0),h=b(64),k=b(64);if(64===k&&-1===e)return c;c+=String.fromCharCode(e<<2|g>>4);64!=h&&(c+=String.fromCharCode(g<<4&240|h>>2),64!=k&&(c+=String.fromCharCode(h<<6&192|k)))}};var Gg;function Hg(a,b){if(!a||b===C.location.hostname)return!1;for(var c=0;c<a.length;c++)if(a[c]instanceof RegExp){if(a[c].test(b))return!0}else if(0<=b.indexOf(a[c]))return!0;return!1}
var Lg=function(){var a=Ig,b=Jg,c=Kg(),d=function(h){a(h.target||h.srcElement||{})},e=function(h){b(h.target||h.srcElement||{})};if(!c.init){Qa(C,"mousedown",d);Qa(C,"keyup",d);Qa(C,"submit",e);var g=HTMLFormElement.prototype.submit;HTMLFormElement.prototype.submit=function(){b(this);g.call(this)};c.init=!0}},Kg=function(){var a=Ka("google_tag_data",{}),b=a.gl;b&&b.decorators||(b={decorators:[]},a.gl=b);return b};var Mg=/(.*?)\*(.*?)\*(.*)/,Ng=/^https?:\/\/([^\/]*?)\.?cdn\.ampproject\.org\/?(.*)/,Og=/^(?:www\.|m\.|amp\.)+/,Pg=/([^?#]+)(\?[^#]*)?(#.*)?/,Qg=/(.*?)(^|&)_gl=([^&]*)&?(.*)/,Sg=function(a){var b=[],c;for(c in a)if(a.hasOwnProperty(c)){var d=a[c];void 0!==d&&d===d&&null!==d&&"[object Object]"!==d.toString()&&(b.push(c),b.push(Eg(String(d))))}var e=b.join("*");return["1",Rg(e),e].join("*")},Rg=function(a,b){var c=[window.navigator.userAgent,(new Date).getTimezoneOffset(),window.navigator.userLanguage||
window.navigator.language,Math.floor((new Date).getTime()/60/1E3)-(void 0===b?0:b),a].join("*"),d;if(!(d=Gg)){for(var e=Array(256),g=0;256>g;g++){for(var h=g,k=0;8>k;k++)h=h&1?h>>>1^3988292384:h>>>1;e[g]=h}d=e}Gg=d;for(var l=4294967295,m=0;m<c.length;m++)l=l>>>8^Gg[(l^c.charCodeAt(m))&255];return((l^-1)>>>0).toString(36)},Ug=function(){return function(a){var b=jb(u.location.href),c=b.search.replace("?",""),d=eb(c,"_gl",!0)||"";a.query=Tg(d)||{};var e=ib(b,"fragment").match(Qg);a.fragment=Tg(e&&e[3]||
"")||{}}},Vg=function(){var a=Ug(),b=Kg();b.data||(b.data={query:{},fragment:{}},a(b.data));var c={},d=b.data;d&&(za(c,d.query),za(c,d.fragment));return c},Tg=function(a){var b;b=void 0===b?3:b;try{if(a){var c;a:{for(var d=a,e=0;3>e;++e){var g=Mg.exec(d);if(g){c=g;break a}d=decodeURIComponent(d)}c=void 0}var h=c;if(h&&"1"===h[1]){var k=h[3],l;a:{for(var m=h[2],n=0;n<b;++n)if(m===Rg(k,n)){l=!0;break a}l=!1}if(l){for(var p={},t=k?k.split("*"):[],q=0;q<t.length;q+=2)p[t[q]]=Fg(t[q+1]);return p}}}}catch(r){}};
function Wg(a,b,c){function d(m){var n=m,p=Qg.exec(n),t=n;if(p){var q=p[2],r=p[4];t=p[1];r&&(t=t+q+r)}m=t;var v=m.charAt(m.length-1);m&&"&"!==v&&(m+="&");return m+l}c=void 0===c?!1:c;var e=Pg.exec(b);if(!e)return"";var g=e[1],h=e[2]||"",k=e[3]||"",l="_gl="+a;c?k="#"+d(k.substring(1)):h="?"+d(h.substring(1));return""+g+h+k}
function Xg(a,b,c){for(var d={},e={},g=Kg().decorators,h=0;h<g.length;++h){var k=g[h];(!c||k.forms)&&Hg(k.domains,b)&&(k.fragment?za(e,k.callback()):za(d,k.callback()))}if(Aa(d)){var l=Sg(d);if(c){if(a&&a.action){var m=(a.method||"").toLowerCase();if("get"===m){for(var n=a.childNodes||[],p=!1,t=0;t<n.length;t++){var q=n[t];if("_gl"===q.name){q.setAttribute("value",l);p=!0;break}}if(!p){var r=C.createElement("input");r.setAttribute("type","hidden");r.setAttribute("name","_gl");r.setAttribute("value",
l);a.appendChild(r)}}else if("post"===m){var v=Wg(l,a.action);$a.test(v)&&(a.action=v)}}}else Yg(l,a,!1)}if(!c&&Aa(e)){var w=Sg(e);Yg(w,a,!0)}}function Yg(a,b,c){if(b.href){var d=Wg(a,b.href,void 0===c?!1:c);$a.test(d)&&(b.href=d)}}
var Ig=function(a){try{var b;a:{for(var c=a,d=100;c&&0<d;){if(c.href&&c.nodeName.match(/^a(?:rea)?$/i)){b=c;break a}c=c.parentNode;d--}b=null}var e=b;if(e){var g=e.protocol;"http:"!==g&&"https:"!==g||Xg(e,e.hostname,!1)}}catch(h){}},Jg=function(a){try{if(a.action){var b=ib(jb(a.action),"host");Xg(a,b,!0)}}catch(c){}},Zg=function(a,b,c,d){Lg();var e={callback:a,domains:b,fragment:"fragment"===c,forms:!!d};Kg().decorators.push(e)},$g=function(){var a=C.location.hostname,b=Ng.exec(C.referrer);if(!b)return!1;
var c=b[2],d=b[1],e="";if(c){var g=c.split("/"),h=g[1];e="s"===h?decodeURIComponent(g[2]):decodeURIComponent(h)}else if(d){if(0===d.indexOf("xn--"))return!1;e=d.replace(/-/g,".").replace(/\.\./g,"-")}return a.replace(Og,"")===e.replace(Og,"")},ah=function(a,b){return!1===a?!1:a||b||$g()};var bh={};var ch=/^\w+$/,dh=/^[\w-]+$/,eh=/^~?[\w-]+$/,fh={aw:"_aw",dc:"_dc",gf:"_gf",ha:"_ha"};function gh(a){return a&&"string"==typeof a&&a.match(ch)?a:"_gcl"}var ih=function(){var a=jb(u.location.href),b=ib(a,"query",!1,void 0,"gclid"),c=ib(a,"query",!1,void 0,"gclsrc"),d=ib(a,"query",!1,void 0,"dclid");if(!b||!c){var e=a.hash.replace("#","");b=b||eb(e,"gclid",void 0);c=c||eb(e,"gclsrc",void 0)}return hh(b,c,d)};
function hh(a,b,c){var d={},e=function(g,h){d[h]||(d[h]=[]);d[h].push(g)};if(void 0!==a&&a.match(dh))switch(b){case void 0:e(a,"aw");break;case "aw.ds":e(a,"aw");e(a,"dc");break;case "ds":e(a,"dc");break;case "3p.ds":(void 0==bh.gtm_3pds?0:bh.gtm_3pds)&&e(a,"dc");break;case "gf":e(a,"gf");break;case "ha":e(a,"ha")}c&&e(c,"dc");return d}
function jh(a,b,c){function d(p,t){var q=kh(p,e);q&&sb(q,t,h,g,l,!0)}b=b||{};var e=gh(b.prefix),g=b.domain||"auto",h=b.path||"/",k=void 0==b.be?7776E3:b.be;c=c||wa();var l=0==k?void 0:new Date(c+1E3*k),m=Math.round(c/1E3),n=function(p){return["GCL",m,p].join(".")};a.aw&&(!0===b.Fh?d("aw",n("~"+a.aw[0])):d("aw",n(a.aw[0])));a.dc&&d("dc",n(a.dc[0]));a.gf&&d("gf",n(a.gf[0]));a.ha&&d("ha",n(a.ha[0]))}
var kh=function(a,b){var c=fh[a];if(void 0!==c)return b+c},lh=function(a){var b=a.split(".");return 3!==b.length||"GCL"!==b[0]?0:1E3*(Number(b[1])||0)};function mh(a){var b=a.split(".");if(3==b.length&&"GCL"==b[0]&&b[1])return b[2]}
var nh=function(a,b,c,d,e){if(ka(b)){var g=gh(e);Zg(function(){for(var h={},k=0;k<a.length;++k){var l=kh(a[k],g);if(l){var m=kb(l,C.cookie);m.length&&(h[l]=m.sort()[m.length-1])}}return h},b,c,d)}},oh=function(a){return a.filter(function(b){return eh.test(b)})},qh=function(a){for(var b=["aw","dc"],c=gh(a&&a.prefix),d={},e=0;e<b.length;e++)fh[b[e]]&&(d[b[e]]=fh[b[e]]);ra(d,function(g,h){var k=kb(c+h,C.cookie);if(k.length){var l=k[0],m=lh(l),n={};n[g]=[mh(l)];jh(n,a,m)}})};var rh=/^\d+\.fls\.doubleclick\.net$/;function sh(a){var b=jb(u.location.href),c=ib(b,"host",!1);if(c&&c.match(rh)){var d=ib(b,"path").split(a+"=");if(1<d.length)return d[1].split(";")[0].split("?")[0]}}
function th(a,b){if("aw"==a||"dc"==a){var c=sh("gcl"+a);if(c)return c.split(".")}var d=gh(b);if("_gcl"==d){var e;e=ih()[a]||[];if(0<e.length)return e}var g=kh(a,d),h;if(g){var k=[];if(C.cookie){var l=kb(g,C.cookie);if(l&&0!=l.length){for(var m=0;m<l.length;m++){var n=mh(l[m]);n&&-1===la(k,n)&&k.push(n)}h=oh(k)}else h=k}else h=k}else h=[];return h}
var uh=function(){var a=sh("gac");if(a)return decodeURIComponent(a);var b=zg(),c=[];ra(b,function(d,e){for(var g=[],h=0;h<e.length;h++)g.push(e[h].Vf);g=oh(g);g.length&&c.push(d+":"+g.join(","))});return c.join(";")},vh=function(a,b,c,d,e){yg(b,c,d,e);var g=ug[vg(b)],h=ih().dc||[],k=!1;if(g&&0<h.length){var l=Ic.joined_au=Ic.joined_au||{},m=b||"_gcl";if(!l[m])for(var n=0;n<h.length;n++){var p="https://adservice.google.com/ddm/regclk",t=p=p+"?gclid="+h[n]+"&auiddc="+g;Ia.sendBeacon&&Ia.sendBeacon(t)||Pa(t);k=l[m]=
!0}}null==a&&(a=k);if(a&&g){var q=vg(b),r=ug[q];r&&xg(q,r,c,d,e)}};var wh;if(3===Hc.Ab.length)wh="g";else{var xh="G";wh=xh}
var yh={"":"n",UA:"u",AW:"a",DC:"d",G:"e",GF:"f",HA:"h",GTM:wh,OPT:"o"},zh=function(a){var b=Hc.i.split("-"),c=b[0].toUpperCase(),d=yh[c]||"i",e=a&&"GTM"===c?b[1]:"OPT"===c?b[1]:"",g;if(3===Hc.Ab.length){var h=void 0;g="2"+(h||"w")}else g=
"";return g+d+Hc.Ab+e};
var Ah=function(a){return!(void 0===a||null===a||0===(a+"").length)},Bh=function(a,b){var c;if(2===b.L)return a("ord",oa(1E11,1E13)),!0;if(3===b.L)return a("ord","1"),a("num",oa(1E11,1E13)),!0;if(4===b.L)return Ah(b.sessionId)&&a("ord",b.sessionId),!0;if(5===b.L)c="1";else if(6===b.L)c=b.Sc;else return!1;Ah(c)&&a("qty",c);Ah(b.Fb)&&a("cost",b.Fb);Ah(b.transactionId)&&a("ord",b.transactionId);return!0},Ch=encodeURIComponent,Dh=function(a,b){function c(n,p,t){g.hasOwnProperty(n)||(p+="",e+=";"+n+"="+
(t?p:Ch(p)))}var d=a.xc,e=a.protocol;e+=a.Qb?"//"+d+".fls.doubleclick.net/activityi":"//ad.doubleclick.net/activity";e+=";src="+Ch(d)+(";type="+Ch(a.zc))+(";cat="+Ch(a.Wa));var g=a.Lf||{};ra(g,function(n,p){e+=";"+Ch(n)+"="+Ch(p+"")});if(Bh(c,a)){Ah(a.Wb)&&c("u",a.Wb);Ah(a.Vb)&&c("tran",a.Vb);c("gtm",zh());!1===a.kf&&c("npa","1");if(a.vc){var h=th("dc",a.Aa);h&&h.length&&c("gcldc",h.join("."));var k=th("aw",a.Aa);k&&k.length&&c("gclaw",k.join("."));var l=uh();l&&c("gac",l);yg(a.Aa,void 0,a.Hf,a.If);
var m=ug[vg(a.Aa)];m&&c("auiddc",m)}Ah(a.Oc)&&c("prd",a.Oc,!0);ra(a.$c,function(n,p){c(n,p)});e+=b||"";Ah(a.Ob)&&c("~oref",a.Ob);a.Qb?Oa(e+"?",a.I):Pa(e+"?",a.I,a.S)}else D(a.S)};var Eh=["input","select","textarea"],Fh=["button","hidden","image","reset","submit"],Gh=function(a){var b=a.tagName.toLowerCase();return!ma(Eh,function(c){return c===b})||"input"===b&&ma(Fh,function(c){return c===a.type.toLowerCase()})?!1:!0},Hh=function(a){return a.form?a.form.tagName?a.form:C.getElementById(a.form):Ya(a,["form"],100)},Ih=function(a,b,c){if(!a.elements)return 0;for(var d=b.getAttribute(c),e=0,g=1;e<a.elements.length;e++){var h=a.elements[e];if(Gh(h)){if(h.getAttribute(c)===d)return g;
g++}}return 0};var Lh=!!u.MutationObserver,Mh=void 0,Nh=function(a){if(!Mh){var b=function(){var c=C.body;if(c)if(Lh)(new MutationObserver(function(){for(var e=0;e<Mh.length;e++)D(Mh[e])})).observe(c,{childList:!0,subtree:!0});else{var d=!1;Qa(c,"DOMNodeInserted",function(){d||(d=!0,D(function(){d=!1;for(var e=0;e<Mh.length;e++)D(Mh[e])}))})}};Mh=[];C.body?b():D(b)}Mh.push(a)};
var ei=function(){var a=C.body,b=C.documentElement||a&&a.parentElement,c,d;if(C.compatMode&&"BackCompat"!==C.compatMode)c=b?b.clientHeight:0,d=b?b.clientWidth:0;else{var e=function(g,h){return g&&h?Math.min(g,h):Math.max(g,h)};bb("GTM",7);c=e(b?b.clientHeight:0,a?a.clientHeight:0);d=e(b?b.clientWidth:0,a?a.clientWidth:0)}return{width:d,height:c}},fi=function(a){var b=ei(),c=b.height,d=b.width,e=a.getBoundingClientRect(),g=e.bottom-e.top,h=e.right-e.left;return g&&h?(1-Math.min((Math.max(0-e.left,
0)+Math.max(e.right-d,0))/h,1))*(1-Math.min((Math.max(0-e.top,0)+Math.max(e.bottom-c,0))/g,1)):0},gi=function(a){if(C.hidden)return!0;var b=a.getBoundingClientRect();if(b.top==b.bottom||b.left==b.right||!u.getComputedStyle)return!0;var c=u.getComputedStyle(a,null);if("hidden"===c.visibility)return!0;for(var d=a,e=c;d;){if("none"===e.display)return!0;var g=e.opacity,h=e.filter;if(h){var k=h.indexOf("opacity(");0<=k&&(h=h.substring(k+8,h.indexOf(")",k)),"%"==h.charAt(h.length-1)&&(h=h.substring(0,h.length-
1)),g=Math.min(h,g))}if(void 0!==g&&0>=g)return!0;(d=d.parentElement)&&(e=u.getComputedStyle(d,null))}return!1};var hi=[],ii=!(!u.IntersectionObserver||!u.IntersectionObserverEntry),ji=function(a,b,c){for(var d=new u.IntersectionObserver(a,{threshold:c}),e=0;e<b.length;e++)d.observe(b[e]);for(var g=0;g<hi.length;g++)if(!hi[g])return hi[g]=d,g;return hi.push(d)-1},ki=function(a,b,c){function d(k,l){var m={top:0,bottom:0,right:0,left:0,width:0,
height:0},n={boundingClientRect:k.getBoundingClientRect(),intersectionRatio:l,intersectionRect:m,isIntersecting:0<l,rootBounds:m,target:k,time:wa()};D(function(){return a(n)})}for(var e=[],g=[],h=0;h<b.length;h++)e.push(0),g.push(-1);c.sort(function(k,l){return k-l});return function(){for(var k=0;k<b.length;k++){var l=fi(b[k]);if(l>e[k])for(;g[k]<c.length-1&&l>=c[g[k]+1];)d(b[k],l),g[k]++;else if(l<e[k])for(;0<=g[k]&&l<=c[g[k]];)d(b[k],l),g[k]--;e[k]=l}}},li=function(a,b,c){for(var d=0;d<c.length;d++)1<
c[d]?c[d]=1:0>c[d]&&(c[d]=0);if(ii){var e=!1;D(function(){e||ki(a,b,c)()});return ji(function(g){e=!0;for(var h={Ia:0};h.Ia<g.length;h={Ia:h.Ia},h.Ia++)D(function(k){return function(){return a(g[k.Ia])}}(h))},b,c)}return u.setInterval(ki(a,b,c),1E3)},mi=function(a){ii?0<=a&&a<hi.length&&hi[a]&&(hi[a].disconnect(),hi[a]=void 0):u.clearInterval(a)};var oi=u.clearTimeout,pi=u.setTimeout,L=function(a,b,c){if(Ad()){b&&D(b)}else return Ma(a,b,c)},qi=function(){return new Date},ri=function(){return u.location.href},si=function(a){return ib(jb(a),"fragment")},ti=function(a){return hb(jb(a))},ui=null;
var vi=function(a,b){return qd(a,b||2)},wi=function(a,b,c){b&&(a.eventCallback=b,c&&(a.eventTimeout=c));return Ef(a)},xi=function(a,b){u[a]=b},M=function(a,b,c){b&&(void 0===u[a]||c&&!u[a])&&(u[a]=b);return u[a]},yi=function(a,b,c){return kb(a,b,void 0===c?!0:!!c)},zi=function(a,b,c,d){var e={prefix:a,path:b,domain:c,be:d},g=ih();jh(g,e);qh(e)},Ai=function(a,b,c,d,e){for(var g=Vg(),h=gh(b),k=0;k<a.length;++k){var l=a[k];if(void 0!==fh[l]){var m=kh(l,h),
n=g[m];if(n){var p=Math.min(lh(n),wa()),t;b:{for(var q=p,r=kb(m,C.cookie),v=0;v<r.length;++v)if(lh(r[v])>q){t=!0;break b}t=!1}t||sb(m,n,c,d,0==e?void 0:new Date(p+1E3*(null==e?7776E3:e)),!0)}}}var w={prefix:b,path:c,domain:d};jh(hh(g.gclid,g.gclsrc),w);},Bi=function(a,b,c,d,e){nh(a,b,c,d,e);},Ci=function(a,b){if(Ad()){
b&&D(b)}else Oa(a,b)},Di=function(a){return!!ng(a,"init",!1)},Ei=function(a){lg(a,"init",!0)},Fi=function(a,b,c){var d=(void 0===c?0:c)?"www.googletagmanager.com/gtag/js":Mc;d+="?id="+encodeURIComponent(a)+"&l=dataLayer";b&&ra(b,function(e,g){g&&(d+="&"+e+"="+encodeURIComponent(g))});L(H("https://","http://",d))},Gi=function(a,b){var c=a[b];return c};

var Ii=gg.og;
var Ji=new pa,Ki=function(a,b){function c(h){var k=jb(h),l=ib(k,"protocol"),m=ib(k,"host",!0),n=ib(k,"port"),p=ib(k,"path").toLowerCase().replace(/\/$/,"");if(void 0===l||"http"==l&&"80"==n||"https"==l&&"443"==n)l="web",n="default";return[l,m,n,p]}for(var d=c(String(a)),e=c(String(b)),g=0;g<d.length;g++)if(d[g]!==e[g])return!1;return!0},Li=function(a){var b=a.arg0,c=a.arg1;if(a.any_of&&ka(c)){for(var d=0;d<c.length;d++)if(Li({"function":a["function"],arg0:b,arg1:c[d]}))return!0;return!1}switch(a["function"]){case "_cn":return 0<=
String(b).indexOf(String(c));case "_css":var e;a:{if(b){var g=["matches","webkitMatchesSelector","mozMatchesSelector","msMatchesSelector","oMatchesSelector"];try{for(var h=0;h<g.length;h++)if(b[g[h]]){e=b[g[h]](c);break a}}catch(v){}}e=!1}return e;case "_ew":var k,l;k=String(b);l=String(c);var m=k.length-l.length;return 0<=m&&k.indexOf(l,m)==m;case "_eq":return String(b)==String(c);case "_ge":return Number(b)>=Number(c);case "_gt":return Number(b)>Number(c);case "_lc":var n;n=String(b).split(",");
return 0<=la(n,String(c));case "_le":return Number(b)<=Number(c);case "_lt":return Number(b)<Number(c);case "_re":var p;var t=a.ignore_case?"i":void 0;try{var q=String(c)+t,r=Ji.get(q);r||(r=new RegExp(c,t),Ji.set(q,r));p=r.test(b)}catch(v){p=!1}return p;case "_sw":return 0==String(b).indexOf(String(c));case "_um":return Ki(b,c)}return!1};var Ni=function(a,b){var c=function(){};c.prototype=a.prototype;var d=new c;a.apply(d,Array.prototype.slice.call(arguments,1));return d};var Oi={},Pi=encodeURI,W=encodeURIComponent,Qi=Pa;var Ri=function(a,b){if(!a)return!1;var c=ib(jb(a),"host");if(!c)return!1;for(var d=0;b&&d<b.length;d++){var e=b[d]&&b[d].toLowerCase();if(e){var g=c.length-e.length;0<g&&"."!=e.charAt(0)&&(g--,e="."+e);if(0<=g&&c.indexOf(e,g)==g)return!0}}return!1};
var Si=function(a,b,c){for(var d={},e=!1,g=0;a&&g<a.length;g++)a[g]&&a[g].hasOwnProperty(b)&&a[g].hasOwnProperty(c)&&(d[a[g][b]]=a[g][c],e=!0);return e?d:null};Oi.lg=function(){var a=!1;return a};var rj=function(){var a=u.gaGlobal=u.gaGlobal||{};a.hid=a.hid||oa();return a.hid};var ak=window,bk=document,ck=function(a){var b=ak._gaUserPrefs;if(b&&b.ioo&&b.ioo()||a&&!0===ak["ga-disable-"+a])return!0;try{var c=ak.external;if(c&&c._gaUserPrefs&&"oo"==c._gaUserPrefs)return!0}catch(g){}for(var d=kb("AMP_TOKEN",bk.cookie,!0),e=0;e<d.length;e++)if("$OPT_OUT"==d[e])return!0;return bk.getElementById("__gaOptOutExtension")?!0:!1};var hk=function(a,b,c){Ue(b,c,a)},ik=function(a,b,c){Ue(b,c,a,!0)},kk=function(a,b){},jk=function(a,b){},
lk=function(a){return"_"===a.charAt(0)},mk=function(a){ra(a,function(c){lk(c)&&delete a[c]});var b=a[G.vb]||{};ra(b,function(c){lk(c)&&delete b[c]})};var Y={a:{}};


Y.a.jsm=["customScripts"],function(){(function(a){Y.__jsm=a;Y.__jsm.b="jsm";Y.__jsm.g=!0;Y.__jsm.priorityOverride=0})(function(a){if(void 0!==a.vtp_javascript){var b=a.vtp_javascript;try{var c=M("google_tag_manager");return c&&c.e&&c.e(b)}catch(d){}}})}();
Y.a.flc=[],function(){function a(b,c){c=c?c.slice(0,-1):void 0;Dh(b,c)}(function(b){Y.__flc=b;Y.__flc.b="flc";Y.__flc.g=!0;Y.__flc.priorityOverride=0})(function(b){var c=!b.hasOwnProperty("vtp_enableConversionLinker")||b.vtp_enableConversionLinker,d=Si(b.vtp_customVariable||[],"key","value")||{},e={Wa:b.vtp_activityTag,vc:c,Aa:b.vtp_conversionCookiePrefix||void 0,Fb:void 0,L:{UNIQUE:3,SESSION:4}[b.vtp_ordinalType]||2,xc:b.vtp_advertiserId,zc:b.vtp_groupTag,S:b.vtp_gtmOnFailure,I:b.vtp_gtmOnSuccess,
Ob:b.vtp_useImageTag?void 0:b.vtp_url,protocol:"",Sc:void 0,Qb:!b.vtp_useImageTag,sessionId:b.vtp_sessionId,Vb:b.vtp_transactionVariable,transactionId:void 0,Wb:b.vtp_userVariable,$c:d};if(b.vtp_enableAttribution){var g=b.vtp_attributionFields||[];if(g.length){L("//www.gstatic.com/attribution/collection/attributiontools.js",function(){a(e,M("google_attr").build([Si(g,"key","value")||{}]))},b.vtp_gtmOnFailure);return}}a(e)})}();
Y.a.sp=["google"],function(){(function(a){Y.__sp=a;Y.__sp.b="sp";Y.__sp.g=!0;Y.__sp.priorityOverride=0})(function(a){var b=a.vtp_gtmOnFailure;le();L("//www.googleadservices.com/pagead/conversion_async.js",function(){var c=M("google_trackConversion");if(ha(c)){var d={};"DATA_LAYER"==a.vtp_customParamsFormat?d=a.vtp_dataLayerVariable:"USER_SPECIFIED"==a.vtp_customParamsFormat&&(d=Si(a.vtp_customParams,"key","value"));var e={};a.vtp_enableDynamicRemarketing&&(a.vtp_eventName&&(d.event=a.vtp_eventName),
a.vtp_eventValue&&(e.value=a.vtp_eventValue),a.vtp_eventItems&&(e.items=a.vtp_eventItems));c({google_conversion_id:a.vtp_conversionId,google_conversion_label:a.vtp_conversionLabel,google_custom_params:d,google_gtag_event_data:e,google_remarketing_only:!0,onload_callback:a.vtp_gtmOnSuccess,google_gtm:zh()})||b()}else b()},b)})}();Y.a.c=["google"],function(){(function(a){Y.__c=a;Y.__c.b="c";Y.__c.g=!0;Y.__c.priorityOverride=0})(function(a){return a.vtp_value})}();
Y.a.d=["google"],function(){(function(a){Y.__d=a;Y.__d.b="d";Y.__d.g=!0;Y.__d.priorityOverride=0})(function(a){var b=null,c=null,d=a.vtp_attributeName;if("CSS"==a.vtp_selectorType){var e=nf(a.vtp_elementSelector);e&&0<e.length&&(b=e[0])}else b=C.getElementById(a.vtp_elementId);b&&(c=d?Sa(b,d):Wa(b));return va(String(b&&c))})}();
Y.a.e=["google"],function(){(function(a){Y.__e=a;Y.__e.b="e";Y.__e.g=!0;Y.__e.priorityOverride=0})(function(a){return String(zd(a.vtp_gtmEventId,"event"))})}();
Y.a.f=["google"],function(){(function(a){Y.__f=a;Y.__f.b="f";Y.__f.g=!0;Y.__f.priorityOverride=0})(function(a){var b=vi("gtm.referrer",1)||C.referrer;return b?a.vtp_component&&"URL"!=a.vtp_component?ib(jb(String(b)),a.vtp_component,a.vtp_stripWww,a.vtp_defaultPages,a.vtp_queryKey):ti(String(b)):String(b)})}();
Y.a.cl=["google"],function(){function a(b){var c=b.target;if(c){var d=jg(c,"gtm.click");wi(d)}}(function(b){Y.__cl=b;Y.__cl.b="cl";Y.__cl.g=!0;Y.__cl.priorityOverride=0})(function(b){if(!Di("cl")){var c=M("document");Qa(c,"click",a,!0);Ei("cl")}D(b.vtp_gtmOnSuccess)})}();
Y.a.j=["google"],function(){(function(a){Y.__j=a;Y.__j.b="j";Y.__j.g=!0;Y.__j.priorityOverride=0})(function(a){for(var b=String(a.vtp_name).split("."),c=M(b.shift()),d=0;d<b.length;d++)c=c&&c[b[d]];return c})}();

Y.a.r=["google"],function(){(function(a){Y.__r=a;Y.__r.b="r";Y.__r.g=!0;Y.__r.priorityOverride=0})(function(a){return oa(a.vtp_min,a.vtp_max)})}();
Y.a.t=["google"],function(){(function(a){Y.__t=a;Y.__t.b="t";Y.__t.g=!0;Y.__t.priorityOverride=0})(function(){return qi().getTime()})}();
Y.a.u=["google"],function(){var a=function(b){return{toString:function(){return b}}};(function(b){Y.__u=b;Y.__u.b="u";Y.__u.g=!0;Y.__u.priorityOverride=0})(function(b){var c;c=(c=b.vtp_customUrlSource?b.vtp_customUrlSource:vi("gtm.url",1))||ri();var d=b[a("vtp_component")];if(!d||"URL"==d)return ti(String(c));var e=jb(String(c)),g;if("QUERY"===d)a:{var h=b[a("vtp_multiQueryKeys").toString()],k=b[a("vtp_queryKey").toString()]||"",l=b[a("vtp_ignoreEmptyQueryParam").toString()],m;m=h?ka(k)?k:String(k).replace(/\s+/g,
"").split(","):[String(k)];for(var n=0;n<m.length;n++){var p=ib(e,"QUERY",void 0,void 0,m[n]);if(void 0!=p&&(!l||""!==p)){g=p;break a}}g=void 0}else g=ib(e,d,"HOST"==d?b[a("vtp_stripWww")]:void 0,"PATH"==d?b[a("vtp_defaultPages")]:void 0,void 0);return g})}();
Y.a.v=["google"],function(){(function(a){Y.__v=a;Y.__v.b="v";Y.__v.g=!0;Y.__v.priorityOverride=0})(function(a){var b=a.vtp_name;if(!b||!b.replace)return!1;var c=vi(b.replace(/\\\./g,"."),a.vtp_dataLayerVersion||1);return void 0!==c?c:a.vtp_defaultValue})}();
Y.a.ua=["google"],function(){var a,b={},c=function(d){var e={},g={},h={},k={},l={},m=void 0;if(d.vtp_gaSettings){var n=d.vtp_gaSettings;f(Si(n.vtp_fieldsToSet,"fieldName","value"),g);f(Si(n.vtp_contentGroup,"index","group"),h);f(Si(n.vtp_dimension,"index","dimension"),k);f(Si(n.vtp_metric,"index","metric"),l);d.vtp_gaSettings=null;n.vtp_fieldsToSet=void 0;n.vtp_contentGroup=void 0;n.vtp_dimension=void 0;n.vtp_metric=void 0;var p=f(n);d=f(d,p)}f(Si(d.vtp_fieldsToSet,"fieldName","value"),g);f(Si(d.vtp_contentGroup,
"index","group"),h);f(Si(d.vtp_dimension,"index","dimension"),k);f(Si(d.vtp_metric,"index","metric"),l);var t=se(d.vtp_functionName);if(ha(t)){var q="",r="";d.vtp_setTrackerName&&"string"==typeof d.vtp_trackerName?""!==d.vtp_trackerName&&(r=d.vtp_trackerName,q=r+"."):(r="gtm"+Tc(),q=r+".");var v={name:!0,clientId:!0,sampleRate:!0,siteSpeedSampleRate:!0,alwaysSendReferrer:!0,allowAnchor:!0,allowLinker:!0,cookieName:!0,cookieDomain:!0,cookieExpires:!0,cookiePath:!0,cookieUpdate:!0,legacyCookieDomain:!0,
legacyHistoryImport:!0,storage:!0,useAmpClientId:!0,storeGac:!0},w={allowAnchor:!0,allowLinker:!0,alwaysSendReferrer:!0,anonymizeIp:!0,cookieUpdate:!0,exFatal:!0,forceSSL:!0,javaEnabled:!0,legacyHistoryImport:!0,nonInteraction:!0,useAmpClientId:!0,useBeacon:!0,storeGac:!0,allowAdFeatures:!0},x=function(R){var O=[].slice.call(arguments,0);O[0]=q+O[0];t.apply(window,O)},y=function(R,O){return void 0===O?O:R(O)},z=function(R,O){if(O)for(var na in O)O.hasOwnProperty(na)&&x("set",R+na,O[na])},B=function(){},A=function(R,O,na){var Ta=0;if(R)for(var Ba in R)if(R.hasOwnProperty(Ba)&&(na&&v[Ba]||!na&&void 0===v[Ba])){var Ua=w[Ba]?ta(R[Ba]):R[Ba];"anonymizeIp"!=Ba||Ua||(Ua=void 0);O[Ba]=Ua;Ta++}return Ta},E={name:r};A(g,E,!0);t("create",d.vtp_trackingId||e.trackingId,E);x("set","&gtm",zh(!0));d.vtp_enableRecaptcha&&x("require","recaptcha","recaptcha.js");(function(R,O){void 0!==d[O]&&x("set",R,d[O])})("nonInteraction","vtp_nonInteraction");z("contentGroup",h);z("dimension",k);z("metric",l);var F={};A(g,F,!1)&&x("set",F);var I;
d.vtp_enableLinkId&&x("require","linkid","linkid.js");x("set","hitCallback",function(){var R=g&&g.hitCallback;ha(R)&&R();d.vtp_gtmOnSuccess()});if("TRACK_EVENT"==d.vtp_trackType){d.vtp_enableEcommerce&&(x("require","ec","ec.js"),B());var S={hitType:"event",eventCategory:String(d.vtp_eventCategory||e.category),eventAction:String(d.vtp_eventAction||e.action),eventLabel:y(String,d.vtp_eventLabel||e.label),eventValue:y(sa,d.vtp_eventValue||
e.value)};A(I,S,!1);x("send",S);}else if("TRACK_SOCIAL"==d.vtp_trackType){}else if("TRACK_TRANSACTION"==d.vtp_trackType){}else if("TRACK_TIMING"==
d.vtp_trackType){var P={hitType:"timing",timingCategory:String(d.vtp_timingCategory||e.category),timingVar:String(d.vtp_timingVar||e.name),timingValue:sa(d.vtp_timingValue||e.value),timingLabel:y(String,d.vtp_timingLabel||e.label)};A(I,P,!1);x("send",P);}else if("DECORATE_LINK"==d.vtp_trackType){}else if("DECORATE_FORM"==d.vtp_trackType){}else if("TRACK_DATA"==d.vtp_trackType){}else{d.vtp_enableEcommerce&&(x("require","ec","ec.js"),B());if(d.vtp_doubleClick||"DISPLAY_FEATURES"==d.vtp_advertisingFeaturesType){var X=
"_dc_gtm_"+String(d.vtp_trackingId).replace(/[^A-Za-z0-9-]/g,"");x("require","displayfeatures",void 0,{cookieName:X})}if("DISPLAY_FEATURES_WITH_REMARKETING_LISTS"==d.vtp_advertisingFeaturesType){var Z="_dc_gtm_"+String(d.vtp_trackingId).replace(/[^A-Za-z0-9-]/g,"");x("require","adfeatures",{cookieName:Z})}I?x("send","pageview",I):x("send","pageview");}if(!a){var ba=d.vtp_useDebugVersion?"u/analytics_debug.js":"analytics.js";d.vtp_useInternalVersion&&!d.vtp_useDebugVersion&&(ba="internal/"+ba);a=!0;var Ca=H("https:","http:","//www.google-analytics.com/"+ba,g&&g.forceSSL);L(Ca,function(){var R=qe();R&&R.loaded||d.vtp_gtmOnFailure();},d.vtp_gtmOnFailure)}}else D(d.vtp_gtmOnFailure)};Y.__ua=c;Y.__ua.b="ua";Y.__ua.g=!0;Y.__ua.priorityOverride=0}();



Y.a.gclidw=["google"],function(){var a=["aw","dc","gf","ha"];(function(b){Y.__gclidw=b;Y.__gclidw.b="gclidw";Y.__gclidw.g=!0;Y.__gclidw.priorityOverride=100})(function(b){D(b.vtp_gtmOnSuccess);var c,d,e;b.vtp_enableCookieOverrides&&(e=b.vtp_cookiePrefix,c=b.vtp_path,d=b.vtp_domain);var g=null;b.vtp_enableCookieUpdateFeature&&(g=!0,void 0!==b.vtp_cookieUpdate&&(g=!!b.vtp_cookieUpdate));var h=e,k=c,l=d;b.vtp_enableCrossDomainFeature&&(b.vtp_enableCrossDomain&&!1===b.vtp_acceptIncoming||(b.vtp_enableCrossDomain||
$g())&&Ai(a,h,k,l));zi(e,c,d);vh(g,e,c,d);var m=e;if(b.vtp_enableCrossDomainFeature&&b.vtp_enableCrossDomain&&b.vtp_linkerDomains){var n=b.vtp_linkerDomains.toString().replace(/\s+/g,"").split(",");Bi(a,n,b.vtp_urlPosition,!!b.vtp_formDecoration,m)}})}();

Y.a.aev=["google"],function(){function a(q,r){var v=zd(q,"gtm");if(v)return v[r]}function b(q,r,v,w){w||(w="element");var x=q+"."+r,y;if(n.hasOwnProperty(x))y=n[x];else{var z=a(q,w);if(z&&(y=v(z),n[x]=y,p.push(x),35<p.length)){var B=p.shift();delete n[B]}}return y}function c(q,r,v){var w=a(q,t[r]);return void 0!==w?w:v}function d(q,r){if(!q)return!1;var v=e(ri());ka(r)||(r=String(r||"").replace(/\s+/g,"").split(","));for(var w=[v],x=0;x<r.length;x++)if(r[x]instanceof RegExp){if(r[x].test(q))return!1}else{var y=
r[x];if(0!=y.length){if(0<=e(q).indexOf(y))return!1;w.push(e(y))}}return!Ri(q,w)}function e(q){m.test(q)||(q="http://"+q);return ib(jb(q),"HOST",!0)}function g(q,r,v){switch(q){case "SUBMIT_TEXT":return b(r,"FORM."+q,h,"formSubmitElement")||v;case "LENGTH":var w=b(r,"FORM."+q,k);return void 0===w?v:w;case "INTERACTED_FIELD_ID":return l(r,"id",v);case "INTERACTED_FIELD_NAME":return l(r,"name",v);case "INTERACTED_FIELD_TYPE":return l(r,"type",v);case "INTERACTED_FIELD_POSITION":var x=a(r,"interactedFormFieldPosition");
return void 0===x?v:x;case "INTERACT_SEQUENCE_NUMBER":var y=a(r,"interactSequenceNumber");return void 0===y?v:y;default:return v}}function h(q){switch(q.tagName.toLowerCase()){case "input":return Sa(q,"value");case "button":return Wa(q);default:return null}}function k(q){if("form"===q.tagName.toLowerCase()&&q.elements){for(var r=0,v=0;v<q.elements.length;v++)Gh(q.elements[v])&&r++;return r}}function l(q,r,v){var w=a(q,"interactedFormField");return w&&Sa(w,r)||v}var m=/^https?:\/\//i,n={},p=[],t={ATTRIBUTE:"elementAttribute",
CLASSES:"elementClasses",ELEMENT:"element",ID:"elementId",HISTORY_CHANGE_SOURCE:"historyChangeSource",HISTORY_NEW_STATE:"newHistoryState",HISTORY_NEW_URL_FRAGMENT:"newUrlFragment",HISTORY_OLD_STATE:"oldHistoryState",HISTORY_OLD_URL_FRAGMENT:"oldUrlFragment",TARGET:"elementTarget"};(function(q){Y.__aev=q;Y.__aev.b="aev";Y.__aev.g=!0;Y.__aev.priorityOverride=0})(function(q){var r=q.vtp_gtmEventId,v=q.vtp_defaultValue,w=q.vtp_varType;switch(w){case "TAG_NAME":var x=a(r,"element");return x&&x.tagName||
v;case "TEXT":return b(r,w,Wa)||v;case "URL":var y;a:{var z=String(a(r,"elementUrl")||v||""),B=jb(z),A=String(q.vtp_component||"URL");switch(A){case "URL":y=z;break a;case "IS_OUTBOUND":y=d(z,q.vtp_affiliatedDomains);break a;default:y=ib(B,A,q.vtp_stripWww,q.vtp_defaultPages,q.vtp_queryKey)}}return y;case "ATTRIBUTE":var E;if(void 0===q.vtp_attribute)E=c(r,w,v);else{var F=q.vtp_attribute,I=a(r,"element");E=I&&Sa(I,F)||v||""}return E;case "MD":var S=q.vtp_mdValue,T=b(r,"MD",ai);return S&&T?di(T,S)||
v:T||v;case "FORM":return g(String(q.vtp_component||"SUBMIT_TEXT"),r,v);default:return c(r,w,v)}})}();
Y.a.gas=["google"],function(){(function(a){Y.__gas=a;Y.__gas.b="gas";Y.__gas.g=!0;Y.__gas.priorityOverride=0})(function(a){var b=f(a),c=b;c[fc.ka]=null;c[fc.Ue]=null;var d=b=c;d.vtp_fieldsToSet=d.vtp_fieldsToSet||[];var e=d.vtp_cookieDomain;void 0!==e&&(d.vtp_fieldsToSet.push({fieldName:"cookieDomain",value:e}),delete d.vtp_cookieDomain);return b})}();

Y.a.awct=["google"],function(){var a=!1,b=[],c=function(k){var l=M("google_trackConversion"),m=k.gtm_onFailure;"function"==typeof l?l(k)||m():m()},d=function(){for(;0<b.length;)c(b.shift())},e=function(){return function(){d();a=!1}},g=function(){return function(){d();b={push:c};}},h=function(k){le();var l={google_basket_transaction_type:"purchase",google_conversion_domain:"",google_conversion_id:k.vtp_conversionId,google_conversion_label:k.vtp_conversionLabel,
google_conversion_value:k.vtp_conversionValue||0,google_remarketing_only:!1,onload_callback:k.vtp_gtmOnSuccess,gtm_onFailure:k.vtp_gtmOnFailure,google_gtm:zh()},m=function(t){return function(q,r,v){var w="DATA_LAYER"==t?vi(v):k[r];w&&(l[q]=w)}},n=m("JSON");n("google_conversion_currency","vtp_currencyCode");n("google_conversion_order_id","vtp_orderId");k.vtp_enableProductReporting&&(n=m(k.vtp_productReportingDataSource),n("google_conversion_merchant_id","vtp_awMerchantId","aw_merchant_id"),n("google_basket_feed_country",
"vtp_awFeedCountry","aw_feed_country"),n("google_basket_feed_language","vtp_awFeedLanguage","aw_feed_language"),n("google_basket_discount","vtp_discount","discount"),n("google_conversion_items","vtp_items","items"),l.google_conversion_items=l.google_conversion_items.map(function(t){return{value:t.price,quantity:t.quantity,item_id:t.id}}));!k.hasOwnProperty("vtp_enableConversionLinker")||k.vtp_enableConversionLinker?(k.vtp_conversionCookiePrefix&&(l.google_gcl_cookie_prefix=k.vtp_conversionCookiePrefix),
l.google_read_gcl_cookie_opt_out=!1):l.google_read_gcl_cookie_opt_out=!0;var p=!0;p&&b.push(l);a||(a=!0,L("//www.googleadservices.com/pagead/conversion_async.js",g(),e("//www.googleadservices.com/pagead/conversion_async.js")))};Y.__awct=h;Y.__awct.b="awct";Y.__awct.g=!0;Y.__awct.priorityOverride=0}();
Y.a.remm=["google"],function(){(function(a){Y.__remm=a;Y.__remm.b="remm";Y.__remm.g=!0;Y.__remm.priorityOverride=0})(function(a){for(var b=a.vtp_input,c=a.vtp_map||[],d=a.vtp_fullMatch,e=a.vtp_ignoreCase?"gi":"g",g=0;g<c.length;g++){var h=c[g].key||"";d&&(h="^"+h+"$");var k=new RegExp(h,e);if(k.test(b)){var l=c[g].value;a.vtp_replaceAfterMatch&&(l=String(b).replace(k,l));return l}}return a.vtp_defaultValue})}();Y.a.smm=["google"],function(){(function(a){Y.__smm=a;Y.__smm.b="smm";Y.__smm.g=!0;Y.__smm.priorityOverride=0})(function(a){var b=a.vtp_input,c=Si(a.vtp_map,"key","value")||{};return c.hasOwnProperty(b)?c[b]:a.vtp_defaultValue})}();




Y.a.paused=[],function(){(function(a){Y.__paused=a;Y.__paused.b="paused";Y.__paused.g=!0;Y.__paused.priorityOverride=0})(function(a){D(a.vtp_gtmOnFailure)})}();
Y.a.zone=[],function(){function a(p){for(var t=p.vtp_boundaries||[],q=0;q<t.length;q++)if(!t[q])return!1;return!0}function b(p){var t=Hc.i,q=t+":"+p.vtp_gtmTagId,r=vi("gtm.uniqueEventId")||0,v=Xd(function(){return new k}),w=a(p),x=p.vtp_enableTypeRestrictions?p.vtp_whitelistedTypes.map(function(I){return I.typeId}):null;x=x&&Da(x,h);if(v.registerZone(q,r,w,x))for(var y=p.vtp_childContainers.map(function(I){return I.publicId}),z=0;z<y.length;z++){var B=String(y[z]);if(v.registerChild(B,t,q)){var A=
0!==B.indexOf("GTM-"),E=0===B.indexOf("SB-")?c():void 0;if(A){var F=function(I,S){wi(arguments)};F("js",new Date);F("config",B);m||Fi(B,E,A)}else Fi(B,E,A)}}}function c(){var p=yi("_oid")[0];if(p)return{oid:p,namespace:"cookie"}}var d={active:!1,isWhitelisted:function(){return!1}},e={active:!0,isWhitelisted:function(){return!0}},g={zone:!0,cn:!0,css:!0,ew:!0,eq:!0,ge:!0,gt:!0,lc:!0,le:!0,lt:!0,re:!0,sw:!0,um:!0},h={cl:["ecl"],ecl:["cl"],ehl:["hl"],hl:["ehl"]},k=function(){this.Eb={};this.cd={}};k.prototype.checkState=
function(p,t){var q=this.Eb[p];if(!q)return e;var r=this.checkState(q.fe,t);if(!r.active)return d;for(var v=[],w=0;w<q.bd.length;w++){var x=this.cd[q.bd[w]];x.ab(t)&&v.push(x)}return v.length?{active:!0,isWhitelisted:function(y){if(!r.isWhitelisted(y))return!1;for(var z=0;z<v.length;++z)if(v[z].isWhitelisted(y))return!0;return!1}}:d};k.prototype.unregisterChild=function(p){delete this.Eb[p]};k.prototype.registerZone=function(p,t,q,r){var v=this.cd[p];if(v)return v.Gg(t,q),!1;if(!q)return!1;this.cd[p]=
new l(t,r);return!0};k.prototype.registerChild=function(p,t,q){var r=this.Eb[p];if(!r&&Ic[p]||r&&r.fe!==t)return!1;if(r)return r.bd.push(q),!1;this.Eb[p]={fe:t,bd:[q]};return!0};var l=function(p,t){this.ba=[{eventId:p,ab:!0}];this.Xb=null;if(t){this.Xb={};for(var q=0;q<t.length;q++)this.Xb[t[q]]=!0}};l.prototype.Gg=function(p,t){var q=this.ba[this.ba.length-1];p<=q.eventId||q.ab!=t&&this.ba.push({eventId:p,ab:t})};l.prototype.ab=function(p){if(!this.ba||0==this.ba.length)return!1;for(var t=this.ba.length-
1;0<=t;t--)if(this.ba[t].eventId<=p)return this.ba[t].ab;return!1};l.prototype.isWhitelisted=function(p){return this.Xb?g[p]||!!this.Xb[p]:!0};var m=!1;var n=function(p){b(p);D(p.vtp_gtmOnSuccess)};Y.__zone=n;Y.__zone.b="zone";Y.__zone.g=!0;Y.__zone.priorityOverride=0}();

Y.a.html=["customScripts"],function(){function a(d,e,g,h){return function(){try{if(0<e.length){var k=e.shift(),l=a(d,e,g,h);if("SCRIPT"==String(k.nodeName).toUpperCase()&&"text/gtmscript"==k.type){var m=C.createElement("script");m.async=!1;m.type="text/javascript";m.id=k.id;m.text=k.text||k.textContent||k.innerHTML||"";k.charset&&(m.charset=k.charset);var n=k.getAttribute("data-gtmsrc");n&&(m.src=n,La(m,l));d.insertBefore(m,null);n||l()}else if(k.innerHTML&&0<=k.innerHTML.toLowerCase().indexOf("<script")){for(var p=
[];k.firstChild;)p.push(k.removeChild(k.firstChild));d.insertBefore(k,null);a(k,p,l,h)()}else d.insertBefore(k,null),l()}else g()}catch(t){D(h)}}}var b=function(d,e,g){ce(function(){var h,k=Ic;k.postscribe||(k.postscribe=lc);h=k.postscribe;var l={done:e},m=C.createElement("div");m.style.display="none";m.style.visibility="hidden";C.body.appendChild(m);try{h(m,d,l)}catch(n){D(g)}})};var c=function(d){if(C.body){var e=
d.vtp_gtmOnFailure,g=Ii(d.vtp_html,d.vtp_gtmOnSuccess,e),h=g.Ac,k=g.I;if(d.vtp_useIframe){}else d.vtp_supportDocumentWrite?b(h,k,e):a(C.body,Xa(h),k,e)()}else pi(function(){c(d)},
200)};Y.__html=c;Y.__html.b="html";Y.__html.g=!0;Y.__html.priorityOverride=0}();





Y.a.lcl=[],function(){function a(){var c=M("document"),d=0,e=function(g){var h=g.target;if(h&&3!==g.which&&(!g.timeStamp||g.timeStamp!==d)){d=g.timeStamp;h=Ya(h,["a","area"],100);if(!h)return g.returnValue;var k=g.defaultPrevented||!1===g.returnValue,l=ng("lcl",k?"nv.mwt":"mwt",0),m;m=k?ng("lcl","nv.ids",[]):ng("lcl","ids",[]);if(m.length){var n=jg(h,"gtm.linkClick",m);if(b(g,h,c)&&!k&&l&&h.href){var p=M((Gi(h,"target")||"_self").substring(1)),t=!0;if(wi(n,Ff(function(){t&&p&&(p.location.href=Gi(h,
"href"))}),l))t=!1;else return g.preventDefault&&g.preventDefault(),g.returnValue=!1}else wi(n,function(){},l||2E3);return!0}}};Qa(c,"click",e,!1);Qa(c,"auxclick",e,!1)}function b(c,d,e){if(2===c.which||c.ctrlKey||c.shiftKey||c.altKey||c.metaKey)return!1;var g=Gi(d,"href"),h=g.indexOf("#"),k=Gi(d,"target");if(k&&"_self"!==k&&"_parent"!==k&&"_top"!==k||0===h)return!1;if(0<h){var l=ti(g),m=ti(e.location);return l!==m}return!0}(function(c){Y.__lcl=c;Y.__lcl.b="lcl";Y.__lcl.g=!0;Y.__lcl.priorityOverride=
0})(function(c){var d=void 0===c.vtp_waitForTags?!0:c.vtp_waitForTags,e=void 0===c.vtp_checkValidation?!0:c.vtp_checkValidation,g=Number(c.vtp_waitForTagsTimeout);if(!g||0>=g)g=2E3;var h=c.vtp_uniqueTriggerId||"0";if(d){var k=function(m){return Math.max(g,m)};mg("lcl","mwt",k,0);e||mg("lcl","nv.mwt",k,0)}var l=function(m){m.push(h);return m};mg("lcl","ids",l,[]);e||mg("lcl","nv.ids",l,[]);Di("lcl")||(a(),Ei("lcl"));D(c.vtp_gtmOnSuccess)})}();
Y.a.evl=["google"],function(){function a(g,h){this.element=g;this.uid=h}function b(){var g=Number(vi("gtm.start"))||0;return qi().getTime()-g}function c(g,h,k,l){function m(){if(!gi(g.target)){h.has(e.zb)||h.set(e.zb,""+b());h.has(e.jc)||h.set(e.jc,""+b());var p=0;h.has(e.Bb)&&(p=Number(h.get(e.Bb)));p+=100;h.set(e.Bb,""+p);if(p>=k){var t=jg(g.target,"gtm.elementVisibility",[h.uid]),q=fi(g.target);t["gtm.visibleRatio"]=Math.round(1E3*q)/10;t["gtm.visibleTime"]=k;t["gtm.visibleFirstTime"]=Number(h.get(e.jc));
t["gtm.visibleLastTime"]=Number(h.get(e.zb));wi(t);l()}}}if(!h.has(e.Ua)&&(0==k&&m(),!h.has(e.ya))){var n=M("self").setInterval(m,100);h.set(e.Ua,n)}}function d(g){g.has(e.Ua)&&(M("self").clearInterval(Number(g.get(e.Ua))),g.remove(e.Ua))}var e={Ua:"polling-id-",jc:"first-on-screen-",zb:"recent-on-screen-",Bb:"total-visible-time-",ya:"has-fired-"};a.prototype.has=function(g){return!!this.element.getAttribute("data-gtm-vis-"+g+this.uid)};a.prototype.get=function(g){return this.element.getAttribute("data-gtm-vis-"+
g+this.uid)};a.prototype.set=function(g,h){this.element.setAttribute("data-gtm-vis-"+g+this.uid,h)};a.prototype.remove=function(g){this.element.removeAttribute("data-gtm-vis-"+g+this.uid)};(function(g){Y.__evl=g;Y.__evl.b="evl";Y.__evl.g=!0;Y.__evl.priorityOverride=0})(function(g){function h(){var x=!1,y=null;if("CSS"===l){try{y=nf(m)}catch(F){}x=!!y&&v.length!=y.length}else if("ID"===l){var z=C.getElementById(m);z&&(y=[z],x=1!=v.length||v[0]!==z)}y||(y=[],x=0<v.length);if(x){for(var B=0;B<v.length;B++){var A=
new a(v[B],q);d(A)}v=[];for(var E=0;E<y.length;E++)v.push(y[E]);0<=w&&mi(w);0<v.length&&(w=li(k,v,[t]))}}function k(x){var y=new a(x.target,q);x.intersectionRatio>=t?y.has(e.ya)||c(x,y,p,"ONCE"===r?function(){for(var z=0;z<v.length;z++){var B=new a(v[z],q);B.set(e.ya,"1");d(B)}mi(w);if(n&&Mh)for(var A=0;A<Mh.length;A++)Mh[A]===h&&Mh.splice(A,1)}:function(){y.set(e.ya,"1");d(y)}):(d(y),"MANY_PER_ELEMENT"===r&&y.has(e.ya)&&(y.remove(e.ya),y.remove(e.Bb)),y.remove(e.zb))}var l=g.vtp_selectorType,m;"ID"===
l?m=String(g.vtp_elementId):"CSS"===l&&(m=String(g.vtp_elementSelector));var n=!!g.vtp_useDomChangeListener,p=g.vtp_useOnScreenDuration&&Number(g.vtp_onScreenDuration)||0,t=(Number(g.vtp_onScreenRatio)||50)/100,q=g.vtp_uniqueTriggerId,r=g.vtp_firingFrequency,v=[],w=-1;h();n&&Nh(h);D(g.vtp_gtmOnSuccess)})}();

var nk={};nk.macro=function(a){if(gg.kc.hasOwnProperty(a))return gg.kc[a]},nk.onHtmlSuccess=gg.Kd(!0),nk.onHtmlFailure=gg.Kd(!1);nk.dataLayer=rd;nk.callback=function(a){Rc.hasOwnProperty(a)&&ha(Rc[a])&&Rc[a]();delete Rc[a]};nk.vf=function(){Ic[Hc.i]=nk;za(Sc,Y.a);Yb=Yb||gg;Zb=Vd};
nk.gg=function(){bh.gtm_3pds=!0;Ic=u.google_tag_manager=u.google_tag_manager||{};if(Ic[Hc.i]){var a=Ic.zones;a&&a.unregisterChild(Hc.i)}else{for(var b=data.resource||{},c=b.macros||[],d=0;d<c.length;d++)Qb.push(c[d]);for(var e=b.tags||[],g=0;g<e.length;g++)Tb.push(e[g]);for(var h=b.predicates||[],
k=0;k<h.length;k++)Sb.push(h[k]);for(var l=b.rules||[],m=0;m<l.length;m++){for(var n=l[m],p={},t=0;t<n.length;t++)p[n[t][0]]=Array.prototype.slice.call(n[t],1);Rb.push(p)}Wb=Y;Xb=Li;nk.vf();Kf();Yd=!1;Zd=0;if("interactive"==C.readyState&&!C.createEventObject||"complete"==C.readyState)ae();else{Qa(C,"DOMContentLoaded",ae);Qa(C,"readystatechange",ae);if(C.createEventObject&&C.documentElement.doScroll){var q=!0;try{q=!u.frameElement}catch(x){}q&&be()}Qa(u,"load",ae)}yf=!1;"complete"===C.readyState?Af():
Qa(u,"load",Af);a:{if(!gd)break a;u.setInterval(hd,864E5);}
Oc=(new Date).getTime();}};(0,nk.gg)();

})()
