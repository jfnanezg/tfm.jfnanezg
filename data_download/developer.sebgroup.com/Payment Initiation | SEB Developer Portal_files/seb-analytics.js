/**
 * @SEB
 * Generates dataLayer for Adobe Analytics
 */
function sebCleanArray(e){for(var a=new Array,r=0;r<e.length;r++)e[r]&&a.push(e[r]);return a}function sebBuildDataLayer(e,a,r,n){for(var t=sebCleanArray(e),o="",i={pageName:document.title,language:void 0!==a?a:"en",environment:void 0!==r?r:"prod",pagetype:void 0!==n?n:"Information page",pageurl:window.location.href},l=0;l<t.length;l++)t[l]&&(0===l||(1===l?i.website=t[l]:(o=o+t[l]+"|",i["siteSection"+(l-1)]=o)));return i}var sebPageUrl=window.location.href.split("/");window.dataLayer=sebBuildDataLayer(sebPageUrl);