/**
 * Make elastic search tables display two rows of the last column
 * and allow for a show more button.
 */
(function ($) {
    //$.widget.bridge('uibutton', $.ui.button);
    //$.widget.bridge('uitooltip', $.ui.tooltip);
    $(function () {
        var window_height = $(window).height() || 0,
            header_height = $('#navigation').outerHeight() || 0,
            footer_height = ($('#footer').outerHeight() || 0) + ($('.main-footer').outerHeight() || 0);
        $('#main').css('min-height', window_height - (header_height + footer_height) - 30);


        $('#elasticsearch_hits_table tr td:nth-of-type(2)').each(function () {
            var text = $(this).html();
            var array = text.split('<br>');

            if (array.length > 2) {
                var div = $('<div />', {});
                div.append(array.shift() + '<br>' + array.shift());
                var hidden = $('<div />', { 'class': 'hidden-hit' }).css('display', 'none');
                hidden.append(array.join('<br>'));
                div.append(hidden);
                div.append('<br>');
                var btn = $('<button />', {
                    'type': 'button',
                    'class': 'btn btn-secondary btn-sm'
                }).html('Show More')
                    .click(function (e) {
                        e.preventDefault();

                        var hidden_hit = hidden;
                        if (hidden_hit.hasClass('is_open')) {
                            hidden_hit.removeClass('is_open');
                            hidden_hit.slideUp();
                            btn.html('Show More');
                        }
                        else {
                            hidden_hit.slideDown();
                            hidden_hit.addClass('is_open');
                            btn.html('Show Less');
                        }
                    });
                div.append(btn);
                $(this).html(div);
            }
        });

        // Add table responsive to divs that contain tables as a direct child
        $('div').has('table').last().addClass('table-responsive');

        $('#block-seb-elasticsearch-website-search-category .es-category-item').each(function () {
            $(this).css('position', 'relative');
            var text = $(this).text().split(' ');
            var num = text.pop();
            text = text.join(' ').split('_').join(' ');
            var span = $('<span />', { 'class': 'float-right' }).text(num.replace('(', '').replace(')', ''));
            span.css({
                position: 'absolute',
                right: 0,
                top: 0,
                backgroundColor: '#fff'
            });

            if ($(this).hasClass('active')) {
                $(this).css('color', '#000');
                span.html('<i class="fa fa-check"></i>');
            }
            else {
                span.css({
                    backgroundColor: '#e6e6e6',
                    borderRadius: 10,
                    paddingTop: 2,
                    paddingBottom: 2,
                    paddingLeft: 7,
                    paddingRight: 7,
                    fontSize: 12,
                    color: '#999'
                });
            }

            $(this).html(text);
            $(this).append(span);
        });

        $('.es-search-form-in-title .btn, .es-search-form-in-home .btn, .elasticsearch-search-input .btn').each(function () {
            var attributes = {};
            for (var i = 0; i < this.attributes.length; i++) {
                attributes[this.attributes[i].nodeName] = this.attributes[i].nodeValue;
            }

            var searchButton = $('<button />', attributes).html('<i class="fa fa-search"></i>');
            $(this).replaceWith(searchButton);
        });

        $('.elasticsearch-search-input .form-control').attr('style', '');

        $(document).on('elasticsearch.completed', function (event) {
            $('.elastic-result-block-footer a').attr('class', 'btn btn-primary');
        });

        $('.seb_pane').not('.hidesebPane').each(function () {
            var c = $(this).attr('class');
            var field = c.match(/^seb_pane-fieldset-(.*$)/) || [];
            if (field.length === 2) {
                $('#' + field[1].split(' ')[0]).addClass('active');
            }
        });

        $(document).on('seb_ds_pane_expanded', function () {
            if (typeof Event !== 'undefined') {
                window.dispatchEvent(new Event('resize'));
            } else {
                // Support IE
                var event = window.document.createEvent('UIEvents');
                event.initUIEvent('resize', true, false, window, 0);
                window.dispatchEvent(event);
            }
        });
    });
})(jQuery);

/**
 * Allow dropdown menus to open by hover on bigger devices.
 */
(function ($) {
    $(function () {
        $('.navbar .dropdown > .nav-link').click(function (e) {
            if ($(window).width() > 992) {
                var href = $(this).attr('href');
                if (href !== '#') {
                    return window.location.href = href;
                }
            }
        });

        $('.navbar .dropdown').hover(function () {
            if ($(window).width() > 992) {
                $(this).find('.dropdown-menu').fadeIn(100);
            }
        }, function () {
            if ($(window).width() > 992) {
                $(this).find('.dropdown-menu').fadeOut(100);
            }
        });
    });
})(jQuery);

/**
 * Re-implement seb DS
 */
(function ($) {
    $(function () {
        var links = $('.seb_pane-toc-list-item-link');

        links.each(function () {
            var id = '.seb_pane-fieldset-' + $(this).attr('id');
            if ($(id).length === 0) {
                $(this).parents('.views-row').first().remove();
                return;
            }

            if ($(id).children().not('.field-group-format-title').text().trim().length === 0) {
                $(this).parents('.views-row').first().remove();
            }
        });

        links.unbind('click');
        links.on('click', function (e) {
            e.preventDefault();
            $(this).parents('.ds-left').find('.seb_pane-toc-list-item-link.active').removeClass('active');
            $(this).addClass('active');

            var pane = $('.seb_pane-fieldset-' + $(this).attr('id'));
            if (pane.is('.hidesebPane')) {
                $('.seb_pane').not('.hidesebPane').addClass('hidesebPane');
                pane.removeClass('hidesebPane');
                var event = $.Event('seb_ds_pane_expanded');
                $(this).trigger(event);
            }
        });
    });
})(jQuery);
(function ($) {
    $(function () {

        if (jQuery('.singleapi-select-version').length) {
            jQuery(".singleapi-select-version").change(function() {
                location.href = jQuery(this).val();
            })
        }
        if (jQuery('form').length) {

            jQuery('.page-user- .tabs.secondary li.active').hide();
            
            jQuery('.form-item input, .form-item textarea').each(function (index, el) {
                if (jQuery(el).val().length > 0) {
                    var id = jQuery(el).attr('id')
                    jQuery('label[for="' + id + '"]').addClass('active');
                }
            })
            /*
            jQuery('.form-item input[type="text"], .form-item input[type="password"], .form-item textarea').on('focus paste change', function () {
                var id = jQuery(this).attr('id')
                jQuery('label[for="' + id + '"]').addClass('active');
            });
            jQuery('.form-item input[type="text"], .form-item input[type="password"], .form-item textarea').on('blur', function () {
                if (jQuery(this).val().length === 0) {
                    var id = jQuery(this).attr('id')
                    jQuery('label[for="' + id + '"]').removeClass('active');
                }
            });
            */
            jQuery('.form-item label span.form-required').each( function(idx, el) {
                var element = jQuery(el).parents('.form-item').find('input');
                if ( jQuery(element).length ) {
                    if ( jQuery(element).val().trim() === '' && jQuery('.alert.alert-danger ul').children().length ) {
                        jQuery(element.addClass('error'));
                    } else {
                        jQuery(element.removeClass('error'));
                    } 
                    jQuery(element).blur( function() {
                        if ( jQuery(element).val().trim() === '' ) {
                            jQuery(element.addClass('error'));
                        } else {
                            jQuery(element.removeClass('error'));
                        }
                    });
                }
            });
        }
        
        /**
         * Sorry about this. IE has a problem with scrollIntoView() that IBM's API.js uses,
         * causing the API overview page content to jump to the far left when a link in the
         * sidebar is clicked. A quick workaround is to check if the browser is IE, 
         * remove the inline onclick handler and add data attributes and finally add our own 
         * onclick handler.
         */
        var _isIE = navigator.appName == 'Microsoft Internet Explorer' ||  !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv:11/)) || (typeof $.browser !== "undefined" && $.browser.msie == 1)

        if (window.API && _isIE) {
            var scrollBuffer = 160;

            jQuery('.toc-container .tocItem a').each(function() {
                var attr = this.getAttribute('onclick').split("'");
                this.onclick = $.noop;
                this.dataset.path = attr[1];
                this.dataset.expand = attr[3];
            });
            jQuery('.toc-container .tocItem.operation a').on('click', function(event, value) {
                var path = this.dataset.path;
                var expand = this.dataset.expand;
                API.userNavigate = true;
                API.setSelectedPath(path, null, expand);
                var node = document.querySelector('.navigate-apis .navigate-' + path);
                if (node) {
                    jQuery(".opwrapper-" + path).addClass("open");
                    window.scrollTo(0, node.offsetTop-scrollBuffer);
                }
            });
            jQuery('.toc-container .tocItem.definition a').on('click', function(event, value) {
                var api = this.dataset.path;
                var def = this.dataset.expand;
                jQuery(".definitions_toggle_apis-" + api).addClass("open");
                API.userNavigate = true;
                API.setSelectedPath('apis_' + api + '_definitions_' + def, null, def);
                var node = document.querySelector('.navigate-apis .navigate-' + 'apis_' + api + '_definitions_' + def);
                if (node) {
                    window.scrollTo(0, node.offsetTop-scrollBuffer);
                }
            });

            jQuery('.toc-container .tocItem.navigate a').on('click', function(event, value) {
                var path = this.dataset.path;
                var expand = this.dataset.expand;
                this.userNavigate = true;
                this.setSelectedPath(path, null, expand);
                var node = document.querySelector('.navigate-apis .navigate-' + path);
                if (node) {
                    window.scrollTo(0, node.offsetTop-scrollBuffer);
                }
            });
        }

        /**
         * Updating TinyMCE's body fotn size. Default is 10px which is too small.
         * The moduel is not in our repository and is a common module in /all, hence
         * the need for this hackity hack hack
         */
        $('iframe').load( function() {
            $('iframe').contents().find("head").append($("<style type='text/css'>body { font-size: 14px !important; } </style>"));
        });
    });
})(jQuery);