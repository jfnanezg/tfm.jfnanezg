(function ($) {
  $(function() {
    var hasSEBTocItems = $('.mesh-portal-product .sebTocItems');
    if ( hasSEBTocItems.length > 0) {
      var scrollBuffer = 30;
      $('.tocItem', hasSEBTocItems).on('click', function(event, value) {
        $('.tocItem a.selected, .tocItem.selected').removeClass('selected');
        var section = jQuery(this).data('link');
        var node = document.querySelector('.api-doc-wrapper .api-doc-' + section);
        window.scrollTo(0, node.offsetTop-scrollBuffer);
        $(this).addClass('selected');
      });
      $('.toc-container .tocItem, .tocApi .tocItem').on('click', function(event, value) {
        $('.tocItem', hasSEBTocItems).removeClass('selected');
      });
    }
    if ( typeof window.sebTestAPICredentials == 'object' && loadSebTestAPICredentials) {
      $('input[name="paramX-Request-ID"').val(sebTestAPICredentials.id());
      $('input[name="paramPSU-IP-Address"').val(sebTestAPICredentials.host);
      $('input[name="paramAuthorization"').val('Bearer ∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙');
    }
  });
})(jQuery);