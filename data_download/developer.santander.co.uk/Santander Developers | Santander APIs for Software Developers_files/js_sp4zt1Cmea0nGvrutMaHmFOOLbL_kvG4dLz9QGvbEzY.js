(function ($) {
    Drupal.behaviors.SantanderOpenBanking = {

        attach: function (context, settings) {

            function getCookie(cname) {
                var name = cname + "=";
                var decodedCookie = decodeURIComponent(document.cookie);
                var ca = decodedCookie.split(';');
                for(var i = 0; i <ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') {
                        c = c.substring(1);
                    }
                    if (c.indexOf(name) == 0) {
                        return c.substring(name.length, c.length);
                    }
                }
                return "";
            }

            // Handle News Feed on Home Page
            $("div.news-feed").hide();
            $("div.has-newsfeed").hide();
            $("div.news-text").hide();
            if(getCookie("announcements") !== "hide") {
                if($("div.news-feed").length > 0) {
                    $("div.has-newsfeed").show();
                    $("div.news-text").show();
                    $(".has-newsfeed").click(function() {
                        $("span.glyphicon-chevron-down").toggle();
                        $("span.glyphicon-chevron-up").toggle();
                        $("div.news-text").toggle();
                        $("div.news-feed").slideToggle();
                    });
                }

                $("span.close-feed").click(function() {
                    $("div.news-feed").slideUp();
                    $("div.has-newsfeed").hide();
                    $("div.news-text").hide();
                    document.cookie = "announcements=hide";
                });
            }


            // Handle Get Started Page
            $("div.guides-more").hide();
            $("span.guides-title").on("mouseover click", function () {
                if (!$(this).nextAll("span.title-arrow").hasClass("opened")) {
                    $(this).nextAll("span.title-arrow").addClass("opened");
                    $(this).nextAll("div.guides-more").slideDown();
                }
            });
            $(".numberedItem>p").on("mouseover click", function () {
                if (!$(this).prevAll("span.title-arrow").hasClass("opened")) {
                    $(this).prevAll("span.title-arrow").addClass("opened");
                    $(this).nextAll("div.guides-more").slideDown();
                }
            });
            $("span.title-arrow").click(function() {
                if ($(this).hasClass("opened")) {
                    $(this).nextAll("div.guides-more").slideUp();
                    $(this).removeClass("opened");
                }
                else {
                    $(this).nextAll("div.guides-more").slideDown();
                    $(this).addClass("opened");
                }
            });


            $('h2.menu-toggle').click(function(event){
                event.preventDefault();
                $("nav.block-main-menu").toggle();
            });

            // Hide this message "* = Mouseover for more information" - OPENBANKUK-2197
            if($('.mesh-portal-product .plansFooter').length){
                $('.mesh-portal-product .plansFooter').hide();
            }

            // Show this message only in the Submit Application Page:
            if($('#application-node-form').length){
                $("body").prepend('<div id="PleaseWait" style="display: none;" title="Please Wait..">\n' +
                    '<p>We are validating your SSA.</p>\n' +
                    '<div id="PleaseWaitImage"></div>' +
                    '</div>');
            }

            // Redirect to the openid-connect Login:
            if($('body.page-openid-connect').length && $('#PleaseWait').length){
                $("#PleaseWait").dialog({
                    minWidth: 500
                });
                reloadWithQueryStringVars({"ob-login": "yes"});
            }

            $('body.ob-user #application-node-form').submit(function() {
                var pass = true;
                $(window).off("beforeunload");

                //some validations
                if (!$("#field-software-def-add-more-wrapper textarea").val()) {
                    // textarea is empty
                    pass = false;
                }
                if(pass == false){
                    return false;
                }

                $("#PleaseWait").dialog({
                    minWidth: 500
                });

                return true;
            });

            function reloadWithQueryStringVars (queryStringVars) {
                var existingQueryVars = location.search ? location.search.substring(1).split("&") : [],
                    currentUrl = location.search ? location.href.replace(location.search,"") : location.href,
                    newQueryVars = {},
                    newUrl = currentUrl + "&";
                if(existingQueryVars.length > 0) {
                    for (var i = 0; i < existingQueryVars.length; i++) {
                        var pair = existingQueryVars[i].split("=");
                        newQueryVars[pair[0]] = pair[1];
                    }
                }
                if(queryStringVars) {
                    for (var queryStringVar in queryStringVars) {
                        newQueryVars[queryStringVar] = queryStringVars[queryStringVar];
                    }
                }
                if(newQueryVars) {
                    for (var newQueryVar in newQueryVars) {
                        newUrl += newQueryVar + "=" + newQueryVars[newQueryVar] + "&";
                    }
                    newUrl = newUrl.substring(0, newUrl.length-1);
                    window.location.href = newUrl;
                } else {
                    window.location.href = location.href;
                }
            }

            if ($("#field-software-def-add-more-wrapper textarea").length > 0) {

                $(window).off("beforeunload");

                // Enable the message only if the Textarea is not Empty:
                $("#field-software-def-add-more-wrapper textarea").on("change keyup paste", function() {
                    var currentVal = $(this).val();

                    if(currentVal.length > 0) {
                        $(window).on("beforeunload");
                        $(window).bind('beforeunload', function(){
                            if ($("#field-software-def-add-more-wrapper textarea").val()) {
                                return 'Are you sure that you want to leave?';
                            }else{
                                return false;
                            }
                        });
                    }else{
                        $(window).off("beforeunload");
                    }
                });

            }

        }

    };
})(jQuery);;
/**
 * @file
 * A JavaScript file for the theme.
 * This file should be used as a template for your other js files.
 * It defines a drupal behavior the "Drupal way".
 *
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth

(function ($, Drupal, window, document, undefined) {
  'use strict';

  // To understand behaviors, see https://drupal.org/node/756722#behaviors
  Drupal.behaviors.hideSubmitBlockit = {
    attach: function(context) {
      var timeoutId = null;
      $('form', context).once('hideSubmitButton', function () {
        var $form = $(this);

        // Bind to input elements.
        if (Drupal.settings.hide_submit.hide_submit_method === 'indicator') {
          // Replace input elements with buttons.
          $('input.form-submit', $form).each(function(index, el) {
            var attrs = {};

            $.each($(this)[0].attributes, function(idx, attr) {
                attrs[attr.nodeName] = attr.nodeValue;
            });

            $(this).replaceWith(function() {
                return $("<button/>", attrs).append($(this).attr('value'));
            });
          });
          // Add needed attributes to the submit buttons.
          $('button.form-submit', $form).each(function(index, el) {
            $(this).addClass('ladda-button button').attr({
              'data-style': Drupal.settings.hide_submit.hide_submit_indicator_style,
              'data-spinner-color': Drupal.settings.hide_submit.hide_submit_spinner_color,
              'data-spinner-lines': Drupal.settings.hide_submit.hide_submit_spinner_lines
            });
          });
          Ladda.bind('.ladda-button', $form, {
            timeout: Drupal.settings.hide_submit.hide_submit_reset_time
          });
        }
        else {
          $('input.form-submit, button.form-submit', $form).click(function (e) {
            var el = $(this);
            el.after('<input type="hidden" name="' + el.attr('name') + '" value="' + el.attr('value') + '" />');
            return true;
          });
        }

        // Bind to form submit.
        $('form', context).submit(function (e) {
          var $inp;
          if (!e.isPropagationStopped()) {
            if (Drupal.settings.hide_submit.hide_submit_method === 'disable') {
              $('input.form-submit, button.form-submit', $form).attr('disabled', 'disabled').each(function (i) {
                var $button = $(this);
                if (Drupal.settings.hide_submit.hide_submit_css) {
                  $button.addClass(Drupal.settings.hide_submit.hide_submit_css);
                }
                if (Drupal.settings.hide_submit.hide_submit_abtext) {
                  $button.val($button.val() + ' ' + Drupal.settings.hide_submit.hide_submit_abtext);
                }
                $inp = $button;
              });

              if ($inp && Drupal.settings.hide_submit.hide_submit_atext) {
                $inp.after('<span class="hide-submit-text">' + Drupal.checkPlain(Drupal.settings.hide_submit.hide_submit_atext) + '</span>');
              }
            }
            else if (Drupal.settings.hide_submit.hide_submit_method !== 'indicator'){
              var pdiv = '<div class="hide-submit-text' + (Drupal.settings.hide_submit.hide_submit_hide_css ? ' ' + Drupal.checkPlain(Drupal.settings.hide_submit.hide_submit_hide_css) + '"' : '') + '>' + Drupal.checkPlain(Drupal.settings.hide_submit.hide_submit_hide_text) + '</div>';
              if (Drupal.settings.hide_submit.hide_submit_hide_fx) {
                $('input.form-submit, button.form-submit', $form).addClass(Drupal.settings.hide_submit.hide_submit_css).fadeOut(100).eq(0).after(pdiv);
                $('input.form-submit, button.form-submit', $form).next().fadeIn(100);
              }
              else {
                $('input.form-submit, button.form-submit', $form).addClass(Drupal.settings.hide_submit.hide_submit_css).hide().eq(0).after(pdiv);
              }
            }
            // Add a timeout to reset the buttons (if needed).
            if (Drupal.settings.hide_submit.hide_submit_reset_time) {
              timeoutId = window.setTimeout(function() {
                hideSubmitResetButtons(null, $form);
              }, Drupal.settings.hide_submit.hide_submit_reset_time);
            }
          }
          return true;
        });
      });

      // Bind to clientsideValidationFormHasErrors to support clientside validation.
      // $(document).bind('clientsideValidationFormHasErrors', function(event, form) {
        //hideSubmitResetButtons(event, form.form);
      // });

      // Reset all buttons.
      function hideSubmitResetButtons(event, form) {
        // Clear timer.
        window.clearTimeout(timeoutId);
        timeoutId = null;
        switch (Drupal.settings.hide_submit.hide_submit_method) {
          case 'disable':
            $('input.' + Drupal.checkPlain(Drupal.settings.hide_submit.hide_submit_css) + ', button.' + Drupal.checkPlain(Drupal.settings.hide_submit.hide_submit_css), form)
              .each(function (i, el) {
                $(el).removeClass(Drupal.checkPlain(Drupal.settings.hide_submit.hide_submit_hide_css))
                  .removeAttr('disabled');
              });
            $('.hide-submit-text', form).remove();
            break;

          case 'indicator':
            Ladda.stopAll();
            break;

          default:
            $('input.' + Drupal.checkPlain(Drupal.settings.hide_submit.hide_submit_css) + ', button.' + Drupal.checkPlain(Drupal.settings.hide_submit.hide_submit_css), form)
              .each(function (i, el) {
                $(el).stop()
                  .removeClass(Drupal.checkPlain(Drupal.settings.hide_submit.hide_submit_hide_css))
                  .show();
              });
            $('.hide-submit-text', form).remove();
        }
      }
    }
  };

})(jQuery, Drupal, window, this.document);
;
