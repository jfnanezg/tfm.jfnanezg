/**
 * Created by fauzanaziz on 19/1/17.
 */
function generateParameters(obj, htmlBody, parameterObject) {

    parameterBlock = "<table class=\"table table-bordered table-striped borderless table-curved\"><colgroup><col class=\"col-xs-1\"><col class=\"col-xs-1\"><col class=\"col-xs-6\"></colgroup><thead><tr><th id='th1'>Parameter</th><th id='th3'>Type</th><th id='th2'>Description</th></tr></thead><tbody class=\"parameter-table\">";

    for (var parameter in parameterObject) {
        var typein = parameterObject[parameter].in;
        if (typein == "body") {
            var parameters = parseRef(obj, parameterObject[0].schema.$ref).properties;
            parameterBlock += htmlBody;
            parameterBlock = parseRefBody(parameters, parameterBlock);
        } else {
            var type = parameterObject[parameter].type;
            if (type == "number") {
                type = "double";
            }
            parameterBlock += "<tr><td scope=\"row\">" +
                "<code>" + parameterObject[parameter].name + "</code></td>" +
                "<td>" + type + "</td>" +
                "<td>" + parameterObject[parameter].description + "</td></tr>";
        }
    }

    parameterBlock += "</tbody></table></div>";

    return parameterBlock;
}

function parseRefBody(parameters,htmlBody){
    for (var parameter in parameters) {
        if (parameters[parameter].type == "number") {
            parameters[parameter].type = "double";
        }
        htmlBody += "<tr><td scope=\"row\">" +
            "<code>" + parameter + "</code></td>" +
            "<td>" + parameters[parameter].type + "</td>" +
            "<td>" + parameters[parameter].description + "</td></tr>";
        if (parameters[parameter].type == "array") {
            console.log("Array");
            htmlBody = parseRefBody(parameters[parameter].items.properties,htmlBody)
        } else if (parameters[parameter].type == "object") {
            htmlBody = parseRefBody(parameters[parameter].properties,htmlBody)
        }
    }
    return htmlBody;
}
function parseRef(obj, refPath) {
    var arrayPath = refPath.split("/");
    var refObject = obj;

    for (var i = 1; i < arrayPath.length; i++) {
        refObject = refObject[arrayPath[i]];
    }
    return refObject
}
function generateResourceBody(obj, htmlBody, resourceObject, level) {
    for (var property in resourceObject) {
        var typein = resourceObject[property].in;
        if (typein == "body") {
            var parameters = parseRef(obj, resourceObject[property].schema.$ref).properties;
            for (var parameter in parameters) {
                if (parameters[parameter].type == "array") {
                    if (parameters[parameter].items.type == "object") {
                        htmlBody += "<p class=\"code_indent_" + level + "\">\"" + parameter + "\": [</p>";
                        htmlBody += "<p class=\"code_indent_" + (level + 1) + "\">{</p>";
                        htmlBody = generateResourceBody(obj, htmlBody, parameters[parameter].items.properties, level + 2);
                        htmlBody += "<p class=\"code_indent_" + (level + 1) + "\">}, <i>...</i></p>";
                        htmlBody += "<p class=\"code_indent_" + level + "\">]";
                    }
                    else {
                        var itemsType = parameters[parameter].items.type;
                        if (itemsType == "number")
                            itemsType = "double";
                        htmlBody += "<p class=\"code_indent_" + level + "\">\"" + parameter + "\": [...] <i>" + itemsType + "</i>";
                    }
                    // Checking if current property is not the last property in the JSONObject
                    if (Object.keys(parameters)[Object.keys(parameters).length - 1] != parameter) {
                        htmlBody += ",";
                    }
                    htmlBody += "</p>";

                } else if (parameters[parameter].type == "object") {
                    htmlBody += "<p class=\"code_indent_" + level + "\">\"" + parameter + "\": {</p>";
                    htmlBody = generateResourceBody(obj, htmlBody, parameters[parameter].properties, level + 1);
                    htmlBody += "<p class=\"code_indent_" + level + "\">}";
                    if (Object.keys(parameters)[Object.keys(parameters).length - 1] != parameter) {
                        htmlBody += ",";
                    }
                    htmlBody += "</p>";
                } else {
                    htmlBody += "<p class=\"code_indent_" + level + "\">\"" + parameter + "\": <i>" + type + "</i>";
                    // Checking if current property is not the last property in the JSONObject
                    if (Object.keys(parameters)[Object.keys(parameters).length - 1] != parameter) {
                        htmlBody += ",";
                    }
                    htmlBody += "</p>";
                }
            }
        }

        else {
            var type = resourceObject[property].type;
            if (type == "number")
                type = "double";
            if (resourceObject[property].type == "array") {
                if (resourceObject[property].items.type == "object") {
                    htmlBody += "<p class=\"code_indent_" + level + "\">\"" + property + "\": [</p>";
                    htmlBody += "<p class=\"code_indent_" + (level + 1) + "\">{</p>";
                    htmlBody = generateResourceBody(obj, htmlBody, resourceObject[property].items.properties, level + 2);
                    htmlBody += "<p class=\"code_indent_" + (level + 1) + "\">}, <i>...</i></p>";
                    htmlBody += "<p class=\"code_indent_" + level + "\">]";
                }
                else {
                    var itemsType = resourceObject[property].items.type;
                    if (itemsType == "number")
                        itemsType = "double";
                    htmlBody += "<p class=\"code_indent_" + level + "\">\"" + property + "\": [...] <i>" + itemsType + "</i>";
                }
                // Checking if current property is not the last property in the JSONObject
                if (Object.keys(resourceObject)[Object.keys(resourceObject).length - 1] != property) {
                    htmlBody += ",";
                }
                htmlBody += "</p>";

            } else if (resourceObject[property].type == "object") {
                htmlBody += "<p class=\"code_indent_" + level + "\">\"" + property + "\": {</p>";
                htmlBody = generateResourceBody(obj, htmlBody, resourceObject[property].properties, level + 1);
                htmlBody += "<p class=\"code_indent_" + level + "\">}";
                if (Object.keys(resourceObject)[Object.keys(resourceObject).length - 1] != property) {
                    htmlBody += ",";
                }
                htmlBody += "</p>";
            } else {
                htmlBody += "<p class=\"code_indent_" + level + "\">\"" + property + "\": <i>" + type + "</i>";

                // Checking if current property is not the last property in the JSONObject
                if (Object.keys(resourceObject)[Object.keys(resourceObject).length - 1] != property) {
                    htmlBody += ",";
                }
                htmlBody += "</p>";
            }
        }
    }
    return htmlBody;
}

function generateResourceTable(htmlTable, resourceObject) {
    for (var propertyName in resourceObject) {
        var type = resourceObject[propertyName].type;

        if (type == "number")
            type = "double";

        htmlTable += "<tr><td scope=\"row\"><code>" + propertyName + "</code></td><td>" + type + "</td><td>" + resourceObject[propertyName].description + "</td></tr>";
        if (resourceObject[propertyName].type == "array") {
            htmlTable = generateResourceTable(htmlTable, resourceObject[propertyName].items.properties)
        } else if (resourceObject[propertyName].type == "object") {
            htmlTable = generateResourceTable(htmlTable, resourceObject[propertyName].properties)
        }
    }
    return htmlTable;
}

function generateSandbox(obj, htmlBody, sandboxHeaders, sandboxContents) {
    htmlBody += "<colgroup>";
    var i = 1;
    var threeCol = false;

    for (var header in sandboxHeaders) {
        if (i > 2) {
            htmlBody += "<col class=\"col-xs-6\">";
            threeCol = true;
        }
        else {
            htmlBody += "<col class=\"col-xs-1\">";
            i++;
        }
    }

    i = 1;

    htmlBody += "</colgroup><thead> <tr>";

    for (var header in sandboxHeaders) {
        htmlBody += "<th id=\'th" + i + "\'>" + sandboxHeaders[header] + "</th>";
        i++;
    }

    htmlBody += "</tr> </thead><tbody>";

    var names = (Object.keys(sandboxContents.properties));

    var values = sandboxContents.properties;

    var x = 0;

    for (var value in values) {
        if(threeCol) {
            htmlBody += "<tr><td scope=\"row\">" + "<code>" + names[x] + "</code></td>" +
                "<td>" + values[value].example[0].name + "</td><td>" + values[value].example[1].name + "</td></tr>";
            x++;
        }

        else {
            htmlBody += "<tr><td scope=\"row\">" + "<code>" + names[x] + "</code></td>" +
                "<td>" + values[value].example[0].name + "</td></tr>";
            x++;
        }
    }

    htmlBody += "</tbody>";

    return htmlBody;
}