var apitracker = {};
apitracker.tracker = function(obj)
{

    var jsondata = JSON.stringify(obj);
    jsondata = JSON.parse(jsondata);

    var sessionStore = sessionStorage.getItem('localDetails');
    var localDetails = (sessionStore === null || 'undefined' == typeof sessionStore)? getDetails():sessionStore;

    if("undefined" !== typeof localDetails) {
        jsondata['local'] = JSON.parse(localDetails);
    }

    var finaljson = JSON.stringify(removeNulls(jsondata));


    var http = new XMLHttpRequest();
    var url = "https://api.ocbc.com:8243/analytics/1.0/analytics";
    var params = finaljson;

    http.open("POST", url, true);

    //Send the proper header information along with the request
    http.setRequestHeader("App-Name", "api_store");
    http.setRequestHeader("App-Version", "1.0");
    http.setRequestHeader("Content-type", "application/json");
    http.send(params);
};


function removeNulls(obj) {
    var isArray = obj instanceof Array;
    for (var k in obj) {
        if (obj[k] === null) isArray ? obj.splice(k, 1) : delete obj[k];
        else if (typeof obj[k] == "object") removeNulls(obj[k]);
        if (isArray && obj.length == k) removeNulls(obj);
    }
    return obj;
}

function getDetails()
{
    var http = new XMLHttpRequest();
    var url = "https://geoip.nekudo.com/api";


    http.open("GET", url, true);

    //Send the proper header information along with the request
    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=UTF-8");

    var localDetails;

    http.onreadystatechange = function() {//Call a function when the state changes.
        if (http.readyState == 4 && http.status == 200) {

            localDetails = http.responseText;
            if ("undefined" !== typeof localDetails) {
                sessionStorage.setItem('localDetails', localDetails);
                return localDetails;
            }
        }
    }
    http.send();
}

var scribe = new Scribe({
    tracker:          apitracker,
    trackClicks:      true,
    trackHashChanges: true,
    trackEngagement:  true,
    trackLinkClicks:  true,
    trackRedirects:   true,
    trackSubmissions: true,
    trackPageViews:   true
});
