// Get data from json file
let categoryIds = [];
let $apis = $('.apis');
let imgPath;
$.getJSON("./site/conf/api_products.json")
    .done(function(data) {
        $.each(data.Category, function( i, item ) {
            categoryIds.push(item.categoryExpandId);
            imgPath = item.imgPathRoot;
            autoApiHeight($('.row'));
        });
        apiNumberInRow();
    })
    .fail(function(xhr,textStatus,error) {
        console.log("Request Failed: " + textStatus + ", " + error);
    });
// when the size of browser is adjusted,the expand div will adjust also
$(window).resize(function () {
    let $currentSelector = $apis.siblings('.active');
    let $currentCategory = $currentSelector.find(".category-expand");
    autoApiHeight($currentSelector);
    toggleActiveClass($currentSelector, $currentCategory.height());
    apiNumberInRow();
	sizeUpdate();
    autoApiHeight($('.row'));
});

$(window).scroll(function () {
	
	// client's viewport height
    var height = $(window).scrollTop();

    var highlightColor = '#d8232a';
    var sidebarRectangleTopMargin = 25;
	var hrTopAndBottomMarginSum = 40;
    
    // title bar object
    var titleInformationalDiv = $('div#Informational.type');
    var titleTransactionalDiv = $('div#Transactional.type');
    
    // sidebar object
    var sidebarNavList = $('ul#navList');
    var sidebarVerticalBar = $('div#rectangle');
    
    // reset all sidebar children style to be non-highlighted 
    sidebarNavList.children().removeAttr('style');
    
    var sidebarNavList2 = [titleInformationalDiv, titleTransactionalDiv];
    var i;
    for (i = 0; i < sidebarNavList2.length; ++i) {
    	if (height >= (sidebarNavList2[i].offset().top - sidebarNavList2[i].height() - hrTopAndBottomMarginSum)) {	
    		if ((i + 1 < sidebarNavList2.length && height < (sidebarNavList2[i + 1].offset().top - sidebarNavList2[i + 1].height() - 		hrTopAndBottomMarginSum ))|| i + 1 === sidebarNavList2.length) {
    			sidebarNavList.children().eq(i + 1).css('color',highlightColor);
        		sidebarVerticalBar.css('margin-top', sidebarRectangleTopMargin * (i + 1) + 'px');
    			break;
    		} else {
    			continue;
    		}
    	} else {
    		// This is the first All APIs nav list
    	    sidebarNavList.children().first().css('color',highlightColor);
    		sidebarVerticalBar.css('margin-top','0px');
			break;
    	}
    }
});

function autoApiHeight($currentSelector) {
    let maxHeight = 0;
    $currentSelector.find('.individual-api').each(function () {
        $(this).css("height","");
        if(maxHeight < $(this).height()){
            maxHeight = $(this).height();
        }
    });
    $currentSelector.find('.individual-api').each(function () {
        if(window.innerWidth > 1024){
            $(this).css("height",maxHeight + parseInt($(this).css('padding-top')));
        }else {
            $(this).css("height",maxHeight);
        }
    });
}
// Decide how many apis show in one row
function apiNumberInRow() {
	if(window.innerWidth > 768 ){
		let numberOfAPIPerRow = 5;
		if (categoryIds.length < numberOfAPIPerRow) {
			$apis.css("width", 100 / categoryIds.length + "%");
		}
    }else {
        $apis.css("width","");
    }
}

//reorganize when size changes
function sizeUpdate() {
	var smallScreenSize = 768;
	var middleScreenSize = 992;
    var width = $(window).width(); // Get window width

	var apiPerRow = 3;

	if(width > smallScreenSize && width < middleScreenSize) {
		apiPerRow = 2;
	} else if(width <= smallScreenSize) {
		apiPerRow = 1;
	}

	$('hr.removable').remove();
	$('div.type-block').each(function(){
		var apiObjs = $(this).children(".row.apis").children();
		var apiObjsArr = jQuery.makeArray( apiObjs );
		var i;
		var j;
		var length = apiObjsArr.length;
		
		for(i=0; i+apiPerRow -1<length; i+=apiPerRow){
			var newRow = ($(apiObjsArr[i]));
			if(apiPerRow!=1){
				for(j=i; j<i+apiPerRow-1; j++){
					if(apiObjsArr[j].parentElement.className == "row apis")
						$(apiObjsArr[j]).unwrap();
				}
				for(j=i+1; j<=i+apiPerRow-1; j++){
					newRow = newRow.add($(apiObjsArr[j]));
				}
				newRow.wrapAll('<div class="row apis"/>');
			} else {
				if(apiObjsArr[i].parentElement.className == "row apis") {
					$(apiObjsArr[i]).unwrap();
				}
				newRow.wrap("<div class=\"row apis\"/></div>");
			}
		}
		var leftOverNum = length%apiPerRow;
		if(leftOverNum!=0){
			var leftOverStartIndex = length-leftOverNum;
			var newRow = $(apiObjsArr[leftOverStartIndex]);
			for(j=leftOverStartIndex; j<length; j++){
				if(apiObjsArr[j].parentElement.className == "row apis") {
					$(apiObjsArr[j]).unwrap();
				}
			}
			for(j=leftOverStartIndex; j<length; j++){
				newRow = newRow.add($(apiObjsArr[j]));
			}
			newRow.wrapAll('<div class="row apis"/>');
		}
	});
	
	//delete empty div.row.apis and put hr
	var $last = $('div.row.apis').last();
	$("div.row.apis").each(function(){
		var $this = $(this);
		var hasGallery = $this.children().length > 0;
		if(!hasGallery){
			$this.remove();
		}
		if(!($this[0] === $last[0])) {
			$this.after('<hr class="removable">');
		}
	});
}


// To check if div is collapsed or expanded
let toggleActiveClass = function toggleActiveClass($currentSelector, categoryHeight) {
    let expandPadding = 40;
    let currentPaddingBottom = parseInt($currentSelector.css("padding-bottom"));
    if ($currentSelector.find('.category-expand').css('visibility') == 'visible') {
        if (currentPaddingBottom <= 10) {
            $(".active").css("padding-bottom", "+=" + (categoryHeight + expandPadding).toString());
        } else if (currentPaddingBottom != categoryHeight) {
            $currentSelector.css("padding-bottom", "+=" + (categoryHeight - currentPaddingBottom + expandPadding).toString());
        } else {
            $currentSelector.css("padding-bottom", "+=" + categoryHeight);
        }
    } else {
         $currentSelector.css("padding-bottom", "");
    }
};
// Process click event
$apis.find('a:first').click(function () {
    let $currentSelector = $(this).parent();
    let $currentCategory = $currentSelector.find(".category-expand");

    if ($currentSelector.hasClass('active')) {
        $currentSelector.removeClass('active');
    } else {
        let $currentSelectorSibling = $currentSelector.addClass('active').siblings('.active');
        $currentSelectorSibling.removeClass('active');
        $currentSelectorSibling.css("padding-bottom", "");
        $currentSelectorSibling.find('.icon-img').attr("src",imgPath + "icon_medium\/icon_" + $currentSelectorSibling.find('a').attr('id') + "_medium.png");
        $currentSelectorSibling.find('h2').css({'color':'','font-weight':''});
        autoApiHeight($currentSelector);
    }

    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        if ($currentSelector.hasClass('active')) {
            $(this).find('.icon-img').attr("src",imgPath + "icon_medium\/icon_" + $(this).attr('id') + "_medium_hover.png");
        }else {
            $(this).find('.icon-img').attr("src",imgPath + "icon_medium\/icon_" + $(this).attr('id') + "_medium.png");
        }
    }

    if ($currentCategory != null) {
        if($currentSelector.find('.category-expand').css("visibility") == "hidden"){
            $currentSelector.find('.category-expand').css("visibility","visible");
            $currentSelectorSibling.find('.category-expand').css("visibility","hidden");
        }else {
            $currentSelector.find('.category-expand').css("visibility","hidden");
        }
        toggleActiveClass($currentSelector, $currentCategory.height());
    } else {
        $apis.css("padding-bottom", "");
    }
});
//hover
$('.apis > a,.apis.active > a').hover(function () {
    $(this).find('.icon-img').attr("src",imgPath + "icon_medium\/icon_" + $(this).attr('id') + "_medium_hover.png");
    $(this).find('h2').css({'color':'#d8232a','font-weight':'700'});
},function() {
    if(!$(this).parent().hasClass('active')){
        $(this).find('.icon-img').attr("src",imgPath + "icon_medium\/icon_" + $(this).attr('id') + "_medium.png");
        $(this).find('h2').css({'color':'','font-weight':''});
    }
});