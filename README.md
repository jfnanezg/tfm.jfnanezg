# tfm.jfnanezg

**Resumen**

En el presente documento se expone una comparativa de algoritmos de de recuperación de información, que se utilizará para analizar las especificaciones funcionales de APIs escritas en lenguaje natural, expuestas en portales de APIs del sector financiero (Banca). 

La comparativa permite determinar cual de los algoritmos seleccionados genera el mejor resultado, una vez realizado el procesamiento de la información, teniendo en cuenta diferentes técnicas de lenguaje natural.  Es necesario tener en cuenta variables como precisión, recuperación, ubicación de las especificaciones recuperadas en la comparación, cantidad de información recuperada, cantidad de información no recuperada y tiempo de respuesta.  El conjunto de datos utilizado en el procesamiento esta conformado por la información escrita en lenguaje natural que se encuentran en diferentes portales de API’s, principalmente especificaciones.

Las técnicas utilizadas son:
1.- Procesamiento de Lenguaje natural
2.- Recuperación de información

Palabras Clave: APIs, procesamiento de lenguaje natural, recuperación de información, modelos de similitud de texto basados en conjuntos, modelos de similitud de texto basados en geometría
